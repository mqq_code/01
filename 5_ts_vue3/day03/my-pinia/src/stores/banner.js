import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useUserStore } from './user.js'

export const useBannerStore = defineStore('banner', () => {
  const userInfo = useUserStore()

  console.log('banenrStore中使用其他store的数据', userInfo.username)
  const banner = ref([])

  const getBanner = async () => {
    const res = await axios.get('http://zyxcl.xyz/music_api/banner')
    banner.value = res.data.banners
  }

  return {
    banner,
    getBanner
  }
})

