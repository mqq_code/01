import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const username = ref('小明')
  const age = ref(20)
  const sex = ref('男')

  const info = computed(() => {
    console.log('重新计算')
    return `我的名字叫${username.value}, 今年${age.value}岁`
  })

  const changeAge = (num) => {
    age.value += num
  }

  return {
    username,
    age,
    sex,
    info,
    changeAge
  }
})

