import { defineStore } from 'pinia'
import { ref } from 'vue'


export const useMovieStore = defineStore('movie', () => {
  const pageNum = ref(1) // 当前第几页
  const total = ref(0) // 总条数
  const curListLen = ref(0) // 当前列表总数

  const changePageNum = () => {
    console.log('到底了')
    if (curListLen.value < total.value) {
      pageNum.value ++
    }
  }

  return {
    pageNum,
    total,
    curListLen,
    changePageNum
  }
})
