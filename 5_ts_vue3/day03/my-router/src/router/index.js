import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '../views/home/Home.vue'
import Movie from '../views/home/movie/Movie.vue'
import MovieList from '../views/home/movie/movielist/MovieList.vue'

import Cinema from '../views/home/cinema/Cinema.vue'
import Mine from '../views/home/mine/Mine.vue'
import Detail from '../views/detail/Detail.vue'
import City from '../views/city/City.vue'
import Login from '../views/login/Login.vue'

const router = createRouter({
  history: createWebHistory(), // history模式，url没有#
  // history: createWebHashHistory(), // hash模式，url有#
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          redirect: '/home/movie/hot',
          children: [
            {
              path: '/home/movie/:type',
              name: 'movielist',
              component: MovieList
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/mine',
          name: 'mine',
          meta: {
            isNeedLogin: true
          },
          component: Mine
        }
      ]
    },
    {
      path: '/detail/:id',
      name: 'detail',
      meta: {
        isNeedLogin: true
      },
      component: Detail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/city',
      name: 'city',
      component: City
    },
    {
      redirect: '/home'
    }
  ]
})

router.beforeEach((to, from) => {
  
  if (to.matched.some(v => v.meta.isNeedLogin)) {
    // 此路由需要拦截
    const token = localStorage.getItem('token')
    if (!token) {
      console.log('要跳转的目标地址', to.fullPath)
      return {
        path: '/login',
        query: {
          redirectPath: encodeURIComponent(to.fullPath)
        }
      }
      // 取消跳转
      // return false
    }
  }
})

export default router
