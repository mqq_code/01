const http = require('http')
const fs = require('fs')
const path = require('path')

const httpPort = 8000

http.createServer((req, res) => {
  const fullPath = path.join(__dirname, 'dist', req.url) 
  if (fs.existsSync(fullPath) && req.url !== '/') {
    res.end(fs.readFileSync(fullPath, 'utf-8'))
    return
  }

  // 处理history模式路由，如果找不到对应的静态资源就直接返回 index.html
  fs.readFile(path.join(__dirname, 'dist/index.html'), 'utf-8', (err, content) => {
    if (err) {
      console.log('We cannot open "index.html" file.')
    }
    res.writeHead(200, {
      'Content-Type': 'text/html; charset=utf-8',
    })
    res.end(content)
  })
})
.listen(httpPort, () => {
  console.log('Server listening on: http://localhost:%s', httpPort)
})