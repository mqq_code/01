// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // 1. 类型推论, ts 会根据值反推变量的类型 
  // let a = 100
  // const arr = [1,2,3,4,5,6,7]
  // const arr1 = arr.map(item => item + 'abc')

  // 2. 类型断言：当开发者比编辑器更确定类型时使用
  const h1 = document.querySelector('.title') as HTMLHeadingElement
  const inp = <HTMLInputElement>document.querySelector('.search')
  const bt = document.querySelector('.btn') as HTMLButtonElement
  const box = document.querySelector('.box') as HTMLDivElement
  const inp1 = document.querySelector('input')!
  h1.classList.add('test')
  inp.value = '100'

  inp1.addEventListener('input', (e) => {
    console.log((e.target as HTMLInputElement).value)
  })

  document.addEventListener('keydown', e => {
    console.log(e.keyCode)
  })


  // 3. 非空断言
  const btn = document.querySelector('.btn')
  btn!.addEventListener('click', (e) => {
    console.log('点击按钮', (e as MouseEvent).pageX)
  })

  // 3. 可选链
  const btn1 = document.querySelector('.btn1')
  btn1?.addEventListener('click', () => {
    console.log('点击按钮')
  })


}