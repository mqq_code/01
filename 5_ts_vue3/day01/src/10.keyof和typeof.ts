// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // typeof: typeof js变量 => 获取某一个变量的类型
  const obj =  { name: '小明', age: 22 }

  type IObj = typeof obj
  const o1: IObj = {
    name: 'xm',
    age: 44
  }


  // keyof: keyof 类型变量 => 对象类型中所有的key组件成的联合类型
  interface IPerson {
    name: string;
    age: number;
    sex: '男' | '女';
    hobby: string[];
  }

  type Key = keyof IPerson // 'name' | 'age' | 'sex' | 'hobby'
  let k: Key = 'hobby'



  const xm = {
    name: '小明',
    father: '大名',
    mother: '翠花',
    son: '小小名'
  }


  type xmKey = keyof typeof xm


}