// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

// 枚举: 为了方便记忆可以使用单词来表示某个数字或者字母
enum Sex {
  men = 0,
  women = 1
}


// 用枚举表示单词和数字的对应关系
enum Direction {
  up = 38,
  down = 40,
  left = 37,
  right = 39
}

const box = document.querySelector('.box')! as HTMLDivElement
document.addEventListener('keydown', e => {
  if (e.keyCode === Direction.left) {
    box.style.left = box.offsetLeft - 10 + 'px'
  } else if (e.keyCode === Direction.up) {
    box.style.top = box.offsetTop - 10 + 'px'
  } else if (e.keyCode === Direction.right) {
    box.style.left = box.offsetLeft + 10 + 'px'
  } else if (e.keyCode === Direction.down) {
    box.style.top = box.offsetTop + 10 + 'px'
  }
})








}