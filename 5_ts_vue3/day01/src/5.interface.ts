// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // interface 接口
  // 1. 定义对象类型
  interface Person {
    readonly name: string;
    age: number;
    sex?: boolean;
    say: (hobby: string) => void;
  }
  const obj: Person = {
    name: 'xiaoming',
    age: 22,
    sex: true,
    say(hobby: string): void {
      console.log(`我叫${this.name},爱好${hobby}`)
    }
  }

  // 2. 定义函数类型
  // interface fn {
  //   (a: number, b: string): boolean;
  // }
  // const fn: fn = function(a, b) {
  //   return true
  // }

  // 3. 定义数组
  // interface Arr {
  //   [index: number]: number
  // }
  // const arr: Arr = [1,2,3,4,5]


  // 4. 继承
  interface Doctor extends Person {
    job: string;
  }
  const doc: Doctor = {
    name: '小明',
    age: 33,
    sex: true,
    job: '外科医生',
    say() {
    }
  }

  // 5. 定义重名的接口会进行合并
  interface Doctor {
    children?: string[]
  }
  const xg: Doctor = {
    name: '小刚',
    age: 44,
    job: '实习医生',
    say() {},
    children: ['a', 'b', 'c']
  }

  

}