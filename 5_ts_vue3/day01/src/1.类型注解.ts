// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{


// 定义变量时添加类型注解，定义类型后变量不能赋值为其他类型
let num: number = 100
let str: string = 'abc'
let flag: boolean = true
let n: null = null
let un: undefined = undefined
let s: symbol = Symbol(100)

// 定义指定类型的数组
let arr1: number[] = [1,2,3,4,5,6,7]
// 泛型方式定义数组
let arr2: Array<string> = ['a', 'b', 'c']

// 元组：已知类型和长度的数组
const arr3: [string, number, boolean] = ['a', 10, true]


// any: 任意类型，相当于放弃类型校验
let a: any = 100
a = 'aaa'
a = null
a = true

// void: 没有值，通常用在函数没有返回值的情况
function fn() {
  console.log('我是函数')
}
let fr = fn()

// never: 永远不会出现的值
let bb: number & string

// unknown: 暂时不确定值的类型，但是不想放弃校验，后续确定类型时可以使用类型断言
let unk: unknown
if (window.$a === 1) {
  unk = 1000
  ;(unk as number).toFixed(2)
} else {
  unk = 'abcdefg'
  ;(unk as string).split('')
}



// sybmol: 唯一值，可以当作对象的 key
// const a = Symbol('abc')
// const b = Symbol('abc')
// const obj = {
//   a: 100
// }
// obj[a] = '1111'
// obj[b] = '22222'
// console.log(obj)

}