// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

// 1. 定义对象类型
// const obj: {
//   name: string;
//   age: number;
//   sex: boolean;
//   hobby: string[];
// } = {
//   name: '小明',
//   age: 20,
//   sex: true,
//   hobby: ['唱歌', '跳舞']
// }
// function fn1(person: { name: string; age: number; sex: boolean; hobby: string[];}) {
//   console.log(person.name, person.age)
// }

// 2. 定义类型别名
// type IObj = {
//   name: string;
//   age: number;
//   sex: boolean;
//   hobby: string[];
// }
// const obj: IObj = {
//   name: '小明',
//   age: 20,
//   sex: true,
//   hobby: ['唱歌', '跳舞']
// }
// function fn1(person: IObj) {
//   console.log(person.name, person.age)
// }
// console.log(fn1(obj))


// 3. 可选属性和只读属性
type IObj = {
  readonly name: string; // 只读属性
  age: number;
  sex: boolean;
  hobby?: string[]; // 可选属性
  [k: string]: any; // 其他属性类型
}
const obj: IObj = {
  name: '小明',
  age: 20,
  sex: true
}

// 4. 使用接口定义对象类型
interface IPerson {
  readonly name: string; // 只读属性
  age: number;
  sex: boolean;
  hobby?: string[]; // 可选属性
  [k: string]: any; // 其他属性类型
}

const obj2: IPerson = {
  name: 'xm',
  age: 22,
  sex: false,
  hobby: ['a', 'b'],
  aaaa: '测试'
}



// 5. 定义对象类型数组
type testA = { a: number, b: string }
const arr: testA[] = [{ a: 1, b: '2' }, { a: 2, b: '3' }]

type route = {
  path: string;
  name: string;
  children?: route[]
}
const routes: route[] = [
  {
    path: '/home',
    name: 'home',
    children: [
      {
        path: '/home/movie',
        name: 'movie',
        children: [
          {
            path: '/home/movie/hot',
            name: 'hot',
          }
        ]
      },
      {
        path: '/home/cinema',
        name: 'cinema',
      }
    ]
  }
]


type item = number | item[]
const arr1: item[] = [[1,[2,[3,[4],5],6],7,8],9,0]



}