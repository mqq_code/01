// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // 泛型：定义类型时的形参

  // 泛型函数
  // function log<T>(a: T) {
  //   console.log(a)
  // }

  // log<string>('123')
  // log<number>(100)


  function createArray<T>(val: T, num: number): T[] {
    return new Array(num).fill(val)
  }
  let a1 = createArray<string>('abc', 10)
  let b1 = createArray<number>(100, 10)
  let c1 = createArray<{ name: string }>({ name: '小明' }, 10)
  console.log(a1)
  console.log(b1)
  console.log(c1)

  function testFn<T, U, K>(a: T, b: U): U {
    let c: K
    return b
  }


  // 泛型别名
  type deepArr<T> = T | deepArr<T>[]
  const arr1: deepArr<number>[] = [[1,[2,[3,4,5],6],7,8],9]
  const arr2: deepArr<string>[] = [['a', ['b'], ['c', 'd'], 'e'], 'f']


  // 泛型接口
  interface Person<T> {
    name: string;
    age: number;
    hobby: T
  }

  const obj: Person<string> = {
    name: '小明',
    age: 33,
    hobby: '吃饭'
  }
  const obj1: Person<number> = {
    name: '小明',
    age: 33,
    hobby: 100
  }


  function format<T>(arr: T[]): { key: T }[] {
    return arr.map(item => {
      return {
        key: item
      }
    })
  }

  const a3 = format<number>([1,2,3,4,5,6])
  const a2 = format<string>(['a', 'b', 'c', 'd', 'e'])

  a3.forEach(item => {
    console.log(item.key.toFixed())
  })
  a2.forEach(item => {
    console.log(item.key.slice)
  })


  // 泛型约束：泛型 T 必须符合 extends 后的条件
  function getLen<T extends { length: number }>(a: T): number {
    return a.length
  }

  console.log(getLen<string>('100'))
  console.log(getLen<number[]>([1,2,3,4,5,6,7]))
  console.log(getLen<{ a: number, length: number }>({ a: 100, length: 10 }))



  // function fn1<T extends string | number[]>(a: T) {

  // }


}