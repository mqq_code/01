// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // 1. 联合类型
  // 联合类型的变量只能使用多种类型共有的属性和方法
  function fn(a: string | string[]) {
    console.log(a.length)
    console.log(a.slice(1))
    if (Array.isArray(a)) {
      // 类型保护
      console.log(a.reverse())
    } else {
      console.log(a.split(''))
    }
  }

  // 2. 交叉类型: 合并多个类型，不能合并不可兼容的类型
  const obj: { a: string } & { b: number } = {
    a: 'aaa',
    b: 200
  }
  // let aa: number & string = '100'


  // 3. 文字类型
  const sex: '男' | '女' = '男'

  type person = {
    name: string;
    sex: '男' | '女',
    age: 10 | 20 | 30
  }
  const xm: person = {
    name: 'xm',
    sex: '女',
    age: 30
  }

}