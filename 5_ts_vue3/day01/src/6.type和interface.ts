// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

  // type 和 interface 的相同点
  // 1. 都可以定义类型
  // 2. 都可以扩展

  type Person = {
    name: string;
    age: number;
  }
  const obj: Person = {
    name: '小明',
    age: 22
  }

  // type使用交叉类型进行扩展
  type Person1 = Person & { sex: string }
  const obj1: Person1 = {
    name: 'xm',
    age: 33,
    sex: '男'
  }

  // type Person1 = {}

  type Num1 = number
  type str = string
  const a: str = 'asfdasdfasd'

  // type 和 interface 的区别
  // 1. interface是定义新类型，type是给现有的类型定义别名
  // 2. interface 通过继承扩展，type 通过交叉类型扩展
  // 3. 重名的 interface 会进行合并，重名的 type 会报错
  // 4. type 可以定义基础类型，interface不能定义基础类型

  
}