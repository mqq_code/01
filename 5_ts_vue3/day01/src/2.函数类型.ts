// ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
{

// 1.定义函数类型
function fn1(a: number, b: string): number {
  return a + b.length
}
const fn2 = function(a: number, b: string): number {
  return a + b.length
}
const fn3 = (a: number, b: string): number => {
  return a + b.length
}
const obj = {
  fn4(a: number, b: string): number{
    return a + b.length
  }
}
// 完整写法
// const fn5: (a: number, b: string) => number = (a: number, b: string): number => {
//   return a + b.length
// }

// fn1(1, 'a')

// 2. 函数的可选参数
function fn6(a: number, b?: number, c?: string) {
  if (b) {
    return a + b
  }
  return a
}
// console.log(fn6(1))

// 3. 参数默认值
function fn7(a: number, b: string = '100') {
  console.log(a, b)
}
// fn7(10, '200')

// 4. 函数重载: 提前定义函数的多种类型，调用函数时自动匹配复合的类型
function fn(a: number, b: string): number
function fn(a: boolean): boolean
function fn(a: any, b?: string): number | boolean {
  if (b) {
    return a + b.length
  } else {
    return a
  }
}
let a1 = fn(100, 'abc')
let a2 = fn(true)
console.log(a1)
console.log(a2)

}