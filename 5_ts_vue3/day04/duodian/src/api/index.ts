import axios from 'axios'

// 抛出类型
export interface BannerItem {
  imageUrl: string;
  targetId: number;
}

interface BannerResponse {
  code: number;
  banners: BannerItem[]
}

export const getBanner = () => {
  // 指定接口返回值类型
  return axios.get<BannerResponse>('http://zyxcl.xyz/music_api/banner')
}

export interface GoodsItem {
  checked: boolean;
  id: string;
  images: string[];
  imgUrl: string;
  name:string;
  price: number;
}

export const getGoodsListApi = () => {
  return axios.get<GoodsItem[]>('/api/list')
}


interface mapRes {
  status: number;
  message: string;
  result: { name: string; address: string }[]
}
// 根据环境变量判断请求的域名
const mapHost = import.meta.env.MODE === 'development' ? '/api' : 'https://api.map.baidu.com'

export const mapSearchApi = (query: string, region: string) => {
  return axios.get<mapRes>(`${mapHost}/place/v2/suggestion`, {
    params: {
      query,
      region,
      city_limit: true,
      output: 'json',
      ak: 'nqdUuCXZg7BNjuKReZHTsWbc0L28hYrF'
    }
  })
}

