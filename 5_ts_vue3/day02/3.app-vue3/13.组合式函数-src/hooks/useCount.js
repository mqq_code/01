import { ref, onBeforeUnmount } from 'vue'

// 组合式函数：封装公用逻辑，函数名称以 ‘use’ 开头，内部可以使用响应式变量、计算属性、watch、生命周期......
export const useCount = (defaultCount = 10) => {

  const count = ref(defaultCount)
  let timer = null
  const start = () => {
    timer = setInterval(() => {
      count.value--
      console.log(count.value)
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer)
  }
  const reset = () => {
    count.value = 10
    clearInterval(timer)
  }

  onBeforeUnmount(() => {
    console.log('组件销毁之前')
    stop()
  })
  

  return {
    count,
    start,
    stop,
    reset
  }
}
