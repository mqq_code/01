
interface ITest {
  name: string;
}

// 给 window 对象添加属性
interface Window {
  $a: string;
  getA: () => void;
}
// 给全局变量添加类型
declare const _VERSION_: '我是一个版本号';
declare let _test_: number[];
declare function getVersion(): string;


// 给模块声明类型
declare module '*.jpg'
declare module '*.webp'
// 给没有声明文件的第三方包添加类型
// declare module 'mockjs' {
//   // 定义模块内的属性和方法类型
//   const a: string;
//   function mock(): void;
// }