import './scss/style.scss'
import img0 from './img/img0.jpg'
import img1 from './img/img1.webp'
import axios from 'axios'
import Mock from 'mockjs'
// 引入局部类型文件中定义的类型
import { Util } from './utils/util'

const appendImage = (src: string) => {
  return new Promise<HTMLImageElement>((resolve, reject) => {
    const image = new Image()
    image.src = src
    image.onload = function () {
      resolve(image)
    }
    image.onerror = function () {
      reject(new Error('图片加载失败'))
    }
  })
}

appendImage(img0).then(img => {
  document.body.appendChild(img)
})

appendImage(img1).then(img => {
  document.body.appendChild(img)
})

const aa: Util = {
  a: '111'
}


interface Iobj {
  name: string;
  age: number;
}
const obj: Iobj = {
  name: '小明',
  age: 20
}
console.log(obj)


const xm: ITest = {
  name: '100'
}



console.log('$a', window.$a)
console.log('_VERSION_', _VERSION_)
console.log('_test_', _test_)
// window.getA()
getVersion()




// const p = new Promise<number>((resolve, reject) => {
//   setTimeout(() => {
//     resolve(Math.random())
//   }, 1000)
// })

// p.then(res => {
//   console.log(res.toFixed(2))
// })










// ts中有两种文件
// 1. .ts 可以定义类型和逻辑，.d.ts 类型声明文件只能定义类型
// 2. 默认会直接读取全局的 .d.ts 文件中的类型
// 3. .d.ts 文件中没有使用 import 或者 export 就是全局文件
// 4. 在 ts 文件中引入第三方包
//    - 自带声明文件，例如：axios
//    - 没有声明文件，例如：mockjs, 尝试从 npm 下载对应的声明文件 `npm i --save-dev @types/包名`
//    - 如果 `npm i --save-dev @types/包名` 没有对应的声明文件，需要手动定义对应的模块类型
