const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|webp|svg)$/i,
        type: 'asset'
      },
      {
        test: /\.(js|ts)$/i,
        exclude: /(node_modules|bower_components)/,
        // npm i -D typescript ts-loader
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html'
    })
  ],
  devServer: {
    port: 8000,
    hot: true,
    open: true
  }
}
