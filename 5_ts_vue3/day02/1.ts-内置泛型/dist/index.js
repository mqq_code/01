"use strict";
{
    const sum = (...rest) => {
        return rest.reduce((prev, val) => prev + val);
    };
    // type Test<T> =  T | Test<T>[]
    // interface Person<T, U> {
    //   name: T,
    //   age: number,
    //   sex: U
    // }
    // const xm: Person<string, 0 | 1> = {
    //   name: 'adsfad',
    //   age: 222,
    //   sex: 1
    // }
    // function getInfo<T, U>(name: T, age: number, sex: U) {
    //   return { name, age, sex }
    // }
    // const xm1 = getInfo<number, '男' | '女'>(20000, 100, '男')
}
