{
  // ts 中常用的内置泛型
  // Partial
  // Required
  // Readonly
  // Pick
  // Omit
  // Record
  // Exclude
  // Extract
  // ReturnType

  interface Iobj {
    name: string;
    age: number;
    sex: '男' | '女';
    hobby: string[];
    info: {
      a: string;
      b: number;
      c: { e: boolean };
    };
    readonly say: (text: string) => void;
    children?: Iobj[];
  }

 type PartialObj = Partial<Iobj> // 把对象的所有属性改成可选属性
 type RequiredObj = Required<Iobj> // 把对象的所有属性改成必传属性
 type ReadonlyObj = Readonly<Iobj> // 把对象的所有属性改成只读属性
 
 type PickObj = Pick<Iobj, 'age' | 'name' | 'children'> // 从对象类型中获取指定的属性生成新类型
 type OmitObj = Omit<Iobj, 'age' | 'name' | 'children'> // 从对象类型中删除指定的属性生成新类型

//  type RecordObj = Record<'a' | 'b' | 'c', boolean> // Record<T, U> 创建对象类型，T是对象的key，U是对象所有属性值的类型
type RecordObj = Record<keyof Iobj, string> 

type ExcludeTest = Exclude<string | number | boolean, number | any[]> // Exclude<T, U> 删除 T 中可以赋值给 U 的类型
type ExtractText = Extract<string | number | boolean, number | any[]> // Extract<T, U> 提取 T 和 U 中相同的类型

 
type getHobby = (name: string, age: number) => string[]
type hobbyReturn = ReturnType<getHobby> // 函数的返回值类型


const sum = (...rest: string[]) => {
  return rest.reduce((prev, val) => prev + val)
}
type sumRetrun = ReturnType<typeof sum>























  // type Test<T> =  T | Test<T>[]

  // interface Person<T, U> {
  //   name: T,
  //   age: number,
  //   sex: U
  // }

  // const xm: Person<string, 0 | 1> = {
  //   name: 'adsfad',
  //   age: 222,
  //   sex: 1
  // }

  // function getInfo<T, U>(name: T, age: number, sex: U) {
  //   return { name, age, sex }
  // }

  // const xm1 = getInfo<number, '男' | '女'>(20000, 100, '男')







}