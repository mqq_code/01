import React, { useState } from 'react'
import { Space, Table, Button } from 'antd'

// 表头数据
const columns = [
  {
    title: '姓名',
    dataIndex: 'name', // 此列渲染的字段
    key: 'name',
    render: (_, record) => {
      // _ : dataIndex 对应的值
      // record: 当前行的数据
      // console.log(_, record)
      return <b style={{ color: 'red' }}>{record.name}</b>
    }
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
    onCell: (_, index) => {
      console.log(_, index)
      return {
        colSpan: index === 1 ? 2 : 1
      }
    }
  },
  {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
    onCell: (_, index) => {
      let rowSpan = 1
      if (index === 2) {
        rowSpan = 2
      } else if (index === 3) {
        rowSpan = 0
      }
      return {
        colSpan: index === 1 ? 0 : 1,
        rowSpan
      }
    }
  },
  {
    title: '操作',
    key: 'action',
    render: () => {
      return <Space>
        <Button type="primary">查看</Button>
        <Button type="primary" ghost>编辑</Button>
        <Button danger>删除</Button>
      </Space>
    }
  },
];
// 表格每一行数据
const data = [
  {
    key: '1',
    name: '小明',
    age: 32,
    address: '北京',
  },
  {
    key: '2',
    name: '小红',
    age: 42,
    address: '上海',
  },
  {
    key: '3',
    name: '小刚',
    age: 32,
    address: '杭州',
  },
  {
    key: '4',
    name: '小李',
    age: 32,
    address: '杭州',
  },
];

const App = () => {
  const [page, setPage] = useState(4)
  return (
    <div className="app">
      <h1>table</h1>

      <Table
        loading={false}
        bordered
        columns={columns}
        dataSource={data}
        pagination={{
          current: page,
          pageSize: 10,
          total: 200,
          onChange: (page, pagesize) => {
            console.log(page, pagesize)
            setPage(page)
          }
        }}
      />
    </div>
  )
}

export default App