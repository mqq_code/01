import React, { Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import './index.scss'
import 'animate.css'
import App from './App'
import {
  HashRouter
} from 'react-router-dom'
import { Provider123 } from './store'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <Provider123>
    <App />
  </Provider123>
);
