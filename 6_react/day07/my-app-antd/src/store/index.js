import { createContext, useContext, useEffect, useState } from 'react'
import axios from 'axios'

const ctx = createContext()


export const Provider123 = (props) => {
  const [count, setCount] = useState(10)
  const [title] = useState('aaaaa')
  const [banner, setBanenr] = useState(10)
  
  const getBanner = async () => {
    const res = await axios.get('xxxxxxx')
    setBanenr(res.data.banners)
  }
  
  return <ctx.Provider value={{
    count,
    setCount,
    title,
    banner,
    getBanner
  }}>
    {props.children}
  </ctx.Provider>
}


export const useStore = () => {
  return useContext(ctx)
}



export default ctx