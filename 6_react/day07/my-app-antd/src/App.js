import React, { useState, createContext } from 'react'
import Child1 from './components/Child1'
import { useStore } from './store'


const App = () => {
  const { count, setCount } = useStore()

  return (
    <div className="app">
      <h2>App</h2>
      <p>count: {count}</p>
      <Child1 />
    </div>
  )
}

export default App




// function fn(callback) {
//   setTimeout(() => {
//     const num = Math.random()
//     callback(num)
//   }, 2000)
// }


// fn((num) => {
//   console.log(1, num)
//   fn((num) => {
//     console.log(2, num)
//     fn((num) => {
//       console.log(3, num)
//       fn((num) => {
//         console.log(4, num)
//         fn((num) => {
//           console.log(5, num)
//         })
//       })
//     })
//   })
// })

// function fn() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       const num = Math.random()
//       resolve(num)
//     }, 1000)
//   })
// }

// fn()
// .then(res => {
//   console.log(1, res)
//   return fn()
// })
// .then(res => {
//   console.log(2, res)
//   return fn()
// })
// .then(res => {
//   console.log(3, res)
// })

// async function run(){
//   const res1 = await fn()
//   console.log(res1)

//   const res2 = await fn()
//   console.log(res2)

//   const res3 = await fn()
//   console.log(res3)
// }
// run()













// Promise三种状态：pending、fulfilled、rejected
// window.p = new Promise((resolve, reject) => {
//   // 同步代码，立即执行
//   setTimeout(() => {
//     const num = Math.floor(Math.random() * 10)
//     if (num >= 5) {
//       resolve(num) // pending => fulfilled
//     } else {
//       reject('失败了' + num) // pending => rejected
//     }
//   }, 1000)
// })


// console.log(window.p)

// window.p.then(
//   res => {
//     // fulfilled 执行此函数
//     console.log(res)
//     console.log(res.aaa())
//   },
//   err => {
//     // rejected 执行此函数
//     console.log(err)
//   }
// ).catch(e => {
//   // 兜底逻辑
//   console.log(e)
// })




// function axios() {
//   return new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest()
//     xhr.open('get', 'url')
//     xhr.send()
//     xhr.onload = () => {
//       resolve()
//     }
//     xhr.onerror = () => {
//       reject()
//     }
//   })
// }

// axios('https://sssss.com').then(Res => {
  
// }).catch(e => {

// })
