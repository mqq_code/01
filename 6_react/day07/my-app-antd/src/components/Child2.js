import React from 'react'
import Child3 from './Child3'

const Child2 = () => {
  return (
    <div className='box'>
      <h2>Child2</h2>
      <Child3 />
    </div>
  )
}

export default Child2