import React from 'react'
import Child4 from './Child4'

const Child3 = () => {
  return (
    <div className='box'>
      <h2>Child3</h2>
      <Child4 />
    </div>
  )
}

export default Child3