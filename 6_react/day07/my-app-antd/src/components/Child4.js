import React, { useEffect } from 'react'
import { useStore } from '../store'

const Child4 = () => {
  const { count, setCount, getBanner }= useStore()

  return (
    <div className='box'>
      <h2>Child4</h2>
      <p>app的count： {count}</p>
      <button onClick={() => {
        setCount(count + 1)
        getBanner()
      }}>+</button>
    </div>
  )
}

export default Child4