import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

interface BannerItem {
  imageUrl: string;
  typeTitle: string;
  targetId: number;
}

interface BannerRes {
  code: number;
  banners: BannerItem[];
}

const Home: React.FC = () => {
  const [banners, setBanners] = useState<BannerItem[]>([])
  const navigate = useNavigate()

  const getBannerApi = async () => {
    const res = await axios.get<BannerRes>('https://zyxcl.xyz/music_api/banner')
    setBanners(res.data.banners)
  }

  useEffect(() => {
    getBannerApi()
  }, [])

  console.log(banners)

  return (
    <div>
      {banners.map(item =>
        <div className="item" key={item.targetId}>
          <img src={item.imageUrl} alt={item.typeTitle} onClick={() => {
            // navigate(`detail?title=${item.typeTitle}&id=${item.targetId}&image=${encodeURIComponent(item.imageUrl)}`)
            navigate(`/detail/${item.targetId}/${item.typeTitle}?image=${encodeURIComponent(item.imageUrl)}`)
          }} />
        </div>
      )}
    </div>
  )
}

export default Home