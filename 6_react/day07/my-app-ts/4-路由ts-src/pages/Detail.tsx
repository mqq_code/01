import React from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'


type IParams = Record<'id' | 'title', string>

interface qs {
  [k: string]: string
}

const query = (search: string) => {
  const res: qs  = {}
  const arr = search.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    res[key] = val
  })
  return res
}


const Detail: React.FC = () => {
  const location = useLocation()
  const [searchParams] = useSearchParams()
  const params = useParams<IParams>()


  console.log(query(location.search))
  console.log(location)
  console.log(searchParams.get('image'))
  console.log(params)

  return (
    <div>
      <h2>Detail</h2>
      <p>id: {params.id}</p>
      <p>title: {params.title}</p>
    </div>
  )
}

export default Detail