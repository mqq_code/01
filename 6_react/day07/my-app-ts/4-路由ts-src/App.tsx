import React from 'react'
import Home from './pages/Home'
import Detail from './pages/Detail'
import { useRoutes } from 'react-router-dom'

const App: React.FC = () => {
  const routes = useRoutes([
    { path: '/', element: <Home /> },
    { path: '/detail/:id/:title', element: <Detail /> }
  ])

  return routes
}

export default App