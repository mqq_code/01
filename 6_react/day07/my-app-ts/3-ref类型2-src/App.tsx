import React, { useEffect, useMemo, useRef, useState } from 'react'
import Child1 from './components/Child1'
import type { Child1Ref } from './components/Child1'

// .ts 文件只能写 ts 代码，不可以使用 jsx 语法
// .tsx 文件，支持写jsx语法

const App = () => {
  const Child1Ref = useRef<Child1Ref>(null)


  return (
    <div>
      <h1>app组件</h1>
      <button onClick={() => {
        console.log(Child1Ref.current)
        Child1Ref.current?.changeCount()
      }}>获取child方法</button>

      <Child1 ref={Child1Ref} />
    </div>
  )
}

export default App