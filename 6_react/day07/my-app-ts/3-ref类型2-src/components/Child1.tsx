import React, { useState, forwardRef, useImperativeHandle } from 'react'

interface Props {
}

export interface Child1Ref {
  count: number;
  changeCount: () => void;
}

const Child1: React.ForwardRefRenderFunction<Child1Ref, Props> = (props, ref) => {
  // 2. 第二个参数接收父组件传入的ref对象
  // ref: 父组件传入的ref对象
  const [title, setTitle] = useState('Child1')
  const [count, setCount] = useState(0)
  const changeCount = () => {
    setCount(count + 1)
  }

  // 3. 给ref添加数据
  useImperativeHandle(ref, () => {
    // return的数据会赋值给ref的current属性
    return {
      count,
      changeCount
    }
  }, [count])


  return (
    <div className='box'>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <p>count: {count}</p>
      <button onClick={changeCount}>add</button>
    </div>
  )
}

// 1. 转发父组件传入的ref对象给Child1
export default forwardRef(Child1)