import React, { useEffect } from 'react'

// 合成事件
const App = () => {

  useEffect(() => {
    document.querySelector('h1')?.addEventListener('click', e => {
      console.log(e)
    })
  }, [])

  return (
    <div>
      <h1>标题</h1>
      <div className="box" onClick={e => {
        // 合成事件：
        // react中的事件没有绑定在具体元素上，
        // react内部实现一套事件处理机制，收集所有事件，存到事件池中
        // 利用事件冒泡的原理把所有元素的事件绑定到 document（react18是root元素） 上
        // 触发事件时根据触发事件的元素去事件池中执行对应的函数

        // e: react 合成事件对象
        // e.nativeEvent 获取原生事件对象
        console.log('box', e.nativeEvent)
      }}>
        <button onClick={e => {
          console.log('button', e)
        }}>按钮</button>
      </div>
    </div>
  )
}

export default App