import React, { Component } from 'react'

class App extends Component {
  state = {
    num: 0
  }
  // add = () => {
  //   console.log(this.state)
  // }

  add() {
    // console.log(this.state)
    console.log(this)
  }

  render() {
    console.log(this)
    return (
      <div>
        <h1>App</h1>
        {/* 使用箭头函数绑定add中的this指向 */}
        <button onClick={() => this.add()}>按钮</button>
        {/* 使用bind西拐add的this指向 */}
        <button onClick={this.add.bind(this)}>按钮</button>


        {/* this指向undefined */}
        {/* <button onClick={this.add}>按钮</button> */}
      </div>
    )
  }
}


export default App


// // 事件池
// const obj = {
//   a: function() {
//     console.log(this)
//   },
//   b: function() {}
// }
// // 触发事件的元素
// const name = 'a'
// // 根据元素找到对应的函数
// const f = obj[name]
// // 执行函数
// f() // this => 全局 => undefined