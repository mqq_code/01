import React, { useState } from 'react'

const App = () => {
  const [arr, setArr] = useState<number[]>([])
  const add = () => {
    setArr([...arr, Date.now()])
  }
  return (
    <div className='box'>
      <h2>列表中的key</h2>
      <button onClick={() => {
        const newArr = [...arr]
        newArr.shift()
        setArr(newArr)
      }}>删除第一个</button>
      <button onClick={add}>add</button>
      <ul>
        {arr.map((item, index) =>
          <li key={item}>
            <input type="checkbox" />
            {item} - {new Date(item).toLocaleString()}
          </li>
        )}
      </ul>
    </div>
  )
}

export default App

// 使用index做key
// 更新前：0 1 2 3
// 删除第一个
// 更新后：0 1 2

// 使用唯一值当做key
// 更新前：a b c d
// 删除第一个
// 更新后：b c d