import React, { Component } from 'react'

interface Props {
  text: string;
}

interface ArrItem {
  id: number
}
interface State {
  num: number;
  arr: ArrItem[];
}


class Child2 extends Component<Props, State> {

  state = {
    num: 0,
    arr: [] as ArrItem[]
  }
  push = () => {
    this.setState({
      arr: [...this.state.arr, { id:  Math.random() }]
    })
  }
  render() {
    console.log(this.props.text)
    return (
      <div>
        <p>num: {this.state.num}</p>
        <button onClick={() => {
          this.setState({ num: this.state.num + 1 })
        }}>+</button>
        <hr />
        <button onClick={this.push}>push</button>
        <ul>
          {this.state.arr.map(item =>
            <li key={item.id}>{item.id}</li>
          )}
        </ul>
        <hr />
        接收props参数: {this.props.text}
      </div>
    )
  }
}


export default Child2