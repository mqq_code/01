import React from 'react'

interface Props {
  count: number;
  sub: (n: number) => void;
}

const Child1: React.FC<Props> = (props) => {
  return (
    <div>
      <h2>Child1</h2>
      <p>count: {props.count}</p>
      <button onClick={() => {
        props.sub(2)
      }}>-</button>
    </div>
  )
}

export default Child1