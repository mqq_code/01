import React, { useEffect, useMemo, useRef, useState } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2';
// .ts 文件只能写 ts 代码，不可以使用 jsx 语法
// .tsx 文件，支持写jsx语法

interface ArrItem {
  id: number;
  text: string;
  date: string;
}

const App = () => {
  const [count, setCount] = useState(0)
  const [num, setNum] = useState<number | string>(10)
  const [arr, setArr] = useState<ArrItem[]>([])
  const [inp, setInp] = useState('')
  const h1Ref = useRef<HTMLHeadingElement>(null)

  const change = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInp(e.target.value)
  }
  const submit = () => {
    setArr([...arr, {
      id: Date.now(),
      text: inp,
      date: new Date().toLocaleString()
    }])
    setInp('')
  }
  const keyDown = (e: React.KeyboardEvent) => {
    // console.log((e.target as HTMLInputElement).value)
    if (e.keyCode === 13) {
      submit()
    }
  }

  useEffect(() => {
    console.log(h1Ref.current?.innerHTML)
  }, [])

  // 根据return的数据反推变量类型
  const test = useMemo(() => {
    return arr.map(item => item.date)
  }, [])

  const sub = (n: number) => {
    setCount(count - n)
  }

  return (
    <div>
      <h1 ref={h1Ref}>App</h1>
      <button onClick={() => setCount(count + 1)}>+</button> {count}
      <hr />
      <input type="text" value={inp} onChange={change} onKeyDown={keyDown}/>
      <button onClick={submit}>添加</button>
      <ul>
        {arr.map(item =>
          <li key={item.id}>{item.text} - {item.date}</li>
        )}
      </ul>
      <hr />
      <Child1 count={count} sub={sub} />
      <hr />
      <Child2 text={inp} />
    </div>
  )
}

export default App