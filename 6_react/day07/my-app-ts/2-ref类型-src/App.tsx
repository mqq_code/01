import React, { useEffect, useMemo, useRef, useState } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'

// .ts 文件只能写 ts 代码，不可以使用 jsx 语法
// .tsx 文件，支持写jsx语法

const App = () => {
  const [count, setCount] = useState(0)
  const h1Ref = useRef<HTMLHeadingElement>(null)
  const Child1Ref = useRef(null)
  const Child2Ref = useRef<Child2>(null)
  const isDown = useRef<string>('aaaaa')

  useEffect(() => {
    console.log(isDown.current)
  }, [])

  return (
    <div>
      <h1 ref={h1Ref}>App</h1>
      <button onClick={() => setCount(count + 1)}>+</button> {count}
      <div>
        <button onClick={() => {
          console.log('获取class组件实例', Child2Ref.current)
          Child2Ref.current?.add()
        }}>获取Child2组件实例</button>
      </div>
      <hr />
      <Child1 />
      <hr />
      <Child2 ref={Child2Ref} />
    </div>
  )
}

export default App