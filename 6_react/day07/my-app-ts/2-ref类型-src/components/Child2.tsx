import React, { Component } from 'react'

interface Props {
}

interface ArrItem {
  id: number
}
interface State {
  num: number;
  arr: ArrItem[];
}


class Child2 extends Component<Props, State> {

  state = {
    num: 0,
    arr: [] as ArrItem[]
  }
  push = () => {
    this.setState({
      arr: [...this.state.arr, { id:  Math.random() }]
    })
  }
  add = () => {
    this.setState({ num: this.state.num + 1 })
  }
  render() {
    return (
      <div className='box'>
        <h2>Child2</h2>
        <p>num: {this.state.num}</p>
        <button onClick={this.add}>+</button>
        <hr />
        <button onClick={this.push}>push</button>
        <ul>
          {this.state.arr.map(item =>
            <li key={item.id}>{item.id}</li>
          )}
        </ul>
      </div>
    )
  }
}


export default Child2