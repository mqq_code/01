import React from 'react'

interface Props {
}

const Child1: React.FC<Props> = (props) => {
  return (
    <div className='box'>
      <h2>Child1</h2>

    </div>
  )
}

export default Child1