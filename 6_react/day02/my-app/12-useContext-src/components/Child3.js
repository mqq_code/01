import React, { useContext } from 'react'
import ctx from '../context/ctx'

const Chil3 = () => {
  // 接收context对象中传入的数据
  const value = useContext(ctx)

  console.log(value)
  return (
    <div className='box'>
      <h2>Chil3</h2>
      <p>app的num: {value.num}</p>
      <button onClick={() => value.changeNum(2)}>num+</button>
    </div>
  )
}

export default Chil3