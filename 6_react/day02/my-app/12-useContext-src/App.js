import React, { useCallback, useState } from 'react'
import Child from './components/Child'
import ctx from './context/ctx'

const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('标题')
  const changeNum = useCallback(n => {
    setNum(num + n)
  }, [num])
  return (
    <ctx.Provider value={{ num, changeNum }}>
      <div>
        <h1>{title}</h1>
        <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
        <p>num: {num}</p>
        <button onClick={() => changeNum(1)}>num+</button>
        <Child />
      </div>
    </ctx.Provider>
  )
}

export default App

