import React, { useState, useRef } from 'react'


const App = () => {
  const [count, setCount] = useState(0)
  // useRef: 存和组件更新无关的数据，数据改变页面不会更新，ref对象地址在整个组件的生命周期中始终指向同一个地址
  let timer = 0 // 普通变量：组件更新时会重置
  let timer1 = useRef(0) // ref变量：组件更新时不会重置
  let timer2 = useRef(null)
  const start = () => {
    timer2.current = setInterval(() => {
      timer++
      timer1.current++
      setCount(c => c + 1)
    }, 1000)

  }
  const stop = () => {
    clearInterval(timer2.current)
  }
  console.log('timer', timer)
  console.log('timer1', timer1)
  return (
    <div>
      <h1>useRef</h1>
      <p>count: {count}</p>
      <button onClick={start}>开始</button>
      <button onClick={stop}>暂停</button>
    </div>
  )
}

export default App