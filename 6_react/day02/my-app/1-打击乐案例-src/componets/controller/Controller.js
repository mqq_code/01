import React, { Component } from 'react'
import style from './controller.module.scss'
import Switch from '../switch/Switch'
import Volume from '../volume/Volume'

class Controller extends Component {
  render() {
    const { power, title, changePower, bank, changeBank, changeVolume } = this.props
    return (
      <div className={style.controller}>
        <Switch checked={power} onChange={changePower}>power</Switch>
        <h3>{title}</h3>
        <Volume onChange={changeVolume} defaultValue={1} />
        <Switch checked={bank} onChange={changeBank}>bank</Switch>
      </div>
    )
  }
}

export default Controller