import React, { Component, createRef } from 'react'
import style from './volume.module.scss'

 class Volume extends Component {
  state = {
    left: 0
  }
  volumeRef = createRef()
  volumeLeft = null // 父元素到屏幕的距离
  maxLeft = null // 最大移动距离
  isDown = false
  mouseDown = () => {
    this.isDown = true
    console.log('按下')
  }
  mouseMove = (e) => {
    if (this.isDown) {
      // 鼠标X位置 - 父元素X位置
      let left = e.clientX - this.volumeLeft - 5
      // 处理边界
      if (left < 0) left = 0
      if (left > this.maxLeft) left = this.maxLeft
      this.setState({ left })
      // 通知父组件
      this.props?.onChange(left / this.maxLeft)
    }
  }
  mouseUp = () => {
    this.isDown = false
    console.log('抬起')
  }
  componentDidMount() {
    const { left, width } =  this.volumeRef.current.getBoundingClientRect()
    this.volumeLeft = left
    this.maxLeft = width - 10
    this.setState({ left: this.props.defaultValue * this.maxLeft })
    document.addEventListener('mousemove', this.mouseMove)
    document.addEventListener('mouseup', this.mouseUp)
  }
  componentWillUnmount() {
    document.removeEventListener('mousemove', this.mouseMove)
    document.removeEventListener('mouseup', this.mouseUp)
  }
  render() {
    const { left } = this.state
    return (
      <div className={style.volume} ref={this.volumeRef}>
        <span style={{ left: left + 'px' }} onMouseDown={this.mouseDown}></span>
      </div>
    )
  }
}


export default Volume