import React, { Component } from 'react'
import style from './switch.module.scss'

class Switch extends Component {
  render() {
    const { checked, children, onChange } = this.props
    return (
      <div className={style.switch}>
        <h4>{children}</h4>
        <p className={checked ? style.on : ''} onClick={() => onChange(!checked)}>
          <span></span>
        </p>
      </div>
    )
  }
}

export default Switch
