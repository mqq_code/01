import React, { Component } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'

class Keyboard extends Component {

  play = (keyCode, isDown) => {
    const current = this.props.list.find(v => v.keyCode === keyCode)
    if (current) {
      this.props.play(current, isDown)
    }
  }

  keydown = (e) => this.play(e.keyCode, true)
  keyup = (e) => this.play(e.keyCode, false)

  componentDidMount() {
    document.addEventListener('keydown', this.keydown)
    document.addEventListener('keyup', this.keyup)
  }

  componentWillUnmount() {
    console.log('组件销毁之前清除全局事件')
    document.removeEventListener('keydown', this.keydown)
    document.removeEventListener('keydup', this.keyup)
  }

  render() {
    const { power, list, play } = this.props
    return (
      <div className={style.keyboard}>
        {list.map(item =>
          <p
            className={classNames({
              [style.isDown]: item.isDown,
              [style.on]: item.isDown && power
            })}
            key={item.id}
            onMouseDown={() => play(item, true)}
            onMouseUp={() => play(item, false)}
          >{item.keyTrigger}</p>
        )}
      </div>
    )
  }
}

export default Keyboard
