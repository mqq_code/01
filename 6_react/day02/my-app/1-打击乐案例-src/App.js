import React, { Component, createRef } from 'react'
import Keyboard from './componets/keyboard/Keyboard'
import Controller from './componets/controller/Controller'
import axios from 'axios'

class App extends Component {

  state = {
    list: [],
    curMusic: null, // 当前播放的音乐
    power: true, // 音乐开关
    bank: false // 音乐风格
  }
  audioRef = createRef()
  // 播放
  play = (item, isDown) => {
    // 拷贝数据，根据id找下表，修改点击状态
    const newList = [...this.state.list]
    const index = newList.findIndex(v => v.id === item.id)
    newList[index].isDown = isDown
    // 更新列表数据
    this.setState({
      list: newList,
      curMusic: this.state.power ? newList[index] : null
    }, () => {
      // 如果按下并且音乐开关打开就等页面更新之后播放音乐
      if (isDown && this.state.power) {
        this.audioRef.current.load()
        this.playMusic()
      }
    })
  }
  playMusic = () => {
    // 捕获播放报错，报错时重新调用，直到成功为止
    this.audioRef.current.play().catch(e => {
      this.playMusic()
    })
  }
  // 音乐开关
  changePower = (checked) => {
    this.setState({
      power: checked,
      curMusic: checked ? this.state.curMusic : null
    })
  }
  changeBank = (checked) => {
    this.setState({
      bank: checked
    })
  }
  getlist = async () => {
    const res = await axios.get('http://zyxcl.xyz/exam_api/music/list')
    console.log(res.data.value)
    this.setState({ list: res.data.value })
  }
  componentDidMount() {
    this.getlist()
  }
  changeVolume = (p) => {
    console.log('当前音量', p)
    this.audioRef.current.volume = p
  }

  componentDidCatch(e) {
    console.log(e)
  }

  render() {
    const { curMusic, list, power, bank } = this.state
    const url = bank ? curMusic?.bankUrl : curMusic?.url
    return (
      <div className='wrap'>
        <Keyboard
          list={list}
          play={this.play}
          power={power}
        />
        <Controller
          title={curMusic?.id}
          power={power}
          changePower={this.changePower}
          bank={bank}
          changeBank={this.changeBank}
          changeVolume={this.changeVolume}
        />
        <audio ref={this.audioRef} src={url}></audio>
      </div>
    )
  }
}

export default App