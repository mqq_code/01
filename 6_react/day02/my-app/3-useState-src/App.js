import React, { useState } from 'react'
// hook: 让函数组件可以实现class组件的功能，hook是名称以 use 开头的函数
// hook使用规则：
// 必须在函数组件的最顶层使用，不能在if、for、子函数使用，因为react是按照hook的调用顺序保存组件的状态，如果放在if、for...中会导致状态顺序不正确
// 只能在函数组件和自定义hook中使用，不能再class组件和普通函数中使用

// useState
// 作用：定义函数组件中的状态
// 用法: const [state，setState] = useState(初始值)
// 1. setState是异步执行的，用传入值覆盖上一次数据
// 2. 连续多次调用会合并更新，只渲染一次
// 3. setState可以传入函数，函数的参数就是最新的数据，函数return的数据会覆盖原数据



const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('title')
  const [arr, setArr] = useState([])
  const [obj, setObj] = useState({ name: '小明', age: 20 })

  const addNum = () => {
    setNum(n => n + 1)
    setNum(n => n + 1)
    setNum(n => n + 1)
    console.log(num)
  }

  const changeTitle = e => {
    setTitle(e.target.value)
  }
  const add = () => {
    const newArr = [...arr]
    newArr.push(Math.random())
    setArr(newArr)
    // setArr([...arr, Math.random()])
  }
  const remove = index => {
    const newArr = [...arr]
    newArr.splice(index, 1)
    setArr(newArr)
  }
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={changeTitle} />
      <button onClick={() => setNum(num - 1)}>-</button>
      {num}
      <button onClick={addNum}>+</button>
      <hr />
      <button onClick={add}>添加</button>
      <ul>
        {arr.map((item, index) =>
          <li key={item}>
            {item}
            <button onClick={() => remove(index)}>删除</button>
          </li>
        )}
      </ul>
      <hr />
      <button onClick={() => {
        setObj({ ...obj, name: Math.random() })
      }}>修改name</button>
      <button onClick={() => setObj({ ...obj, age: obj.age + 1 })}>修改age</button>
      <p>{JSON.stringify(obj)}</p>
    </div>
  )
}

export default App
