import React, { useState, useRef } from 'react'


const App = () => {
  const [title, setTitle] = useState('默认标题')
  const inp = useRef(null)

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" ref={inp} />
      <button onClick={() => {
        // 获取dom元素
        console.log(inp.current)
      }}>修改title</button>
    </div>
  )
}

export default App