import React, { useState, useImperativeHandle, forwardRef } from 'react'

const Child = (props, ref) => {
  // 2. ref: 父组件传入的ref对象
  const [num, setNum] = useState(0)
  const add = () => {
    setNum(num + 1)
  }

  // 3. 使用此hook给父组件传入的ref的对象添加数据
  useImperativeHandle(ref, () => {
    // 把返回值添加给ref的current属性
    return {
      num,
      add
    }
  }, [num])


  return (
    <div className='box'>
      <h2>Child</h2>
      <button onClick={add}>+</button>
      {num}
    </div>
  )
}

// 1. 把父组件传入的 ref 对象通过 forwardRef 转发给Child组件
export default forwardRef(Child)