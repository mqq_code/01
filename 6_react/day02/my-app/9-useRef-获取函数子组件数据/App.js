import React, { useState, useRef } from 'react'
import Child from './components/Child'

// 通过ref调用函数子组件的数据和方法
// 1. 子组件通过 forwardRef 把父组件传入的 ref 对象转发给 Child 组件
// 2. Child 组件通过第二个参数获取转发过来的 ref 对象
// 3. 使用 useImperativeHandle 给父组件传入的ref的对象添加数据
// useImperativeHandle(父元素的ref, callback, [依赖项])
/*
  useImperativeHandle(ref, () => {
    // 把返回值添加给 ref 的current属性
    return {
      num,
      add
    }
  }, [num])
*/

const App = () => {
  const ChildRef = useRef(12345)
  console.log(ChildRef)
  return (
    <div>
      <h1>useRef</h1>
      <button onClick={() => {
        // 调用子组件实例对象
        console.log(ChildRef.current)
        ChildRef.current.add()
      }}>调用子组件实例</button>
      <Child ref={ChildRef} />
    </div>
  )
}

export default App