import React, { useMemo, useState } from 'react'

// useMemo
// 作用：返回一个缓存的值
// 用法：useMemo(callback, [依赖项数组])
// 依赖项数组改变时执行callback函数，缓存callback return的数据
// 依赖项没有改变从缓存读取数据

const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('标题')

  const len = useMemo(() => {
    console.log('useMemo')
    return num + title.length
  }, [title, num])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <p>num: {num}</p>
      <button onClick={() => setNum(num + 1)}>num+</button>
      <p>len: {len}</p>
    </div>
  )
}

export default App