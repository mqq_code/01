import React, { useState, useEffect } from 'react'
import axios from 'axios'



const App = () => {
  const [banner, serBanner] = useState([])

  // 错误写法：
  // useEffect 的第一个参数需要return一个函数，async 函数默认返回Promise对象
  // useEffect(async () => {
  //   const res = await axios.get('http://zyxcl.xyz/music_api/banner')
  //   console.log(res.data)
  //   serBanner(res.data.banners)
  //   console.log('useEffect')
  // }, [])

  // 正确用法
  const getBanner = async () => {
    const res = await axios.get('http://zyxcl.xyz/music_api/banner')
    console.log(res.data)
    serBanner(res.data.banners)
  }
  useEffect(() => {
    getBanner()
  }, [])

  return (
    <div>App</div>
  )
}

export default App