import React, { useState, useRef } from 'react'

class Child extends React.Component {

  state = {
    num: 10
  }
  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }
  render() {
    return (
      <div style={{ border: '1px solid' }}>
        <h2>Child</h2>
        num: {this.state.num}
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}


const App = () => {
  const ChildRef = useRef(null)

  return (
    <div>
      <h1>useRef</h1>
      <button onClick={() => {
        // 调用子组件实例对象
        console.log(ChildRef.current)
        ChildRef.current.add()
      }}>调用子组件实例</button>
      <Child ref={ChildRef} />
    </div>
  )
}

export default App