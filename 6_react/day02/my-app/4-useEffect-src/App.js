import React, { useState, useEffect } from 'react'
// hook: 让函数组件可以实现class组件的功能，hook是名称以 use 开头的函数
// hook使用规则：
// 必须在函数组件的最顶层使用，不能在if、for、子函数使用，因为react是按照hook的调用顺序保存组件的状态，如果放在if、for...中会导致状态顺序不正确
// 只能在函数组件和自定义hook中使用，不能再class组件和普通函数中使用

// useState
// 作用：定义函数组件中的状态
// 用法: const [state，setState] = useState(初始值)
// 1. setState是异步执行的，用传入值覆盖上一次数据
// 2. 连续多次调用会合并更新，只渲染一次
// 3. setState可以传入函数，函数的参数就是最新的数据，函数return的数据会覆盖原数据
// useEffect
// 作用: 处理组件副作用(例如：调接口，定时器，原生事件...)，可以实现类似class组件中生命周期的功能
// 用法: useEffect(callback, [依赖项])
// 1. callback 在组件挂载或者更新后执行，可以获取到最新的dom
// 2. 只传callback一个参数，主要组件中有数据更新就会执行 callback，类似 componentDidUpdate
// 3. 依赖项传入空数组，callback只执行一次，类似 componentDidMount
// 4. 依赖项数组中传入具体数据，callback 在依赖项更新时执行
// 5. callback 中 return 的函数会在组件销毁时执行，类似 componentWillUnmount

const Child = (props) => {
  const [count, setCount] = useState(10)

  useEffect(() => {
    // console.log('Child组件挂载成功')
    const timer = setInterval(() => {
      // 无法获取到最新值
      // setCount(count - 1)
      setCount(c => {
        // 在定时器中修改数据，必须给setState传入一个函数获取最新值
        return c - 1
      })
    }, 1000)

    return () => {
      // Child组件销毁之前执行次函数，清除异步任务
      // console.log('Child组件销毁之前')
      clearInterval(timer)
    }
  }, [])

  return <div>
    倒计时: {count}
  </div>
}

const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('title')
  const [obj, setObj] = useState({ name: '小明', age: 20 })

  const changeTitle = e => {
    setTitle(e.target.value)
  }

  useEffect(() => {
    // console.log('useEffect', document.querySelector('h1').innerHTML)
  })

  useEffect(() => {
    console.log('传入空数组', num)
  }, [])

  useEffect(() => {
    console.log('obj.age改变了', obj.age)
  }, [obj])
  

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={changeTitle} />
      <button onClick={() => setNum(num - 1)}>-</button>
      {num}
      <button onClick={() => setNum(num + 1)}>+</button>
      <hr />
      <button onClick={() => setObj({ ...obj, name: Math.random() })}>修改name</button>
      <button onClick={() => setObj({ ...obj, age: obj.age + 1 })}>修改age</button>
      <p>{JSON.stringify(obj)}</p>
      <hr />
      {num > 0 && <Child a="100" />}
    </div>
  )
}

export default App
