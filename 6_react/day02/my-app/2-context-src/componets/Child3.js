import React, { Component } from 'react'
import { Consumer } from '../context/nunCtx'

class Child3 extends Component {

  renderDom = value => {
    return <div className='box'>
      <h3>Child3</h3>
      <p>app的num: {value.num}</p>
      <button onClick={() => value.changeNum(-2)}>-2</button>
    </div>
  }

  render() {
    return (
      <Consumer>
        {this.renderDom}
      </Consumer>
    )
  }
}

export default Child3
