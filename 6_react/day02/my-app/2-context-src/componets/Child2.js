import React, { Component } from 'react'
import Child3 from './Child3'

export default class Child2 extends Component {
  render() {
    return (
      <div className='box'>
        <h2>Child2</h2>
        <Child3 />
      </div>
    )
  }
}
