import React, { Component } from 'react'
import Child1 from './componets/Child1'
import ctx, { Provider } from './context/nunCtx'

console.log(ctx)

class App extends Component {

  state = {
    num: 0
  }
  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }
  render() {
    const value = {
      num: this.state.num,
      changeNum: this.changeNum
    }
    return (
      <Provider value={value}>
        <div>
          <h1>App</h1>
          <button onClick={() => this.changeNum(1)}>+1</button>
          <p>num: {this.state.num}</p>
          <Child1 />
        </div>
      </Provider>
    )
  }
}

export default App