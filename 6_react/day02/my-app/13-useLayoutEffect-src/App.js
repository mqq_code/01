import React, { useCallback, useState, useEffect, useLayoutEffect } from 'react'

const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('标题')
  const changeNum = useCallback(n => {
    setNum(num + n)
  }, [num])

  // useEffect(() => {
  //   // 页面渲染完成之后执行
  //   console.log('useEffect', num, document.querySelector('p').outerHTML)
  //   if (num === 'aaaaaaaaaskdflkaslkfasldjfalkdsjfklasdjflkasfjaklsdjlfalj') {
  //     setNum(Math.random())
  //   }
  // }, [num])

  useLayoutEffect(() => {
    // dom构建完成，页面渲染之前执行
    console.log('useLayoutEffect', num, document.querySelector('p').outerHTML)
    if (num === 'aaaaaaaaaskdflkaslkfasldjfalkdsjfklasdjflkasfjaklsdjlfalj') {
      setNum(Math.random())
    }
  }, [num])


  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <p>num: {num}</p>
      <button onClick={() => changeNum(1)}>num+</button>
      <button onClick={() => setNum('aaaaaaaaaskdflkaslkfasldjfalkdsjfklasdjflkasfjaklsdjlfalj')}>修改num</button>
    </div>
  )
}

export default App

