import React, { useState, memo } from 'react'

const Child = (props) => {
  const [count, setCount] = useState(100)
  console.log('child渲染了', props)

  return (
    <div className='box'>
      <h2>Child</h2>
      <p>count: {count}</p>
      <button onClick={() => setCount(count - 1)}>count--</button>
      <hr />
      <p>父组件的num:
        {props.num}
        <button onClick={() => props.changeNum(2)}>+</button>
      </p>
    </div>
  )
}

// memo作用: 
// 比较最新的props和上一次的props中每一项属性是否一致，
// 如果全都一致就阻止组件更新
// 有不一致的数据就允许更新组件
export default memo(Child)

// 手动比较props是否有更新
// export default memo(Child, (prevProps, props) => {
//   // prevProps: 上一次的props
//   // props: 最新的props
//   // return true 不更新
//   // return false 更新
//   return props.num === 2
// })
