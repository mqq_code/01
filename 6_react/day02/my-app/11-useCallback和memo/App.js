import React, { useCallback, useState } from 'react'
import Child from './components/Child'
// useCallback
// 作用：返回一个缓存的函数
// 用法：useCallback(callback, [依赖项数组])
// 依赖项数组改变时创建新函数
// 依赖项没有改变从缓存读取函数
// useCallback 配置 memo 优化子组件性能，减少子组件不必要的更新


const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('标题')
  // 缓存函数，配合子组件memo使用
  const changeNum = useCallback(n => {
    setNum(num + n)
  }, [num])
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <p>num: {num}</p>
      <button onClick={() => changeNum(1)}>num+</button>
      <Child num={num} changeNum={changeNum} />
    </div>
  )
}

export default App



// 基本用法
// const arr = []
// const App = () => {
//   const [num, setNum] = useState(0)
//   const [title, setTitle] = useState('标题')
//   const add = useCallback(() => {
//     setNum(num + 1)
//   }, [num])
//   if (!arr.includes(add)) {
//     arr.push(add)
//   }
//   console.log(arr)
//   return (
//     <div>
//       <h1>{title}</h1>
//       <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
//       <p>num: {num}</p>
//       <button onClick={add}>num+</button>
//     </div>
//   )
// }
// export default App