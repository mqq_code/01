import React from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'


const App = () => {
  return (
    <div>
      <h1>App</h1>
      <Child1 />
      <Child2 />
    </div>
  )
}

export default App