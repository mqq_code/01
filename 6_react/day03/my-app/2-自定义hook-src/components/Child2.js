import React, { useState, useRef, useEffect } from 'react'
import { usePos } from '../hooks/usePos'
import { useRequest } from '../hooks/useRequest'
import { searchApi } from '../api'
import _ from 'lodash'

const useCounter = (num) => {
  const [count, setCount] = useState(num)
  const timer = useRef(null)
  const start = () => {
    timer.current = setInterval(() => {
      setCount(c => {
        if (c - 1 === 0) {
          stop()
        }
        return c - 1
      })
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer.current)
  }
  const reset = () => {
    stop()
    setCount(num)
  }

  useEffect(() => {
    return stop
  }, [])

  return {
    count,
    start,
    stop,
    reset
  }
}


const Child2 = () => {
  // const pos = usePos()
  const { data, loading, run } = useRequest(searchApi, '刘德华', false)
  // console.log(data, loading)
  const { count, start, stop, reset } = useCounter(10)



  return (
    <div className='box'>
      <h2>Child2</h2>
      <p>倒计时: {count}
        <button onClick={start}>开始</button>
        <button onClick={stop}>暂停</button>
        <button onClick={reset}>重置</button>
      </p>

      <input type="text" placeholder="搜索歌曲" onChange={_.debounce(e => run(e.target.value), 500)} />
      {loading ? <div className="loading">加载中</div>
      :
        <ul>
          {data?.result.songs?.map(item =>
            <li key={item.id}>{item.name}</li>
          )}
        </ul>
      }
      {/* pos.x: {pos.x} */}
    </div>
  )
}

export default Child2