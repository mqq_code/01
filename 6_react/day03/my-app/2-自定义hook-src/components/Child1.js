import React, { useState, useEffect } from 'react'
import { usePos } from '../hooks/usePos'
import { getTopList } from '../api'
import { useRequest } from '../hooks/useRequest'

const Child1 = () => {
  // 使用获取鼠标位置的自定义hook
  // const pos = usePos()
  // 调用接口的自定义hook
  const { data, loading, run } = useRequest(getTopList)

  return (
    <div className='box'>
      <button onClick={run}>刷新接口</button>
      {loading ?
        <div className="loading">loading...</div>
      :
        <>
          <h2>Child1</h2>
          {/* <p>pos: {JSON.stringify(pos)}</p> */}
          <ul>
            {data?.list.map(item =>
              <li key={item.id}>
                <h3>{item.name}</h3>
                <img width={100} height={100} src={item.coverImgUrl} alt="" />
              </li>
            )}
          </ul>
        </>
      }
      
    </div>
  )
}

export default Child1