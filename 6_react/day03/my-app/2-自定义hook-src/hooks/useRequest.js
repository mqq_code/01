import { useState, useEffect } from 'react'

export const useRequest = (request, params = null, immediate = true) => {

  const [data, setData] = useState(null)
  const [loading, setLoading] = useState(false)

  const run = async (runParams) => {
    setLoading(true)
    const res = await request(runParams)
    setData(res.data)
    setTimeout(() => {
      setLoading(false)
    }, 2000)
  }

  useEffect(() => {
    if (immediate) {
      run(params)
    }
  }, [])

  return {
    data,
    loading,
    run
  }
}