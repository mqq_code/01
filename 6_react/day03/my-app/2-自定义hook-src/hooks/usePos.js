import { useState, useEffect } from 'react'

// 自定义hook：
// 作用：抽离组件总的公用逻辑，减少重复代码方便复用和后期维护
// 1. 名字以 use 开头的函数
// 2. 自定义 hook 内可以使用其他 hook
export const usePos = () => {

  const [pos, setPos] = useState({ x: 0, y: 0 })
  useEffect(() => {
    const mouseMove = e => {
      setPos({
        x: e.clientX,
        y: e.clientY
      })
    }
    document.addEventListener('mousemove', mouseMove)
    return () => {
      document.removeEventListener('mousemove', mouseMove)
    }
  }, [])


  return pos
}