import React, { useState, useEffect, useMemo } from 'react'
import style from './select.module.scss'
import classNames from 'classnames'

const Select = (props) => {
  // 是否展示下拉列表
  const [showOptions, setShowOptions] = useState(false)
  // 划过的高亮下标
  const [hoverIndex, setHoverIndex] = useState(-1)
  // 定义下拉列表数据
  const [options, setOptions] = useState(props.options)
  // 选中的数据
  const [selected, setSelected] = useState([])
  // 搜索内容
  const [keyword, setKeyword] = useState('')

  // 点击页面隐藏列表
  useEffect(() => {
    const handleClick = () => {
      setShowOptions(false)
    }
    document.addEventListener('click', handleClick)
    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  // 隐藏下拉列表删除划过高亮
  useEffect(() => {
    if (!showOptions) {
      setHoverIndex(-1)
    }
  }, [showOptions])

  // 初始化，监听传入的参数，筛选默认选中的数据
  useEffect(() => {
    setSelected(props.options.filter(v => v.checked))
    setOptions(props.options)
  }, [props.options])

  // 监听选中的数据修改，通知父组件选中数据更新
  useEffect(() => {
    const values = selected.map(v => ({ value: v.value, label: v.label }))
    props.onChange && props.onChange(values)
  }, [selected])

  // 修改选中状态
  const changeChecked = value => {
    // 拷贝数据，根据value查找下标，修改选中
    const newOptions = [...options]
    const index = options.findIndex(v => v.value === value)
    newOptions[index].checked = !newOptions[index].checked
    // 更新数据
    setOptions(newOptions)
    // 修改选中的数据
    if (newOptions[index].checked) {
      setSelected([...selected, newOptions[index]])
    } else {
      const newSelected = [...selected]
      const index = selected.findIndex(v => v.value === value)
      newSelected.splice(index, 1)
      setSelected(newSelected)
    }
  }

  // 根据输入框内容搜索数据
  const filterList = useMemo(() => {
    return options.filter(v => v.label.includes(keyword))
  }, [keyword, options])

  return (
    <div
      className={classNames(style.select, { [style.active]: showOptions })}
      onClick={(e) => {
        e.stopPropagation()
        setShowOptions(!showOptions)
      }}
    >
      <div className={style.selected}>
        {selected.length === 0 && <div className={style.placeholder}>{props.placeholder}</div>}
        {selected.map(item =>
          <p key={item.value}>
            {item.label}
            <span onClick={e => {
              e.stopPropagation()
              changeChecked(item.value)
            }}>x</span>
          </p> 
        )}
        <div className={style.sanjiao}></div>
      </div>
      <div className={style.options} onClick={e => e.stopPropagation()}>
        <div className={style.search}>
          <input type="text" placeholder='搜索' value={keyword} onChange={e => setKeyword(e.target.value)} />
        </div>
        <div className={style.list}>
          {filterList.map((item, index) =>
            <div
              key={item.value}
              className={classNames(style.listItem, {[style.hoverActive]: hoverIndex === index})}
              onMouseEnter={() => setHoverIndex(index)}
              onClick={() => changeChecked(item.value)}
            >
              <i className={classNames({ [style.active]: item.checked })}></i>
              {item.label} - {item.value}
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default Select