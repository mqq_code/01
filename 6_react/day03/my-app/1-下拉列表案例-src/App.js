import React, { useState, useEffect } from 'react'
import Select from './components/select/Select'
import axios from 'axios'

const hobby = [
  { label: '唱歌', value: 'changge', checked: true },
  { label: '跳舞', value: 'tiaowu' },
  { label: 'rap', value: 'rap' },
  { label: '篮球', value: 'lanqiu', checked: true }
]

const App = () => {
  const [form, setForm] = useState({ city: [], hobby: [] })
  const [cityOptions, setCityOptions] = useState([])
  const getCities = async () => {
    const res = await axios.get('https://m.maizuo.com/gateway?k=9074713', {
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033"}',
        'X-Host': 'mall.film-ticket.city.list'
      }
    })
    setCityOptions(res.data.data.cities.map(v => ({ label: v.name, value: v.cityId })))
  }

  useEffect(() => {
    getCities()
  }, [])

  return (
    <div className="app">
      <h2>App</h2>
      <div>
        选择城市：
        <Select
          placeholder="请选择城市"
          options={cityOptions}
          onChange={values => setForm({ ...form, city: values })}
        />
      </div>
      <div>
        选择爱好：<Select placeholder="请选择爱好" options={hobby} onChange={values => setForm({ ...form, hobby: values })} />
      </div>
      <button>提交</button>
      {JSON.stringify(form)}
    </div>
  )
}

export default App