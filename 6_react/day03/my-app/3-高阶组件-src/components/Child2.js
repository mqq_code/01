import React, { Component } from 'react'
import withCounter from '../hoc/withCounter'

class Child2 extends Component {
  render() {
    console.log(this.props)
    return (
      <div className='box'>
        <h2>Child2</h2>
        <p>倒计时：{this.props.count}</p>
        <button onClick={this.props.start}>开始</button>
        <button onClick={this.props.stop}>结束</button>
      </div>
    )
  }
}

// 使用高阶组件，给Child添加功能
export default withCounter(Child2)