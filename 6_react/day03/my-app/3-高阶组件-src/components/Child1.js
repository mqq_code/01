import React, { Component } from 'react'
import withCounter from '../hoc/withCounter'

const Child1 = (props) => {
  return (
    <div className='box'>
      <h2>Child1 - {props.title}</h2>
      counter: {props.count}
      <button onClick={props.start}>start</button>
    </div>
  )
}

export default withCounter(Child1)
