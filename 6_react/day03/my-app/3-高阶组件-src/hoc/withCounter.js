import React, { Component } from 'react'

// 高阶组件
// 概念：接收一个组件作为参数，返回一个新组件的函数
// 作用：抽离组件公用逻辑，减少组件代码

function withCounter(Com) {
  class Counter extends Component {

    state = {
      count: 10
    }
  
    timer = null
    start = () => {
      this.timer = setInterval(() => {
        this.setState({ count: this.state.count - 1 })
        console.log(this.state.count - 1)
      }, 1000)
    }
    stop = () => {
      clearInterval(this.timer)
    }
  
    componentWillUnmount() {
      this.stop()
    }
    
    render() {
      const props = {
        count: this.state.count,
        start: this.start,
        stop: this.stop,
        // 告诫组件接收到父组件参数后，给原本的组件传回去
        ...this.props
      }
      console.log(this.props)
      return (
        <div style={{ border: '1px solid red' }}>
          <Com {...props} />
        </div>
      )
    }
  }
  return Counter
}

export default withCounter