import React, { useState } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import withCounter from './hoc/withCounter'

const App = () => {
  const [show, setShow] = useState(true)
  return (
    <div>
      <h1>App</h1>
      <Child1 title="我是传给Child1的标题" />
      <button onClick={() => setShow(!show)}>toggle</button>
      {show && <Child2 />}
    </div>
  )
}

export default App