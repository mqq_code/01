import axios from "axios";


export const getTopList = () => {
  return axios.get('https://zyxcl.xyz/music_api/toplist')
}

export const searchApi = (keywords) => {
  return axios.get('https://zyxcl.xyz/music_api/search', {
    params: {
      keywords
    }
  })
}