import React from 'react'
import { useLocation, useParams } from 'react-router-dom'

const Detail = () => {
  const location = useLocation()
  const params = useParams()
  // 参数显示在url
  console.log('？后的参数', location.search)
  console.log('动态路由参数', params)
  // 参数不在url显示
  console.log('state参数', location.state)
  return (
    <div>Detail</div>
  )
}

export default Detail