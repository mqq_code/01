import React from 'react'
import { useHistory } from 'react-router-dom'

const Child2 = () => {
  const history = useHistory()

  return (
    <div>
      <h2>Child2</h2>
      <button onClick={() => {
        // history.push('/detail')
        // history.push('/detail?a=100&b=200')
        history.push('/detail/123?a=100&b=200', {
          test: '测试一下',
          arr: [1,2,3,4,5]
        })
      }}>跳转到详情页面</button>
    </div>
  )
}

export default Child2