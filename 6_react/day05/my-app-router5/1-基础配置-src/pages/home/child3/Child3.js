import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class Child3 extends Component {
  render() {
    // 获取路由信息
    console.log(this.props)
    return (
      <div>Child3</div>
    )
  }
}


// const Child3 = (props) => {
//   console.log(props.history)
//   console.log(props.location)
//   return (
//     <div>Child3</div>
//   )
// }

// withRouter: 高阶组件
// 作用：把路由信息传给Child组件的props
export default withRouter(Child3)