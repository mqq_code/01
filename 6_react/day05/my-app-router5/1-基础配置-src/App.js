import React from 'react'
import Home from './pages/home/Home'
import Child1 from './pages/home/child1/Child1'
import Test1 from './pages/home/child1/Test1'
import Test2 from './pages/home/child1/Test2'
import Child2 from './pages/home/child2/Child2'
import Child3 from './pages/home/child3/Child3'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import Notfound from './pages/404'
import {
  Route, // 配置路由视图
  Redirect, // 重定向
  Switch // 只渲染匹配到的第一个路由
} from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route path="/home" render={() => (
        <Home>
          <Switch>
            <Route path="/home/child1" render={() => (
              <Child1>
                <Switch>
                  <Route path="/home/child1/test1" component={Test1} />
                  <Route path="/home/child1/test2" component={Test2} />
                  <Redirect from="/home/child1" to="/home/child1/test1" />
                </Switch>
              </Child1>
            )} />
            <Route path="/home/child2" component={Child2} />
            <Route path="/home/child3" component={() => <Child3 />} />
            <Redirect from="/home" to="/home/child1" />
          </Switch>
        </Home>
      )}/>
      <Route path="/detail/:id" component={Detail} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/404" component={Notfound} />
      {/* exact: 精准匹配 */}
      <Redirect exact from="/" to="/home" />
      <Redirect to="/404" />
    </Switch>
  )
}

export default App