import React from 'react'

const menu = [
  {
    title: '菜单1', 
    children: [
      {
        title: '菜单1-1',
        children: [
          {
            title: '菜单1-1-1',
            children: [
              { title: '菜单1-1-1-1' },
              { title: '菜单1-1-1-2' },
              { title: '菜单1-1-1-3' },
              { title: '菜单1-1-1-4' }
            ]
          },
          { title: '菜单1-1-2' },
          { title: '菜单1-1-3' },
          { title: '菜单1-1-4' },
        ]
      },
      { title: '菜单1-2' },
      { title: '菜单1-3' },
      { title: '菜单1-4' },
    ]
  },
  { title: '菜单2' },
  {
    title: '菜单3',
    children: [
      { title: '菜单3-1' },
      { title: '菜单3-2' },
      { title: '菜单3-3' },
      { title: '菜单3-4' },
    ]
  },
  { title: '菜单4' },
  { title: '菜单5' },
  { title: '菜单6' }
]


const renderList = (list) => {
  return  <ul className='list'>
    {list.map(item => <li key={item.title}>
      {item.title}
      {item.children && renderList(item.children)}
    </li>)}
  </ul>
}

const App = () => {
  

  return <div>
    <h1>app</h1>
    {renderList(menu)}
  </div>
}

export default App