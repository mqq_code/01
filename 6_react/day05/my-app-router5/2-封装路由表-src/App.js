import React from 'react'
import routesConfig from './router'
import RouterView from './router/RouterView'



const App = () => {
  return <RouterView routes={routesConfig} />
}

export default App