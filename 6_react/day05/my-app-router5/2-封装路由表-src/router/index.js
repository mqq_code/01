import Home from '../pages/home/Home'
import Child1 from '../pages/home/child1/Child1'
import Test1 from '../pages/home/child1/Test1'
import Test2 from '../pages/home/child1/Test2'
import Child2 from '../pages/home/child2/Child2'
import Child3 from '../pages/home/child3/Child3'
import Detail from '../pages/detail/Detail'
import Login from '../pages/login/Login'
import Notfound from '../pages/404'

const routesConfig = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home/child1',
        component: Child1,
        children: [
          { path: '/home/child1/test1', component: Test1 },
          { path: '/home/child1/test2', component: Test2 },
          { path: '/home/chil1', to: '/home/child1/test1' }
        ]
      },
      { path: '/home/child2', component: Child2 },
      { path: '/home/child3', component: Child3 },
      { path: '/home', to: '/home/child1' }
    ]
  },
  { path: '/detail/:id', component: Detail },
  { path: '/login', component: Login, exact: true },
  { path: '/404', component: Notfound },
  { path: '/', to: '/home', exact: true },
  { path: '*', to: '/404' },
]

export default routesConfig