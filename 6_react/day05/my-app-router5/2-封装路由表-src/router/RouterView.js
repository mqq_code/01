import React from 'react'
import {
  Route, // 配置路由视图
  Redirect, // 重定向
  Switch // 只渲染匹配到的第一个路由
} from 'react-router-dom'

const RouterView = (props) => {
  return <Switch>
    {props.routes.map(item => {
      if (item.component) {
        return (
          <Route
            key={item.path}
            exact={item.exact}
            path={item.path}
            render={() => {
              return <item.component>
                {item.children && <RouterView routes={item.children} />}
              </item.component>
            }}
          />
        )
      }
      return <Redirect key={item.path} exact={item.exact} from={item.form} to={item.to} />
    })}
  </Switch>
}

export default RouterView