import React from 'react'
import {
  NavLink
} from 'react-router-dom'

const Home = (props) => {
  return (
    <div className="home">
      <main>
        {props.children}
      </main>
      <footer>
        <NavLink to="/home/child1">导航1</NavLink>
        <NavLink to="/home/child2">导航2</NavLink>
        <NavLink to="/home/child3">导航3</NavLink>
      </footer>
    </div>
  )
}

export default Home