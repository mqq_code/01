import React from 'react'
import { NavLink } from 'react-router-dom'
const Child1 = (props) => {
  return (
    <div>
      <h3>Child1</h3>
      <nav>
        <NavLink to="/home/child1/test1">test1</NavLink>
        <NavLink to="/home/child1/test2">test2</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Child1