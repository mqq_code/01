import React from 'react'
import { useStore } from '../store'

const Child3 = () => {
  const { state } = useStore()
  return (
    <div>
      <h2>Child3</h2>
      {JSON.stringify(state)}
    </div>
  )
}

export default Child3