import React from 'react'
import { useStore } from '../store'

const Child1 = () => {
  // 获取store的数据
  const { state, dispatch } = useStore()

  const onChange = (e) => {
    dispatch({
      type: 'setRootTitle',
      payload: e.target.value
    })
  }
  return (
    <div>
      <h1>{state.rootTitle}</h1>
      <input type="text" value={state.rootTitle} onChange={onChange} />
      <hr />
      <p>姓名: {state.person.name}</p>
      <button onClick={() => {
        dispatch({
          type: 'setPersonName',
          payload: Math.random()
        })
      }}>修改名字</button>
      <h2>child1</h2>
    </div>
  )
}

export default Child1