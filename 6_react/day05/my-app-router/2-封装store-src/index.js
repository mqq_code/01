import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import {
  HashRouter
} from 'react-router-dom'
import { Provider } from './store'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>
);
