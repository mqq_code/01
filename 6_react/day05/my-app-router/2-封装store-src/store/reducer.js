
export const initState = {
  rootTitle: '大标题',
  arr: [1,2,3,4,5,6,7],
  person: {
    name: '小明',
    age: 22
  }
}

// 更新数据
export const reducer = (state, action) => {
  console.log('reducer', action)
  // 根据 type 判断要如何修改数据，返回新数据
  // switch(action.type) {
  //   case 'setRootTitle':
  //     return {...state, rootTitle: action.payload}
  //   case 'setPersonName':
  //     const person = {...state.person}
  //     person.name = action.payload
  //     return {...state, person}
  // }
  if (action.type === 'setRootTitle') {
    return {...state, rootTitle: action.payload}
  } else if (action.type === 'setPersonName') {
    const person = {...state.person}
    person.name = action.payload
    return {...state, person}
  }
  return state
}