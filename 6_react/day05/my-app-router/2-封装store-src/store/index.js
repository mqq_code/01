import { createContext, useReducer, useContext } from 'react'
import { reducer, initState } from './reducer'

// 创建context对象
export const storeCtx = createContext()


// 创建Provider组件，把数据传给所有的后代组件
export const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, initState)

  return <storeCtx.Provider value={{ state, dispatch }}>
    {props.children}
  </storeCtx.Provider>
}

// 封装自定义hook，获取context对象的value
export const useStore = () => useContext(storeCtx)

