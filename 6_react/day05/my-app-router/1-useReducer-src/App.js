import { useRoutes, Navigate, NavLink } from 'react-router-dom'
import Child1 from './pages/Child1'
import Child2 from './pages/Child2'
import Child3 from './pages/Child3'

const routesConfig = [
  { path: '/child1', element: <Child1/> },
  { path: '/child2', element: <Child2/> },
  { path: '/child3', element: <Child3/> },
  { path: '/', element: <Navigate to="/child1" /> },
]

const App = () => {
  const routes = useRoutes(routesConfig)
  return <div>
    <nav>
      <NavLink to="/child1">导航1</NavLink>
      <NavLink to="/child2">导航2</NavLink>
      <NavLink to="/child3">导航3</NavLink>
    </nav>
    {routes}
  </div>
}

export default App