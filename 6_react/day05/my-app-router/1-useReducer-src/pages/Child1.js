import React, { useEffect, useState, useReducer, useRef } from 'react'

// useState: 定义组件状态，数据改变组件更新
// useRef: 存和渲染无关的数据，数据改变组件不更新
// useReducer: 存操作复杂的数据，数据改变组件更新
// useEffect: 处理组件副作用
// useMemo: 缓存数据
// useCallback: 缓存函数,配合memo优化子组件性能
// useLayoutEffect: 功能类似useEffect，在页面渲染前执行

const initState = {
  username: '小明',
  age: 20,
  sex: '男',
  hobby: [],
  address: '北京'
}
// 组件调用 dispatch 函数时执行 reducer 函数
// 根据 action.type 判断本次要修改的内容，返回新的 state 替换组件内的 state
const reducer = (state, action) => {
  // state: 上一次的state
  // action: dispatch 传入的数据
  if (action.type === 'set_username') {
    return {...state, username: action.payload}
  } else if (action.type === 'set_age_sex') {
    return {
      ...state,
      age: action.payload.age,
      sex: action.payload.sex
    }
  } else if (action.type === 'set_age') {
    return {
      ...state,
      age: state.age + action.payload
    }
  }
  return state
}

const Child1 = () => {
  const [state, dispatch] = useReducer(reducer, initState)
  const inp1 = useRef(null)
  const inp2 = useRef(null)

  return (
    <div>
      <div>
        修改name: <input type="text" value={state.username} onChange={e => {
          dispatch({
            type: 'set_username',
            payload: e.target.value
          })
        }} />
      </div>
      <div>
        <input type="text" placeholder='年龄' ref={inp1} />
        <input type="text" placeholder='性别' ref={inp2} />
        <button onClick={() => {
          dispatch({
            type: 'set_age_sex',
            payload: {
              age: inp1.current.value,
              sex: inp2.current.value,
            }
          })
        }}>修改年龄和性别</button>
      </div>
      <button onClick={() => {
        // 通过 dispatch 发送 action，执行reducer函数
        // action是一个有type属性的对象： { type: '描述本次修改内容' }
        dispatch({
          type: 'set_username',
          payload: Math.random()
        })
      }}>修改姓名</button>
      <button onClick={() => {
        dispatch({
          type: 'set_age',
          payload: -1
        })
      }}>age--</button>
      <button onClick={() => {
        dispatch({
          type: 'set_age',
          payload: 1
        })
      }}>age++</button>
      <hr />
      {JSON.stringify(state)}
    </div>
  )
}

export default Child1