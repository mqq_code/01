
export default {

  // 返回值可以是数组形式
  'GET /api/users': { id: 1, name: 'foo' },

  // 返回值也可以是对象形式
  'POST /api/list': [
    { id: 1, name: 'foo' },
    { id: 2, name: 'bar' }
  ],
}