// umi的配置文件
export default {
  // 配置路由模式
  history: {
    type: 'browser'
  },
  // 配置路由
  routes: [
    { path: '/login', component: 'login/login', layout: false },
    // 重定向
    { path: '/', redirect: '/home' },
    { path: '/home', component: 'index', wrappers: ['@/wrappers/Auth'] },
    { path: '/docs', component: 'docs' },
    { path: '/detail/:id', component: '@/pages/detail/detail' },
    { path: '/city', component: 'city/city', wrappers: ['@/wrappers/Auth'] },
  ],
  // 配置前端代理
  proxy: {
    
  },
  // 配置安装的插件
  plugins: ['@umijs/plugins/dist/antd', '@umijs/plugins/dist/dva'],
  // 开启要使用的插件
  antd: {},
  dva: {}
}