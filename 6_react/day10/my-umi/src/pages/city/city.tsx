import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'umi'


const city = () => {
  const username = useSelector(state => state.user.username)
  const age = useSelector(state => state.user.age)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({
      type: 'user/getList'
    })
  }, [])

  return (
    <div>
      <p>姓名： {username}</p>
      <p>年龄： {age}</p>
      <button onClick={() => {
        dispatch({
          type: 'user/setAge',
          payload: 2
        })
      }}>+</button>
    </div>
  )
}

export default city