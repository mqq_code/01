import { Link, useNavigate, history } from 'umi'
import { Button } from 'antd'
import axios from 'axios'
import { useEffect } from 'react'

export default function HomePage() {
  const navigate = useNavigate()

  useEffect(() => {
    axios.post('/api/list').then(res => {
      console.log(res.data)
    })
  }, [])

  return (
    <div>
      <h2>我是home页面</h2>
      <button onClick={() => {
        // navigate('/login')
        history.push('/login')
      }}>跳转登录</button>
      <Link to="/city">跳转city</Link>
      <Button type="primary">按钮</Button>
    </div>
  );
}
