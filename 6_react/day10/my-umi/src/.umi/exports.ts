// @ts-nocheck
// This file is generated by Umi automatically
// DO NOT CHANGE IT MANUALLY!
// defineApp
export { defineApp } from './core/defineApp'
export type { RuntimeConfig } from './core/defineApp'
// plugins
export { connect, useDispatch, useStore, useSelector, getDvaApp } from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/src/.umi/plugin-dva';
// plugins types.d.ts
export * from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/src/.umi/plugin-antd/types.d';
export * from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/src/.umi/plugin-dva/types.d';
// @umijs/renderer-*
export { createBrowserHistory, createHashHistory, createMemoryHistory, Helmet, HelmetProvider, createSearchParams, generatePath, matchPath, matchRoutes, Navigate, NavLink, Outlet, resolvePath, useLocation, useMatch, useNavigate, useOutlet, useOutletContext, useParams, useResolvedPath, useRoutes, useSearchParams, useAppData, useClientLoaderData, useRouteProps, useSelectedRoutes, useServerLoaderData, renderClient, __getRoot, Link, useRouteData, __useFetcher, withRouter } from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/node_modules/.pnpm/@umijs+renderer-react@4.1.9_react-dom@18.1.0_react@18.1.0/node_modules/@umijs/renderer-react';
export type { History } from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/node_modules/.pnpm/@umijs+renderer-react@4.1.9_react-dom@18.1.0_react@18.1.0/node_modules/@umijs/renderer-react'
// umi/client/client/plugin
export { ApplyPluginsType, PluginManager } from '/Users/zhaoyaxiang/Desktop/01/6_react/day10/my-umi/node_modules/.pnpm/umi@4.1.9_@babel+core@7.24.4_@types+react@18.2.78_eslint@8.57.0_prettier@3.2.5_react-dom@18.2_btcf7jrdh66i75krcemtee3w2y/node_modules/umi/client/client/plugin.js';
export { history, createHistory } from './core/history';
export { terminal } from './core/terminal';
// react ssr
export const useServerInsertedHTML: Function = () => {};
// test
export { TestBrowser } from './testBrowser';
