import { Link, Outlet } from 'umi';
import styles from './index.less';

export default function Layout() {
  return (
    <div className={styles.navs}>
      <h1>我是最外层的页面</h1>
      <Link to="/">home</Link>
      <Link to="/docs">docs</Link>
      <Link to="/city">city</Link>
      <Link to="/detail/123">detail</Link>
      <main>
        <Outlet />
      </main>
    </div>
  );
}
