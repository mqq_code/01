import axios from 'axios'

export default {
  // 存数据
  state: {
    username: '小明',
    age: 20
  },
  reducers: {
    setAge(state, action) {
      return {...state, age: state.age + action.payload}
    }
  },

  effects: {
    // Generator 函数
    *getList(action, { call, put }) {
      // 通过 call 调用接口
      const res = yield call(axios.get, 'https://zyxcl.xyz/music_api/banner');
      console.log(res.data.banners.length)

      // 通过put调用reducer函数
      yield put({
        type: 'setAge',
        payload: res.data.banners.length
      });
    },
  },

};


// Generator 函数, 可以中断函数执行
// function* fn() {
//   console.log('111')
//   yield console.log('2222')
//   yield console.log('333')
//   console.log(444)
// }
// window.$gg = fn() // 调用函数不会立即执行，返回一个执行器
// // window.$gg .next() // 调用next方法继续往下执行




// function() {}
// () => {}
// async function name(params ) {
// }