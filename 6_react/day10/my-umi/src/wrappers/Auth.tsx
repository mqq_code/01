import { Navigate, Outlet } from 'umi'

const Auth =  (props: any) => {
  const token = localStorage.getItem('token')
  if (!token) {
    return <Navigate to="/login" />
  }
  return (
    <div style={{ border: '3px solid green' }}>
      <Outlet />
    </div>
  )
}

export default Auth