import React from 'react'
import style from './home.module.scss'
import Footer from './components/footer/Footer'
import Tab from './components/tab/Tab'

const Home = () => {
  return (
    <div className={style.home}>
      <header>header</header>
      <Tab />
      <Footer />
    </div>
  )
}

export default Home