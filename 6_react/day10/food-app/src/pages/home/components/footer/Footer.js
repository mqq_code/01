import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import style from './footer.module.scss'
import { Link } from 'react-router-dom'
import { changeSelected } from '../../../../store/features/food'

const Footer = () => {
  const selected = useSelector(state => state.food.selected)
  const dispatch = useDispatch()

  console.log('选中的数据', selected)
  // 没有选中数据就不渲染内容
  if (selected.length === 0) return null
  return (
    <div className={style.footer}>
      <h3>
        <b>已选食物({selected.length})</b>
        <Link to="/compare">去测评</Link>
      </h3>
      <ul>
        {selected.map(item =>
          <li key={item.name}>
            {item.name}
            <span onClick={() => dispatch(changeSelected(item))}>x</span>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Footer