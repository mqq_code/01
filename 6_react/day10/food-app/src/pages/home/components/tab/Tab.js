import React, { useEffect, useState } from 'react'
import style from './tab.module.scss'
import { useSelector, useDispatch } from 'react-redux'
import { getAllFood, changeSelected } from '../../../../store/features/food'
import classNames from 'classnames'

const Tab = () => {
  const food = useSelector(state => state.food.allFood)
  const dispatch = useDispatch()
  const [curIndex, setCurIndex] = useState(0)

  useEffect(() => {
    if (food.length === 0) {
      // 进入页面立即调用store中的方法
      dispatch(getAllFood())
    }
  }, [])

  return (
    <main className={style.tab}>
      <div className={style.menu}>
        {food.map((item, index) =>
          <p
            key={item.name}
            className={classNames({
              [style.active]: curIndex === index
            })}
            onClick={() => setCurIndex(index)}
          >{item.name}</p>
        )}
      </div>
      <div className={style.list}>
        {food[curIndex]?.list.map(item =>
          <div
            key={item.name}
            className={classNames([style.food, { [style.active]: item.checked }])}
            onClick={() => {
              // 把当前点击的食物添加到选中列表
              dispatch(changeSelected(item))
            }}
          >
            <img src={item.imgUrl} alt="" />
            <p>{item.name}</p>
          </div>
        )}
      </div>
    </main>
  )
}

export default Tab