import React from 'react'
import { useLocation, useSearchParams } from 'react-router-dom'

const Detail = () => {
  const location = useLocation()
  const [searchParams] = useSearchParams()
  const info = {
    name: searchParams.get('name'),
    img: searchParams.get('img')
  }

  return (
    <div>
      <h2>{info.name}</h2>
      <img src={info.img} alt="" />
    </div>
  )
}

export default Detail