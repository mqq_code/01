import React, { useState, useMemo } from 'react'
import style from './compare.module.scss'
import { useSelector } from 'react-redux'
import classNames from 'classnames'
import { useNavigate } from 'react-router-dom'

const Compare = () => {
  // 选中的数据
  const selected = useSelector(state => state.food.selected)
  const [curIndex, setCurIndex] = useState(0)
  const navigate = useNavigate()

  // 根据选中的数据生成导航列表数据
  const navlistMemo = useMemo(() => {
    const navlist = [
      {
        title: '建议少吃',
        tip: '以下食物升糖指数较高，建议少吃',
        color: 'tomato',
        list: [],
        min: 70,
        max: 100
      },
      {
        title: '适量食用',
        tip: '以下食物升糖指数适中，建议适量食用',
        color: '#ffa001',
        list: [],
        min: 40,
        max: 70
      },
      {
        title: '放心食用',
        tip: '以下食物升糖指数偏低，可以放心食用',
        color: '#7bb872',
        list: [],
        min: 0,
        max: 40
      }
    ]
    // 遍历选中的数据
    selected.forEach(item => {
      const glycemicIndex = Number(item.glycemicIndex)
      // 根据 glycemicIndex 在navlist中查找所在的区间
      const index = navlist.findIndex(val => {
        return glycemicIndex >= val.min && glycemicIndex < val.max
      })
      navlist[index].list.push(item)
    })
    return navlist
  }, [selected])

  const curItem = useMemo(() => navlistMemo[curIndex], [navlistMemo, curIndex])


  return (
    <div className={style.compare}>
      <nav>
        {navlistMemo.map((item, index) =>
          <div
            className={classNames(style.navItem, { [style.active]: curIndex === index })}
            onClick={() => setCurIndex(index)}
            key={item.title}
          >
            <p>{item.list.length}</p>
            <p>{item.title}</p>
          </div>
        )}
      </nav>
      <div className={style.content}>
        <div className={style.tip} style={{ color: curItem.color }}>{curItem.tip}</div>
        <ul>
          {curItem.list.map(item =>
            <li key={item.name} onClick={() => {
              navigate(`/detail?name=${item.name}&img=${encodeURIComponent(item.imgUrl)}`)
            }}>
              <img src={item.imgUrl} alt="" />
              <h3>{item.name}</h3>
              <p>{item.content}</p>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Compare