import React from 'react'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Compare from './pages/compare/Compare'
import { useRoutes } from 'react-router-dom'


const App = () => {
  const routes = useRoutes([
    { path: '/', element: <Home /> },
    { path: '/detail', element: <Detail /> },
    { path: '/compare', element: <Compare /> }
  ])
  return routes
}

export default App