import { configureStore } from '@reduxjs/toolkit'
import foodReducer from './features/food'


const store = configureStore({
  reducer: {
    food: foodReducer
  }
})

export default store