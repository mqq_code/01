import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

export const getAllFood = createAsyncThunk('food/getAllFood', async () => {
  const res = await axios.get('https://zyxcl.xyz/exam_api/food')
  return res.data.value
})

const initialState = {
  allFood: [], // 所有数据
  selected: [] // 选中的食物
}

export const foodSlice = createSlice({
  name: 'food',
  initialState: JSON.parse(localStorage.getItem('reduxState')) || initialState,
  reducers: {
    // 修改 state 数据
    changeSelected: (state, action) => {
      // action.payload: 组件内传入的参数
      // 根据传入的参数在allFood中找到点击的对象
      state.allFood.forEach(item => {
        item.list.forEach(food => {
          // 根据传入的name查找数据中对应的食物，修改选中状态
          if (food.name === action.payload.name) {
            food.checked = !food.checked
            // 如果选中就添加到 selected
            if (food.checked) {
              state.selected.push(food)
            } else {
              // 取消选中就删除
              state.selected = state.selected.filter(v => v.name !== food.name)
            }
          }
        })
      })

      localStorage.setItem('reduxState', JSON.stringify(state))
    }
  },
  // 等待异步action执行
  extraReducers: builder => {
    builder
      .addCase(getAllFood.fulfilled, (state, action) => {
        // 等待 getAllFood 异步方法执行成功
        // action.payload => 接收 getAllFood 函数 return 的数据
        state.allFood = action.payload
        localStorage.setItem('reduxState', JSON.stringify(state))
      })
  }
})

export const { changeSelected } = foodSlice.actions

export default foodSlice.reducer