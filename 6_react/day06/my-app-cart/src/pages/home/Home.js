import React, { useEffect, useState, useLayoutEffect } from 'react'
import { Outlet, NavLink, useLocation } from 'react-router-dom'

const navList = [
  { path: '/goods', title: '商品' },
  { path: '/comments', title: '评论' },
  { path: '/shop', title: '商家' }
]
const Home = () => {
  const location = useLocation()
  const [curIndex, setCurIndex] = useState(0)

  useLayoutEffect(() => {
    // 根据当前地址查找下标
    const index = navList.findIndex(v => v.path === location.pathname)
    setCurIndex(index)
  }, [location.pathname])


  return (
    <div className="page home">
      <nav>
        {navList.map(item =>
          <NavLink key={item.path} to={item.path}>{item.title}</NavLink>
        )}
        <span style={{ transform: `translateX(${curIndex * 100}%)` }}></span>
      </nav>
      <main>
        <Outlet />
      </main>
    </div>
  )
}

export default Home