import React, { useEffect, useState, useMemo } from 'react'
import axios from 'axios'
import List from '../../../components/List'
import Footer from '../../../components/Footer'
import Cart from '../../../components/Cart'

const Goods = () => {
  const [list, setList] = useState([])
  const [cart, setCart] = useState([])
  const [showCart, setShowCart] = useState(false)

  const getList = async () => {
    const res = await axios.get('https://zyxcl.xyz/exam_api/goods')
    setList(res.data.data)
  }
  // 添加到购物车
  const addCart = (food) => {
    // 去购物车列表中查找是否存在此商品
    const index = cart.findIndex(v => v.name === food.name)
    if (index > -1) {
      const newCart = [...cart]
      if (food.count === 0) {
        newCart.splice(index, 1)
      } else {
        newCart[index].count = food.count
      }
      setCart(newCart)
    } else {
      setCart([...cart, {...food}])
    }
  }

  // 修改商品数量
  const changeCount = (name, num) => {
    const newList = [...list]
    // 遍历数据，根据name去所有数据中查找点击的食物
    newList.forEach(item => {
      item.foods.forEach(food => {
        if (name === food.name) {
          const count = food.count || 0
          food.count = count + num
          // 添加到购物车
          addCart(food)
        }
      })
    })
    // 更新数据
    setList(newList)
  }
  
  useEffect(() => {
    getList()
  }, [])

  // 监听购物车数据改变，计算总价
  const total = useMemo(() => {
    return cart.reduce((prev, val) => {
      prev.count += val.count
      prev.price += (val.count * val.price)
      return prev
    }, { count: 0, price: 0 })
  }, [cart])

  // 点击底部展示购物车列表
  const clickFooter = () => {
    if (total.count > 0) {
      setShowCart(!showCart)
    }
  }

  // 清空
  const clear = () => {
    const newList = [...list]
    newList.forEach(item => {
      item.foods.forEach(food => {
        food.count = 0
      })
    })
    setList(newList)
    setCart([])
  }

  useEffect(() => {
    if (cart.length === 0) {
      setShowCart(false)
    }
  }, [cart])

  return (
    <div className="page goods">
      <List list={list} onChange={changeCount} />
      <Footer total={total} onClick={clickFooter} />
      <Cart
        cartList={cart}
        onChange={changeCount}
        onClear={clear}
        onClose={() => setShowCart(false)}
        visible={showCart}
      />
    </div>
  )
}

export default Goods