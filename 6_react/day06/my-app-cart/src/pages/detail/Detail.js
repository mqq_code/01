import React from 'react'
import { useSearchParams, useNavigate } from 'react-router-dom'

const Detail = () => {
  const navigate = useNavigate()
  const [searchParams] = useSearchParams()
  const img = searchParams.get('img')
  const name = searchParams.get('name')

  return (
    <div>
      <button onClick={() => navigate(-1)}>返回</button>
      <h2>{name}</h2>
      <img src={img} style={{ width: '100%' }} alt="" />
    </div>
  )
}

export default Detail