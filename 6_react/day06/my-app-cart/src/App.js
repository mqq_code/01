import { useRoutes, Navigate } from 'react-router-dom'
import Home from './pages/home/Home'
import Goods from './pages/home/goods/Goods'
import Comments from './pages/home/comments/Comments'
import Shop from './pages/home/shop/Shop'
import Detail from './pages/detail/Detail'

const App = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Home />,
      children: [
        { path: '/', element: <Navigate to='/goods' /> },
        { path: '/goods', element: <Goods /> },
        { path: '/comments', element: <Comments /> },
        { path: '/shop', element: <Shop /> }
      ]
    },
    { path: '/detail', element: <Detail /> }
  ])
  return routes
}

export default App