import React, { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const List = (props) => {
  const [curIndex, setCurIndex] = useState(0)
  const rightRef = useRef(null)
  const childrenOffsetTop = useRef([])
  const navigate = useNavigate()

  useEffect(() => {
    // 等待页面渲染完成, 获取右侧每一项元素到父元素顶部的距离
    childrenOffsetTop.current = [...rightRef.current.children].map(item => item.offsetTop)
    // console.log(childrenOffsetTop.current)
  }, [props.list])

  const changeTab = index => {
    // setCurIndex(index)
    rightRef.current.scrollTop = rightRef.current.children[index].offsetTop
  }
  const onScroll = (e) => {
    const scrollTop = e.target.scrollTop
    childrenOffsetTop.current.forEach((t, i) => {
      // 根据当前滚动位置去所有元素到顶部的距离中查找下标
      const next = childrenOffsetTop.current[i + 1]
      if (scrollTop >= t && (scrollTop < next || !next)) {
        setCurIndex(i)
      }
    })
  }
  return (
    <div className="list">
      <div className="tab-nav">
        {props.list.map((item, index) =>
          <p
            key={item.name}
            className={curIndex === index ? 'active' : ''}
            onClick={() => changeTab(index)}
          >{item.name}</p>
        )}
      </div>
      <div className="tab-content" ref={rightRef} onScroll={onScroll}>
        {props.list.map(item =>
          <div className="goods-item" key={item.name}>
            <h3>{item.name}</h3>
            <ul>
              {item.foods.map(food =>
                <li key={food.name}>
                  <img src={food.image} width={80} alt="" onClick={() => {
                    navigate(`/detail?name=${food.name}&img=${encodeURIComponent(food.image)}`)
                  }} />
                  <p>{food.name}</p>
                  <p>¥{food.price}</p>
                  {food.count > 0 &&
                    <>
                      <button onClick={() => props.onChange(food.name, -1)}>-</button>
                      {food.count}
                    </>
                  }
                  <button onClick={() => props.onChange(food.name, 1)}>+</button>
                </li>
              )}
            </ul>
          </div>
        )}
      </div>
    </div>
  )
}

export default List