import React from 'react'

const Footer = (props) => {
  return (
    <footer className={props.total.count > 0 ? 'highlight' : ''} onClick={props.onClick}>
      <span>总数: {props.total.count}</span>
      <span>总价: ¥{props.total.price}</span>
    </footer>
  )
}

export default Footer