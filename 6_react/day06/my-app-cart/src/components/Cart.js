import React, { useEffect, useState, useLayoutEffect } from 'react'
import classNames from 'classnames'

const Cart = (props) => {
  // 控制进入还是离开动画
  const [aniClass, setAniClass] = useState(false)
  // 控制 cart 显示隐藏
  const [showDom, setShowDom] = useState(props.visible)

  useLayoutEffect(() => {
    if (props.visible) {
      setAniClass(true)
      setShowDom(true)
    } else {
      setAniClass(false)
    }
  }, [props.visible])
  

  if (!showDom) return null
  return (
    <div
      className={classNames('cart animate__animated', {
        animate__fadeIn: aniClass, // 添加进入动画
        animate__fadeOut: !aniClass // 离开动画
      })}
      onClick={() => {
        // 离开动画
        setAniClass(false)
      }}
      onAnimationEnd={() => {
        if (!aniClass) {
          // 等待离开动画结束
          // 让cart隐藏元素
          setShowDom(false)
          // 通知父组件蒙层关闭
          props.onClose()
        }
      }}
    >
      <div
        className={classNames('cart-content animate__animated', {
          animate__slideInUp: aniClass, // 添加进入动画
          animate__slideOutDown: !aniClass // 离开动画
        })}
        onClick={e => e.stopPropagation()}
      >
        <h3>
          <b>购物车</b>
          <span onClick={props.onClear}>清空</span>
        </h3>
        <ul>
          {props.cartList.map(item =>
            <li key={item.name}>
              <b>{item.name}</b>
              <span>¥{item.price}</span>
              <div className="btns">
                <button onClick={() => props.onChange(item.name, -1)}>-</button>
                <i>{item.count}</i>
                <button onClick={() => props.onChange(item.name, 1)}>+</button>
              </div>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Cart