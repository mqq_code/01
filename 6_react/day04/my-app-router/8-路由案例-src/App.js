import { useRoutes } from 'react-router-dom'
import routesConfig from './router'

const App = () => {
  const routes = useRoutes(routesConfig)
  return routes
}

export default App