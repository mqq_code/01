import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Checkbox, Form, Input } from 'antd'

const Login = () => {
  const navigate = useNavigate()
  // 获取form实例
  const [form] = Form.useForm()

  const submit = () => {
    // console.log(form)
    // 通过form组件实例触发表单校验
    form.validateFields()
    .then((values) => {
      // 校验成功
      console.log('校验成功', values)
      const { user, pwd } = values
      if (user === 'admin' && pwd === '123') {
        alert('登录成功')
        localStorage.setItem('token', user + pwd)
        localStorage.setItem('username', user)
        navigate('/')
      } else {
        alert('用户名或者密码错误')
      }
    })
    .catch(e => {
      // 校验没通过
      console.log(e)
    })
  }
  return (
    <div>
      <Form
        form={form}
        initialValues={{ user: '小明' }}
        className="login-form"
      >
        <Form.Item
          name="user"
          rules={[
            { required: true, message: '请输入用户名!' }
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="用户名" />
        </Form.Item>

        <Form.Item
          name="pwd"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" block onClick={submit}>登录</Button>
        </Form.Item>
      </Form>
    </div>
  )
}



// const Login = () => {
//   const navigate = useNavigate()

//   const [form, setForm] = useState({ username: '', password: '' })
//   const login = () => {
//     const { username, password } = form
//     if (username === 'admin' && password === '123') {
//       alert('登录成功')
//       localStorage.setItem('token', username + password)
//       localStorage.setItem('username', username)
//       navigate('/')
//     } else {
//       alert('用户名或者密码错误')
//     }
//   }
//   return (
//     <div>
//       <h2>Login</h2>
//       <p><input type="text" placeholder='请输入用户名' value={form.username} onChange={e => setForm({...form, username: e.target.value})} /></p>
//       <p><input type="password" placeholder='请输入密码' value={form.password} onChange={e => setForm({...form, password: e.target.value})}  /></p>
//       <button onClick={login}>登录</button>
//     </div>
//   )
// }

export default Login