import React from 'react'
import Menu from '../../components/Menu'
import Header from '../../components/Header'
import { Outlet } from 'react-router-dom'

const Home = () => {
  return (
    <div className="home">
      <Menu />
      <main>
        <Header></Header>
        <div className="content">
          <Outlet />
        </div>
      </main>
    </div>
  )
}

export default Home