import Home from '../pages/home/Home'
import Login from '../pages/login/Login'
import { Navigate } from 'react-router-dom'

const Auth = props => {
  const token = localStorage.getItem('token')
  if (!token) {
    return <Navigate to="/login" />
  }
  return props.children
}


const routes = [
  {
    path: '*',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Auth><Home /></Auth>,
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/child1" />
      },
      {
        path: '/home/child1',
        element: <div>资金组成</div>,
      },
      {
        path: '/home/child2',
        element: <div>资金流向</div>,
      },
      {
        path: '/home/child3',
        element: <div>资金来源</div>,
      },
      {
        path: '/home/child4',
        element: <div>用户设置</div>,
      }
    ]
  },
  {
    path: '/login',
    element: <Login />
  }
]

export default routes