
export const tablist = [
  {
    title: '资金管理',
    list: [
      { path: '/home/child1', text: '资金组成' },
      { path: '/home/child2', text: '资金流向' },
      { path: '/home/child3', text: '资金来源' }
    ]
  },
  {
    title: '系统管理',
    list: [
      { path: '/home/child4', text: '用户设置' }
    ]
  }
]

export const menuList = tablist.reduce((prev, val) => {
  return prev.concat(val.list)
}, [])