import React, { useEffect, useState } from 'react'
import { useNavigate, useLocation, NavLink } from 'react-router-dom'
import { menuList } from '../data'

const Header = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const username = localStorage.getItem('username')
  const [navList, setNavList] = useState([])

  useEffect(() => {
    // 根据当前路由地址去所有菜单中查找下标
    const index = menuList.findIndex(v => v.path === location.pathname)
    // 在当前导航中查找是否存在此路由
    if (!navList.find(v => v.path === location.pathname) && index > -1) {
      // 把当前路由添加到导航
      setNavList([...navList, menuList[index]])
    }
  }, [location.pathname])

  const logout = () => {
    localStorage.removeItem('token')
    navigate('/login')
  }

  const remove = (path, index) => {
    // 拷贝数据
    const newList = [...navList]
    // 要删除的是当前路由
    if (path === location.pathname) {
      if (index < navList.length - 1) {
        // 当前删除的不是最后一个
        navigate(newList[index + 1].path)
        newList.splice(index, 1)
        setNavList(newList)
      } else {
        // 删除的是最后一个
        if (navList.length === 1) {
          // 只剩下最后一个，直接清空，退出登录
          setNavList([])
          logout()
        } else {
          // length > 0，往前一个路由跳转
          navigate(newList[index - 1].path)
          newList.splice(index, 1)
          setNavList(newList)
        }
      }
    } else {
      // 不是当前路由直接删除
      newList.splice(index, 1)
      setNavList(newList)
    }
  }
  return (
    <header>
      <div className="top">
        <p>用户名：{username}</p>
        <button onClick={logout}>退出</button>
      </div>
      <nav>
        {navList.map((item, index) =>
          <NavLink key={item.path} to={item.path}>
            {item.text}
            <span onClick={e => {
              e.preventDefault()
              remove(item.path, index)
            }}>x</span>
          </NavLink>
        )}
      </nav>
    </header>
  )
}

export default Header