import React, { useState, useEffect } from 'react'
import { NavLink, useNavigate, useLocation } from 'react-router-dom'
import { tablist } from '../data'


const Menu = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const [curIndex, setCurIndex] = useState(0)
  const [title, setTitle] = useState('标题')

  useEffect(() => {
    // 遍历tab数据，根据当前地址查找标题和父级的下标
    tablist.forEach((item, index) => {
      item.list.forEach(val => {
        if (val.path === location.pathname) {
          setCurIndex(index)
          setTitle(val.text)
        }
      })
    })
  }, [location.pathname])

  return (
    <div className="menu">
      <div className="tab-nav">
        {tablist.map((item, index) =>
          <div
            className={`tab-nav-item ${curIndex === index ? 'active' : ''}`}
            key={item.title}
            onClick={() => {
              // tab切换，跳转到第一个子路由
              setCurIndex(index)
              navigate(tablist[index].list[0].path)
            }}
          >{item.title}</div>
        )}
      </div>
      <div className="tab-content">
        <h2>{title}</h2>
        <ul>
          {tablist[curIndex].list.map(item =>
            <li key={item.path}>
              <NavLink to={item.path}>{item.text}</NavLink>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Menu