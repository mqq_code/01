import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate, Navigate } from 'react-router-dom'

const Movie = () => {

  // 获取跳转路由的方法
  const navigate = useNavigate()

  const [list, setList] = useState([])
  useEffect(() => {
    axios.get('https://zyxcl.xyz/music_api/toplist').then(res => {
      console.log(res.data.list)
      setList(res.data.list)
    })
  }, [])

  const goDetail = (id) => {
    // 跳详情
    // navigate(`/detail?id=${id}`)
    // 动态路由
    navigate(`/detail/${id}`)
  }


  return (
    <div style={{ height: '100%', overflow: 'auto' }}>
      {list.map(item =>
        <div key={item.id} className="item" onClick={() => goDetail(item.id)}>
          <h3>{item.name}</h3>
          <img src={item.coverImgUrl} width={100} height={100} alt="" />
        </div>
      )}
    </div>
  )
}

export default Movie