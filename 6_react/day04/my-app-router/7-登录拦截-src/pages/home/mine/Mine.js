import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import withRouter from '../../../hoc/withRouter'


class Mine extends Component {

  goDetial = () => {
    console.log('跳转', this.props)
    this.props.navigate('/detail/123?id=aaaaaa')
  }

  render() {
    return (
      <div>
        <h2>Mine</h2>
        <Link to="/login">跳转登录</Link>
        <button onClick={this.goDetial}>跳转详情</button>
      </div>
    )
  }
}

// withRouter: 高阶组件，把路由信息添加到组件的props中
export default withRouter(Mine)
