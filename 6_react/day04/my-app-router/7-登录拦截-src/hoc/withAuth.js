import { Navigate } from 'react-router-dom'

const Auth = (props) => {
   // 判断是否有token
   const token = localStorage.getItem('token')
   if (!token) {
     return <Navigate to="/login" />
   }

   return props.children
}

export default Auth