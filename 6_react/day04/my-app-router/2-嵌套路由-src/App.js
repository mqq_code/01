import React from 'react'
import Home from './pages/home/Home'
import Movie from './pages/home/movie/Movie'
import Cinema from './pages/home/cinema/Cinema'
import Mine from './pages/home/mine/Mine'
import Login from './pages/login/Login'
import Detail from './pages/detail/Detail'

import {
  Routes,
  Route, // 渲染路由组件
  Navigate, // 重定向
  Link, // 跳转路由
  NavLink // 导航路由，会自动添加高亮类名
} from 'react-router-dom'

const App = () => {
  return (
    <Routes>
      <Route path="*" element={<Navigate to="/home" />} />
      <Route path="/home" element={<Home />}>
        <Route path="/home" element={<Navigate to="/home/movie" />} />
        <Route path="/home/movie" element={<Movie />}>
          <Route path="/home/movie/hot" element={<div>我是hot页面</div>} />
          <Route path="/home/movie/coming" element={<div>我是coming页面</div>} />
        </Route>
        <Route path="/home/cinema" element={<Cinema />} />
        <Route path="/home/mine" element={<Mine />} />
      </Route>
      <Route path="/login" element={<Login />} />
      <Route path="/detail" element={<Detail />} />
    </Routes>
  )
}

export default App