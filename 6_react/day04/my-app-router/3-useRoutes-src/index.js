import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import {
  // 路由根组件，路由相关的所有功能必须在根组件内才能使用，一个项目只能用一次根组件
  BrowserRouter, // history 模式，url没有 #
  HashRouter // hash 模式，url有 #
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <App />
  </HashRouter>
);
