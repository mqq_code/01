import React from 'react'
import {
  Routes,
  Route, // 渲染路由组件
  Navigate, // 重定向
  Link, // 跳转路由
  NavLink, // 导航路由，会自动添加高亮类名
  useRoutes // 使用hook配置路由
} from 'react-router-dom'
// 引入路由配置数组
import routesConfig from './router'

const App = () => {
  // 配置路由
  const routes = useRoutes(routesConfig)
  return routes
}

export default App