import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import {
  // 路由根组件，路由相关的所有功能必须在根组件内才能使用，一个项目只能用一次根组件
  BrowserRouter, // history 模式，url没有 #
  HashRouter // hash 模式，url有 #
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // Suspense: 配合lazy异步加载组件，需要提供 fallback 参数
  // fallback: 异步加载组件时展示的默认内容
  <Suspense fallback={<h1 style={{ color: 'red' }}>加载中....</h1>}>
    <HashRouter>
      <App />
    </HashRouter>
  </Suspense>
);
