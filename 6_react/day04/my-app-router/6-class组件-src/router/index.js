import { lazy } from 'react'
import Home from '../pages/home/Home'
import Movie from '../pages/home/movie/Movie'
import Cinema from '../pages/home/cinema/Cinema'
import Mine from '../pages/home/mine/Mine'
import Login from '../pages/login/Login'
// import Detail from '../pages/detail/Detail'
import {
  Navigate, // 重定向
} from 'react-router-dom'

// 组件懒加载, 需要配合 Suspense 组件使用
// 懒加载的组件必须在 Suspense 组件内使用
const Detail = lazy(() => import(/* webpackChunkName: "detail" */'../pages/detail/Detail'))

const routes = [
  {
    path: '*',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/movie" />
      },
      {
        path: '/home/movie',
        element: <Movie />
      },
      {
        path: '/home/cinema',
        element: <Cinema />
      },
      {
        path: '/home/mine',
        element: <Mine />
      }
    ]
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/detail/:id',
    element: <Detail />
  }
]

export default routes