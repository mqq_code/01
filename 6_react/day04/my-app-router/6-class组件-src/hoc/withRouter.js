import {
  useParams,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";

function withRouter(Component) {

  return (props) => (
    <Component
        {...props}
        searchParams={useSearchParams()}
        params={useParams()}
        location={useLocation()}
        navigate={useNavigate()}
    />
  )
}

export default withRouter