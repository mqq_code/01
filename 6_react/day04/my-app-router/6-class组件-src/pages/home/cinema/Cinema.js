import React from 'react'
// import Child from './components/Child'

// 组件懒加载, 需要配合 Suspense 组件使用
// 懒加载的组件必须在 Suspense 组件内使用
const Child = React.lazy(() => import('./components/Child'))


const Cinema = () => {
  return (
    <div>
      <h2>影院</h2>
      <div className="box">
        {/* Suspense: 配合lazy异步加载组件，需要提供 fallback 参数 */}
        {/* fallback: 异步加载组件时展示的默认内容，兜底样式 */}
        <React.Suspense fallback={<h1 style={{ color: 'red' }}>加载中....</h1>}>
          <Child />
        </React.Suspense>
      </div>
    </div>
  )
}

export default Cinema