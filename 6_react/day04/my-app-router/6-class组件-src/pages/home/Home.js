import React from 'react'
import {
  Outlet, // 路由视图的渲染位置
  NavLink
} from 'react-router-dom'

const Home = () => {
  return (
    <div className="home">
      <main>
        <Outlet />
      </main>
      <footer>
        <NavLink replace to="/home/movie">电影</NavLink>
        <NavLink replace to="/home/cinema">影院</NavLink>
        <NavLink replace to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home