import React from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'

const Detail = () => {
  // 获取当前路由信息
  const location = useLocation()
  console.log(location)

  // 获取url的search参数
  const [searchParams, setSearchParams] = useSearchParams()
  console.log(searchParams)
  console.log(searchParams.get('id'))

  // 获取动态路由参数
  const params = useParams()
  console.log(params)
  
  return (
    <div>
      <h3>Detail</h3>
      <button onClick={() => {
        setSearchParams('id=111111111')
      }}>setid</button>
    </div>
  )
}

export default Detail