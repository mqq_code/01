import React from 'react'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import {
  Routes,
  Route, // 渲染路由组件
  Navigate, // 重定向
  Link, // 跳转路由
  NavLink // 导航路由，会自动添加高亮类名
} from 'react-router-dom'

const App = () => {
  return (
    <div className="app">
      <NavLink to="/home">首页</NavLink>
      <NavLink to="/login">登录</NavLink>
      <Routes>
        <Route path="*" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </div>
  )
}

export default App