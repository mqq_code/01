import { configureStore } from '@reduxjs/toolkit'
import countReducer from './features/countSlice'
import userReducer from './features/userSlice'

const store = configureStore({
  reducer: {
    countStore: countReducer,
    userStore: userReducer
  }
})

export default store