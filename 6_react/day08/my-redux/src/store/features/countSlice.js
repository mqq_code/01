import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const delay = () => new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(Math.floor(Math.random() * 10))
  }, 1000)
})


// 创建异步action
export const asyncChangeCount = createAsyncThunk('count/asyncChangeCount', async () => {
  // 调用接口获取数据
  const res = await delay()
  // 返回值会传给 extraReducers 的a ction.payload
  return res
})

export const asyncGetBanner = createAsyncThunk('count/asyncGetBanner', async (params) => {
  console.log('参数', params)
  const res = await axios.get('https://zyxcl.xyz/music_api/banner')
  return res.data.banners
})

export const countSlice = createSlice({
  // 仓库的名称
  name: 'count',
  // 初始值
  initialState: {
    count: 0,
    title: 'count 仓库',
    loading: false,
    banners: []
  },
  // 修改数据，可以直接修改state
  reducers: {
    add(state, action) {
      state.count++
    },
    sub(state, action) {
      state.count--
    },
    changeCount(state, { payload }) {
      console.log('changeCount action', payload)
      state.count += payload
    }
  },
  // 接收异步处理结果
  extraReducers: builder => {
    builder
    .addCase(asyncChangeCount.pending, state => {
      state.loading = true
    })
    .addCase(asyncChangeCount.fulfilled, (state, action) => {
      console.log('请求成功', action)
      // action.payload: asyncChangeCount 函数 return 的值
      state.count += action.payload
      state.loading = false
    })
    .addCase(asyncGetBanner.fulfilled, (state, action) => {
      console.log(action)
      state.banners = action.payload
    })
  }
})

// 抛出actions
export const { add, sub, changeCount } = countSlice.actions

// 抛出此仓库的所有 reducer
export default countSlice.reducer
