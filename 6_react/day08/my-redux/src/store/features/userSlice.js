import { createSlice } from '@reduxjs/toolkit'


export const userSlice = createSlice({
  name: 'user',
  initialState: {
    name: '小明',
    age: 22
  },
  reducers: {
    setName(state, action) {
      state.name = action.payload
    },
    setAge(state, action) {
      state.age = action.payload
    }
  }
})

export const { setName, setAge } = userSlice.actions

// 抛出此仓库的所有 reducer
export default userSlice.reducer
