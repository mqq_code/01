import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { add, sub, changeCount, asyncChangeCount, asyncGetBanner } from '../store/features/countSlice'
import { setName } from '../store/features/userSlice'

const Home = () => {
  const [num, setNum] = useState(0)
  const state = useSelector(s => s)
  const count = useSelector(s => s.countStore.count)
  const loading = useSelector(s => s.countStore.loading)
  const banners = useSelector(s => s.countStore.banners)
  const name = useSelector(s => s.userStore.name)

  const dispatch = useDispatch()

  // console.log(state)
  // console.log(loading)
  console.log(banners)

  useEffect(() => {
    dispatch(asyncGetBanner('abc'))
  }, [])

  return (
    <div>
      <h1>Home</h1>
      <p>姓名：{name}</p>
      <input type="text"  value={name} onChange={e => dispatch(setName(e.target.value))} />
      <hr />
      <p>count: {count}</p>
      <button onClick={() => dispatch(sub())}>-1</button>
      <button onClick={() => dispatch(add())}>+1</button>
      <hr />
      <input type="number" value={num} onChange={e => setNum(Number(e.target.value))} />
      <button onClick={() => dispatch(changeCount(num))}>修改count</button>
      <hr />
      <button disabled={loading} onClick={() => {
        // 发送异步action
        dispatch(asyncChangeCount())
      }}>{loading ? '正在请求' : '异步修改count'}</button>
    </div>
  )
}

export default Home