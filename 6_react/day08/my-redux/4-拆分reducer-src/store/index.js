import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import userReducer from './reducers/user'
import addressReducer from './reducers/address'


// combineReducers: 把多个 reducer 合并成一个
const store = createStore(combineReducers({
  user: userReducer,
  address: addressReducer
}), applyMiddleware(logger))

export default store
