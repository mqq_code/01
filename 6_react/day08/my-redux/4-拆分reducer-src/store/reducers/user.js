const initState = {
  name: '小明',
  age: 20
}


const reducer = (state = initState, action) => {
  console.log('user.js')
  if (action.type === 'user/set_age') {
    return {...state, age: state.age + action.payload}
  }
  return state
}


export default reducer