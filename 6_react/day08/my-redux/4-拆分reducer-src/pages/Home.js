import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Home = () => {
  const user = useSelector(s => s.user)

  // 获取 dispatch 函数
  const dispatch = useDispatch()

  console.log(user)

  return (
    <div>
      <h2>Home</h2>
      <p>姓名: {user.name}</p>
      <p>年龄: {user.age}</p>
      <button onClick={() => {
        // 调用 dispatch 会执行所有的 reducer 函数，可以通过 type 添加前缀区别要执行的 reducer
        dispatch({
          type: 'user/set_age',
          payload: 1
        })
      }}>age+</button>
    </div>
  )
}

export default Home