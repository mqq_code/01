const initState = {
  name: '小明',
  age: 20,
  banners: [],
  person: {
    name: '老王',
    age: 60,
    child: {
      name: '小王',
      age: 30,
      child: {
        nanme: '小小王',
        age: 5
      }
    }
  }
}

// 根据 action.type 返回最新的数据
const reducer = (state = initState, action) => {
  console.log('执行了reducer函数', state, action)

  if (action.type === 'age_add') {
    return {...state, age: state.age + 1}
  } else if (action.type === 'set_name') {
    return {...state, name: action.payload}
  } else if (action.type === 'set_banners') {
    return {...state, banners: action.payload}
  } else if (action.type === 'set_xxw') {
    // 深拷贝数据
    const newState = JSON.parse(JSON.stringify(state))
    newState.person.child.child.age++
    return newState
  }
  return state
}


export default reducer