import { createStore } from 'redux'
import reducer from './reducer'


// 创建store, 需要传入一个reducer函数作为参数
const store = createStore(reducer)

export default store
