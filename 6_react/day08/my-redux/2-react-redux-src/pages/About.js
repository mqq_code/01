import React, { Component } from 'react'
import { connect } from 'react-redux'

class About extends Component {

  render() {
    console.log(this.props)
    const { name, age } = this.props
    return (
      <div>
        <h1>About</h1>
        <p>年龄: {age}</p>
        <p>姓名: {name}</p>
        <input type="text" value={name} onChange={e => {
          // this.props.dispatch({
          //   type: 'set_name',
          //   payload: e.target.value
          // })
          this.props.setName(e.target.value)
        }} />
      </div>
    )
  }
}

// connect: 高阶组件，连接组件和redux数据
// 把 redux 数据传到组件的 props 中
const mapState = state => {
  // state: redux 中的所有数据
  // return 的对象会合并到组件的 props 中
  return {
    name: state.name,
    age: state.age,
    a: 'aaaaa'
  }
}
const mapDispatch = dispatch => {
  return {
    setName: payload => {
      dispatch({
        type: 'set_name',
        payload
      })
    },
    aa() {
    }
  }
}
export default connect(mapState, mapDispatch)(About)
