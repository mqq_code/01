import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'


const Home = () => {
  // 获取 redux 中的所有数据
  // const rootState = useSelector(state => {
  //   // state: redux中的数据
  //   return state
  // })
  // 获取指定数据
  const name = useSelector(s => s.name)
  const age = useSelector(s => s.age)
  const banners = useSelector(s => s.banners)
  const person = useSelector(s => s.person)

  // 获取 dispatch 函数
  const dispatch = useDispatch()

  useEffect(() => {
    axios.get('https://zyxcl.xyz/music_api/banner').then(res => {
      console.log(res.data.banners)
      dispatch({
        type: 'set_banners',
        payload: res.data.banners
      })
    })
  }, [])
  

  return (
    <div>
      <h2>Home</h2>
      <p>姓名: {name}</p>
      <p>年龄: {age}</p>
      <button onClick={() => {
        dispatch({
          type: 'age_add'
        })
      }}>age+</button>
      <hr />
      <div>
        person: {JSON.stringify(person)}
      </div>
      <button onClick={() => dispatch({ type: 'set_xxw' })}>修改小小王的age</button>

      <hr />
      <ul>
        {banners.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home