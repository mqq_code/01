import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import store from './store'
// 连接 react 组件和 redux
import { Provider } from 'react-redux'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // 把 store 传给所有后代组件
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)