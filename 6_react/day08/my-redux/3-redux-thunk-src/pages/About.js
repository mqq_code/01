import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { setNameAction, setBannerAction } from '../store/actions'

const About = () => {
  const name = useSelector(s => s.name)
  const banners = useSelector(s => s.banners)
  // 获取 dispatch 函数
  const dispatch = useDispatch()

  useEffect(() => {
    // 给dispatch传入函数，让dispatch自动执行次函数
    dispatch(setBannerAction)
  }, [])
  

  return (
    <div>
      <h2>about</h2>
      <p>姓名: {name}</p>
      <input type="text" value={name} onChange={e => {
        dispatch(setNameAction(e.target.value))
      }} />
      <ul>
        {banners.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default About