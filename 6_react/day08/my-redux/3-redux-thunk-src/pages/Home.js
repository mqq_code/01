import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { addAgeAction } from '../store/actions'

const Home = () => {
  const name = useSelector(s => s.name)
  const age = useSelector(s => s.age)
  const banners = useSelector(s => s.banners)

  // 获取 dispatch 函数
  const dispatch = useDispatch()

  useEffect(() => {
    axios.get('https://zyxcl.xyz/music_api/banner').then(res => {
      console.log(res.data.banners)
      dispatch({
        type: 'set_banners',
        payload: res.data.banners
      })
    })
  }, [])
  

  return (
    <div>
      <h2>Home</h2>
      <p>姓名: {name}</p>
      <p>年龄: {age}</p>
      <button onClick={() => dispatch(addAgeAction())}>age+</button>
      <hr />
      <ul>
        {banners.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home