import axios from 'axios'


export const addAgeAction = () => {
  return {
    type: 'age_add'
  }
}

export const setNameAction = (payload) => {
  return {
    type: 'set_name',
    payload
  }
}

export const setBannerAction = (dispatch) => {
  // 处理异步
  axios.get('https://zyxcl.xyz/music_api/banner').then(res => {
    // 发送action到reducer函数
    dispatch({
      type: 'set_banners',
      payload: res.data.banners
    })
  })
}
