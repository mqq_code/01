import React, { Component } from 'react'
import Child from './componets/Child'

class App extends Component {
  state = {
    title: 'class组件标题',
    num: 10
  }
  changeTitle = title => {
    console.log('接收子组件的数据', title)
    this.setState({ title })
  }
  render() {
    const { title, num } = this.state
    return (
      <div>
        <h1>{title}</h1>
        <div>
          {num > 0 &&
            <>
              <button onClick={() => this.setState({ num: num - 1 })}>-</button>
              {num}
            </>
          }
          <button onClick={() => this.setState({ num: num + 1 })}>+</button>
        </div>
        <hr />
        <Child
          rootTitle={title}
          rootNum={num}
          changeTitle={this.changeTitle}
        />
      </div>
    )
  }
}

export default App
