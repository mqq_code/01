import React, { Component, createRef } from 'react'

class App extends Component {
  state = {
    title: 'class组件标题',
    isChecked: true
  }
  change = e => {
    console.log(e.target.value)
    this.setState({
      title: e.target.value
    })
  }
  // 创建ref对象
  inpRef = createRef()

  render() {
    console.log(this.state)
    return (
      <div>
        <h1>{this.state.title}</h1>
        {/* 受控组件: 当value或者checked受state控制，必须添加一个onChange事件修改state，input内容会和state同步更新，这就是受控组件 */}
        <input type="text" value={this.state.title} onChange={this.change} />
        <button onClick={() => {
          this.setState({ title: '默认标题' })
        }}>重置</button>
        <input type="checkbox" checked={this.state.isChecked} onChange={e => {
          this.setState({ isChecked: e.target.checked })
        }} />
        {/* 非受控组件：表单元素不受state控制，获取值可以通过ref获取 */}
        <div>
          <h3>非受控组件</h3>
          <input type="text" defaultValue={124} ref={this.inpRef} />
          <input type="checkbox" defaultChecked={true} />
          <button onClick={() => {
            // 通过ref获取dom元素
            console.log(this.inpRef.current)
          }}>获取input内容</button>
        </div>
      </div>
    )
  }
}

export default App
