import React from 'react'; // react核心内容（创建虚拟dom、创建组件、hooks...）
import ReactDOM from 'react-dom/client'; // 渲染真实dom相关内容
import './index.css';
import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App />
);