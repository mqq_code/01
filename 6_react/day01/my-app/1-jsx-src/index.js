import React from 'react'; // react核心内容（创建虚拟dom、创建组件、hooks...）
import ReactDOM from 'react-dom/client'; // 渲染真实dom相关内容
import './index.css';

// 创建虚拟dom
// const h1 = React.createElement('h1', {
//   className: 'title',
//   id: 'titleid'
// }, [
//   '开始学习',
//   React.createElement('b', {}, ['React！'])
// ])

// jsx: js + xml, 在js文件中可以写标签语法本质上是 React.createElement 的语法糖，jsx 语法最终需要由 babel 转译成 React.createElement
// 注意事项：
// 1. class => className
// 2. for => htmlFor
// 3. jsx中使用变量需要使用{}套起来，{}中只能渲染表达式
// 4. jsx中不能直接渲染对象，会报错
// 5. style属性必须传入对象

const title = "大标题"
const fn = () => title
const styleObj = { background: 'red' }
const num = 105
const arr = [
  {name: '小明', age: 22},
  {name: '小红', age: 23},
  {name: '小刚', age: 24},
  {name: '小李', age: 25}
]
// const arr = [
//   <li>姓名：小明, 年龄：22</li>,
//   <li>姓名：小明, 年龄：22</li>,
//   <li>姓名：小明, 年龄：22</li>,
//   <li>姓名：小明, 年龄：22</li>
// ]

const renderText = (n) => {
  if (n < 10) {
    return <div>我现在小于10</div>
  } else if (n >= 10 && n < 20) {
    return <div>我现在是 10 - 20</div>
  } else if (n >= 20 && n < 30) {
    return <div>我现在是 20 - 30</div>
  }
  return '我是默认值ß'
}

const box = <div className='box'>
  <h1 style={styleObj}>开始学习<b>React！</b></h1>
  <ul>
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>5</li>
  </ul>
  {/* react中的条件渲染 */}
  {num > 50 ?
    <div className='box1'>
      <label htmlFor="bbb">bbbb</label>
      <input type="checkbox" id='bbb' />
      <p>字符串: {title}</p>
      <p>数字: {100}</p>
      <p>布尔值: {true}</p>
      <p>布尔值: {false}</p>
      <p>null: {null}</p>
      <p>undefined: {undefined}</p>
      <p>arr: {[1,2,3,4,5,6,7]}</p>
      {/* <p>obj: {  { name: '小明'  }  }</p> */}
      <p>调用函数:{ fn() }</p>
    </div>
  :
    null
  }
  {num > 100 && <div className='box1'>num 大于 100</div>}
  {renderText(-1)}
  <hr />
  <h3>列表渲染</h3>
  <ul>
    {arr.map(item => <li key={item.name}>
      <p>姓名: {item.name}</p>
      <p>年龄: {item.age}</p>
    </li>)}
  </ul>
</div>


const root = ReactDOM.createRoot(document.getElementById('root'));
// 把虚拟dom渲染成真实dom
root.render(
  box
);