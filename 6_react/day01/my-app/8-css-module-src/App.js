import React, { Component, createRef } from 'react'
import Child from './componets/Child'

class App extends Component {
  state = {
    title: 'class组件标题',
    num: 10
  }
  childRef = createRef()
  render() {
    const { title } = this.state
    return (
      <div>
        <h1>{title}</h1>
        <button className='btn' onClick={() => {
          // 父组件通过ref调用子组件的方法
          this.childRef.current.add()
        }}>调用子组件add方法</button>
        <hr />
        <Child ref={this.childRef} />
      </div>
    )
  }
}

export default App
