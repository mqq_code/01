import React, { Component } from 'react'
import style from './Child.module.scss'
// css module 样式隔离
// 原理：加载 *.module.css 时动态生成类名
console.log(style)

class Child extends Component {
  state = {
    childcount: 0
  }
  add = () => {
    this.setState({
      childcount: this.state.childcount + 2
    })
  }
  render() {
    return (
      <div className='box'>
        <button className={style.btn} onClick={this.add}>+2</button>
        {this.state.childcount}
      </div>
    )
  }
}

export default Child
