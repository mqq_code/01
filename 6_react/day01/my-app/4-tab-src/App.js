import React, { Component, createRef } from 'react'

class App extends Component {
  state = {
    tabList: [
      { singer: '许嵩', songs: ['断桥残雪', '玫瑰花的葬礼', '幻听', '想象之中'] },
      { singer: '凤凰传奇', songs: ['最炫民族风', '全是爱', '月亮之上', '自由飞翔'] },
      { singer: '周杰伦', songs: ['稻香', '反方向的钟', '晴天', '七里香'] }
    ],
    curIndex: 2
  }
 
  render() {
    const { tabList, curIndex } = this.state
    console.log(tabList[curIndex])
    return (
      <div>
        <nav>
          {tabList.map((item, index) => {
            return <span
              className={curIndex === index ? 'active' : ''}
              key={item.singer}
              onClick={() => this.setState({ curIndex: index })}
              >{item.singer}</span>
          })}
        </nav>
        <ul>
          {tabList[curIndex].songs.map(item => <li key={item}>{item}</li>)}
        </ul>
      </div>
    )
  }
}

export default App
