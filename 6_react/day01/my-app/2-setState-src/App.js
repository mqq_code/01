// react组件
// 1. class 组件
// 2. 函数组件（16.8之前不能定义数据）
import React, { Component } from 'react'

class App extends Component {
  // 定义组件数据，类似vue组件中的data
  state = {
    title: 'class组件标题',
    num: 10,
    count: 0
  }
  addNum = () => {
    console.log('执行了 addNum')
    // react需要调用 this.setState 改变数据触发页面更新，vue更新数据页面自动更新
    // 传入setState的对象会和原本的state进行合并，然后触发render函数
    this.setState({
      num: this.state.num + 1,
      title: Math.random()
    })
  }
  changeNum = n => {
    console.log(n)
    this.setState({
      num: this.state.num + n
    })
  }
  add3 = () => {
    // setState 是异步函数，连续多次调用 setState 会合并更新，只渲染一次页面
    // this.setState({ num: this.state.num + 1 }) // 11
    // this.setState({ num: this.state.num + 1 }) // 12
    // this.setState({ num: this.state.num + 1 }) // 11

    // setState 传入函数可以通过参数获取到最新值
    this.setState(state => {
      console.log('最新的state', state)
      // 把返回的对象和原本的state进行合并
      return { num: state.num + 1 }
    }) 
    this.setState(state => {
      console.log('最新的state', state)
      return { num: state.num + 1 }
    })
    this.setState(state => {
      console.log('最新的state', state)
      return { num: state.num + 1 }
    })

    // 无法获取到最新数据
    console.log(this.state.num)
  }

  addCount = () => {
    // 异步更新
    this.setState({ count: this.state.count + 1 }, () => {
      // 等待页面更新后执行次函数，可以获取到最新的dom
      console.log(document.querySelector('.p').innerHTML, this.state.count)
    })
  }

  // 渲染虚拟dom，调用组件时立即执行次函数
  render() {
    console.log('render函数', this.state)
    return (
      <div>
        <h1>{this.state.title}</h1>
        <p>num: {this.state.num}</p>
        {/* 调用函数不传参数 */}
        <button onClick={this.addNum}>按钮</button>
        {/* 调用函数传参数 */}
        <button onClick={() => this.changeNum(1)}>+</button>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {/* 多次调用更新函数 */}
        <button onClick={this.add3}>+3</button>
        <ul>
          {[1,2,3,4].map(item => <li key={item}>{item}</li>)}
        </ul>
        <hr />
        <button onClick={this.addCount}>count+</button>
        <p className='p'>{this.state.count}</p>
      </div>
    )
  }
}

export default App
