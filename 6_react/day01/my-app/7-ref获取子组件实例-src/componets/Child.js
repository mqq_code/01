import React, { Component } from 'react'

class Child extends Component {
  state = {
    childcount: 0
  }
  add = () => {
    this.setState({
      childcount: this.state.childcount + 2
    })
  }
  render() {
    return (
      <div className='box'>
        <button onClick={this.add}>+2</button>
        {this.state.childcount}
      </div>
    )
  }
}

export default Child
