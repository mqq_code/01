import React, { Component } from 'react'

class Child extends Component {
  render() {
    console.log('接收父组件参数', this.props)
    const { rootTitle, rootNum, changeTitle, header, footer, children } = this.props
    return (
      <div className='box'>
        {header}
        <h3>Child</h3>
        <p>父组件title: {rootTitle}</p>
        <p>父组件num: {rootNum}</p>
        <button onClick={() => changeTitle(Math.random())}>修改父组件title</button>
        <hr />
        {children}
        <hr />
        {footer}
      </div>
    )
  }
}

export default Child
