import React, { Component } from 'react'
import Child from './componets/Child'

class App extends Component {
  state = {
    title: 'class组件标题',
    num: 10
  }
  changeTitle = title => {
    console.log('接收子组件的数据', title)
    this.setState({ title })
  }
  render() {
    const { title, num } = this.state
    return (
      <div>
        <h1>{title}</h1>
        <div>
          {num > 0 &&
            <>
              <button onClick={() => this.setState({ num: num - 1 })}>-</button>
              {num}
            </>
          }
          <button onClick={() => this.setState({ num: num + 1 })}>+</button>
        </div>
        <hr />
        <Child
          rootTitle={title}
          rootNum={num}
          arr={[1,2,3,4,5,6]}
          obj={{ name: '小明' }}
          changeTitle={this.changeTitle}
          header={<div style={{ background: 'red' }}>我是header</div>}
          footer={<div style={{ background: 'green' }}>我是底部</div>}
        >
          <table border={1}>
            <thead>
              <tr>
                <th>标题1</th>
                <th>标题2</th>
                <th>标题3</th>
                <th>标题4</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>内容1</td>
                <td>内容2</td>
                <td>内容3</td>
                <td>内容4</td>
              </tr>
            </tbody>
          </table>
          <i>我是i标签</i>
        </Child>
      </div>
    )
  }
}

export default App
