import React, { Component } from 'react'
import Child from './componets/Child'
import Child2 from './componets/Child2'
import Child3 from './componets/Child3'

let a = 100

class App extends Component {
  // 挂载阶段：初始化
  constructor(props) {
    // super: 父类的构造函数
    super(props)
    this.state = {
      title: 'class组件标题',
      num: 10
    }
    // console.log('初始化 state 和 props, constructor')
  }

  // state = {
  //   title: 'class组件标题',
  //   num: 10
  // }

  // componentDidMount() {
  //   // 可以获取到页面上的dom元素、调接口、定时器......
  //   console.log('组件挂载完成')
  // }
  // componentDidUpdate() {
  //   console.log('组件更新完成，可以获取到最新的dom')
  // }

  render() {
    // 不要再 render 函数中调用 setState
    const { title, num } = this.state
    console.log('App render 渲染')
    return (
      <div>
        <h1>{title}</h1>
        <input type="text" value={title} onChange={e => this.setState({ title: e.target.value })} />
        <button onClick={() => this.setState({ num: num - 1 })}>{num}-</button>
        <button onClick={() => this.setState({ num: num + 1 })}>{num}+</button>
        <hr />
        {/* {a}
        <button onClick={() => {
          a++
          console.log(a)
          // 强制更新
          this.forceUpdate()
        }}>a++</button> */}
        <hr />
        {num > 12 && <Child />}
        <hr />
        <Child2 />
        <hr />
        <Child3 num={num} />
      </div>
    )
  }
}

export default App

