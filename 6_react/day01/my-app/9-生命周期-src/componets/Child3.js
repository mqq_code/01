import React, { Component } from 'react'

export default class Child3 extends Component {

  state = {
    num: this.props.num
  }

  static getDerivedStateFromProps(props, state) {
    // 返回的数据会合并到state中
    return {
      num: props.num
    }
  }

  getSnapshotBeforeUpdate() {
    // 组件更新时，return 的数据会传给 componentDidUpdate
    return document.querySelector('.span').clientWidth
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate, snapshot:', snapshot)
    console.log('最新的span', document.querySelector('.span').clientWidth)
  }

  render() {
    return (
      <div>
        <h2>Child3</h2>
        <p>state.num: {this.state.num}</p>
        <p>props.num: <span style={{ float: 'right' }} className='span'>{this.props.num}</span></p>
      </div>
    )
  }
}
