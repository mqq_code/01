import React, { Component, PureComponent } from 'react'

// PureComponent: 优化组件性能，会自动比较最新的props和state与当前的props和state的数据是否发生改变
// shouldComponentUpdate：优化组件性能，return true 继续执行更新，return fasle 停止更新
export default class Child2 extends PureComponent {

  state = {
    n: 0
  }

  // 判断组件是否需要更新
  // return true 继续执行更新
  // return fasle 停止更新
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('%c shouldComponentUpdate', 'color: red')
  //   console.log('最新的数据', nextProps, nextState)
  //   console.log('当前的数据', this.props, this.state)
  //   if (nextState.n !== this.state.n) {
  //     return true
  //   }
  //   return false
  // }

  render() {
    console.log('%c child2渲染了', 'color: red')
    return (
      <div>
        <h3>Child2</h3>
        <button onClick={() => this.setState({ n: this.state.n + 1 })}>{this.state.n}</button>
      </div>
    )
  }
}
