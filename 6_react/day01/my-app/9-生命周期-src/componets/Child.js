import React, { Component } from 'react'
import style from './Child.module.scss'

class Child extends Component {
  state = {
    childcount: 20
  }
  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({ childcount: this.state.childcount - 1 })
      console.log(this.state.childcount - 1)
    }, 1000)
  }

  componentWillUnmount() {
    console.log('组件销毁，清除异步任务')
    clearInterval(this.timer)
  }

  render() {
    return (
      <div className='box'>
        倒计时: {this.state.childcount}
      </div>
    )
  }
}

export default Child
