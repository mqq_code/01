import axios from 'axios'
import { message } from 'antd'

axios.defaults.baseURL = 'http://121.89.213.194:9001'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  config.headers.Authorization = localStorage.getItem('token')
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  if (error.response.status === 401) {
    // 登录失效，自动跳转到登录页面
    message.error('登录信息失效，请重新登录！')
    setTimeout(() => {
      window.location.href = '/login'
    }, 500)
  }
  return Promise.reject(error);
});

export const getUserInfoApi = () => {
  return axios.get('/api/user/info')
}

export const getUserListApi = (params) => {
  return axios.get('/api/userlist', {
    params
  })
}

export const delUserApi = (id) => {
  return axios.post('/api/user/delete', {
    id
  })
}
export const createUserApi = (params) => {
  return axios.post('/api/user/create', params)
}


