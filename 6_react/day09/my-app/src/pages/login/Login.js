import React, { useState } from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, message, Form, Input } from 'antd'
import classNames from 'classnames'
import style from './login.module.scss'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const Login = () => {
  const [form] = Form.useForm()
  const [messageApi, contextHolder] = message.useMessage()
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()

  const login = async (params) => {
    setLoading(true)
    const res = await axios.post('http://10.37.19.16:9001/api/login', params)
    setLoading(false)
    if (res.data.code === 0) {
      localStorage.setItem('token', res.data.token)
      messageApi.open({
        type: 'success',
        content: '登录成功'
      })
      navigate('/')
    } else {
      messageApi.open({
        type: 'error',
        content: res.data.msg
      })
    }
  }

  const submit = async () => {
    try {
      const values = await form.validateFields()
      login(values)
    } catch(e) {
      console.log(e)
    }
  }
  return (
    <div className={classNames('page', style.login)}>
      {contextHolder}
      <Form className={style.loginForm} form={form}>
        <h3>登录</h3>
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: '请输入用户名!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>
        <Form.Item>
          <Button loading={loading} type="primary" block onClick={submit}>登录</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login