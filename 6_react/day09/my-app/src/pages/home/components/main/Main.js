import React, { useEffect, useState } from 'react'
import style from './main.module.scss'
import { getUserListApi, delUserApi, createUserApi } from '../../../../api'
import { message, Table, Space, Image, Button, Popconfirm, Modal, Form, Input, InputNumber, Select } from 'antd'
import SearchForm from '../../../../components/searchForm/SearchForm'

const Main = () => {
  const [messageApi, contextHolder] = message.useMessage()
  const [tableData, setTableData] = useState([])
  const [total, setTotal] = useState(0)
  const [query, setQuery] = useState({ page: 2, pagesize: 10 })
  const [filterQuery, setFilterQuery] = useState({})
  const [loading, setLoading] = useState(false)
  const [show, setShow] = useState(false)
  const [form] = Form.useForm()

  const getlist = async () => {
    setLoading(true)
    const res = await getUserListApi({...query, ...filterQuery})
    setLoading(false)
    if (res.data.code === 0) {
      setTableData(res.data.values.list)
      setTotal(res.data.values.total)
    } else {
      messageApi.open({
        type: 'error',
        content: res.data.msg
      })
    }
  }

  const del = async id => {
    const res = await delUserApi(id)
    if (res.data.code === 0) {
      messageApi.open({
        type: 'success',
        content: '删除成功'
      })
      getlist()
    } else {
      messageApi.open({
        type: 'error',
        content: res.data.msg
      })
    }
  }

  useEffect(() => {
    getlist()
  }, [query, filterQuery])

  const columns = [
    {
      title: '序号',
      dataIndex: 'no',
      key: 'no'
    },
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'username'
    },
    {
      title: '头像',
      dataIndex: 'avatar',
      key: 'avatar',
      render: (avatar, record) => {
        if (!avatar) return null
        return <Image width={150} src={avatar} />
      }
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age'
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (sex, record) => {
        return sex === 1 ? '男' : '女'
      }
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email'
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return (
          <Space>
            <Button type="primary" size="small">编辑</Button>
            <Popconfirm
              title="警告"
              description="确定要删除此条数据吗?"
              okText="删除"
              cancelText="取消"
              onConfirm={() => del(record.id)}
            >
              <Button danger size="small">删除</Button>
            </Popconfirm>
          </Space>
        )
      }
    }
  ]

  const createUser = async (values) => {
    console.log('调用新增接口', values)
    const res = await createUserApi(values)
    if (res.data.code === 0) {
      messageApi.open({
        type: 'success',
        content: '新增成功'
      })
      setShow(false)
      getlist()
    } else {
      messageApi.open({
        type: 'error',
        content: res.data.msg
      })
    }
  }

  useEffect(() => {
    if (show) {
      form.resetFields()
    }
  }, [show])


  return (
    <div className={style.main}>
      {contextHolder}
      <SearchForm onSearch={setFilterQuery} />
      <Button type="primary" onClick={() => setShow(true)}>新增</Button>
      <Table
        loading={loading}
        dataSource={tableData}
        columns={columns}
        rowKey={record => record.id}
        pagination={{
          current: query.page,
          pageSize: query.pagesize,
          pageSizeOptions: [10, 20, 30, 40],
          total,
          showTotal: (total) => `共 ${total} 条数据`,
          onChange: (page, pagesize) => {
            // console.log('修改分页', page, pagesize)
            setQuery({ page, pagesize })
          }
        }}
      />
      <Modal
        title="新增"
        open={show}
        okText="确定"
        cancelText="取消"
        onOk={async () => {
          try {
              // 校验表单
            const values = await form.validateFields()
            createUser(values)
          } catch(e) {}
        }}
        onCancel={() => setShow(false)}
      >
        <Form form={form}>
          <Form.Item name="username" label="姓名" rules={[
            { required: true, message: '请输入用户名!' }
          ]}>
            <Input placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item name="sex" label="性别" rules={[
            { required: true, message: '请选择性别!' }
          ]}>
            <Select
              allowClear
              placeholder="选择性别"
              options={[
                { value: 1, label: '男' },
                { value: 0, label: '女' }
              ]}
            />
          </Form.Item>
          <Form.Item name="age" label="年龄" rules={[
            { required: true, message: '请输入年龄!' },
            { type: 'number', message: '请输入数字!' }
          ]}>
            <InputNumber placeholder="请输入年龄" />
          </Form.Item>
          <Form.Item name="password" label="密码" rules={[
            { required: true, message: '请输入密码!' },
            { min: 3, max: 16, message: '长度限制在 3-16 位!' }
          ]}>
            <Input.Password placeholder="请输入密码" />
          </Form.Item>
          <Form.Item name="email" label="邮箱" rules={[
            { required: true, message: '请输入邮箱!' },
            { type: 'email', message: '邮箱格式错误!' }
          ]}>
            <Input placeholder="请输入邮箱" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default Main