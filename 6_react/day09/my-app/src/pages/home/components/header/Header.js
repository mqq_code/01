import React from 'react'
import style from './header.module.scss'
import { useSelector } from 'react-redux'
import { UserOutlined, DownOutlined, SmileOutlined } from '@ant-design/icons'
import { Avatar, Space, Dropdown } from 'antd'
import { useNavigate } from 'react-router-dom'

const Header = () => {
  const navigate = useNavigate()
  const user = useSelector(s => s.user)

  const handleClick = ({ key }) => {
    if (key === '2') {
      localStorage.removeItem('token')
      navigate('/login')
    }
  }

  const items = [
    {
      key: '1',
      label: <span onClick={() => {
        alert(1111)
      }}>个人设置</span>,
    },
    {
      key: '2',
      label: '退出登录',
    }
  ]

  return (
    <div className={style.header}>
      <Space>
        <Avatar shape="square" size="large" url={user.avatar} icon={<UserOutlined />} />
        <Dropdown
          menu={{
            items,
            onClick: handleClick
          }}
        >
          <Space>
            {user.username}
            <DownOutlined />
          </Space>
        </Dropdown>
      </Space>
    </div>
  )
}

export default Header