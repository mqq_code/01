import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { getUserInfo } from '../../store/features/userSlice'
import Header from './components/header/Header'
import Main from './components/main/Main'
import style from './home.module.scss'

const Home = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserInfo())
  }, [])


  return (
    <div className={style.home}>
      <Header />
      <Main />
    </div>
  )
}

export default Home