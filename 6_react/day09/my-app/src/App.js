import React from 'react'
import { useRoutes } from 'react-router-dom'
import Home from './pages/home/Home'
import Login from './pages/login/Login'

const routerConfig = [
  { path: '/', element: <Home /> },
  { path: '/login', element: <Login /> },
]


const App = () => {
  const routes = useRoutes(routerConfig)

  return routes
}

export default App