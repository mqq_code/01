import React from 'react'
import { LockOutlined, DollarOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Form, Input, Row, Col, Space, Select } from 'antd'

const SearchForm = (props) => {
  const [form] = Form.useForm()
  const onFinish = (values) => {
    const params = {}
    // 过滤 undefined
    Object.keys(values).forEach(key => {
      if (values[key] !== undefined) {
        params[key] = values[key]
      }
    })
    // 通知父组件开始搜索
    props.onSearch(params)
  }
  const reset = () => {
    form.resetFields()
    form.submit()
  }
  return (
    <div>
      <Form form={form} labelCol={{ span: 6 }} wrapperCol={{ span: 18 }} onFinish={onFinish}>
        <Row>
          <Col span={8}>
            <Form.Item name="id" label="id">
              <Input prefix={<DollarOutlined />} placeholder="用户id" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="username" label="姓名">
              <Input prefix={<UserOutlined />} placeholder="用户名" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="age" label="年龄">
              <Input prefix={<LockOutlined />} placeholder="年龄" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item name="sex" label="性别">
              <Select
                allowClear
                placeholder="选择性别"
                options={[
                  { value: 1, label: '男' },
                  { value: 0, label: '女' }
                ]}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="email" label="邮箱">
              <Input prefix={<UserOutlined />} placeholder="邮箱" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item wrapperCol={{ offset: 6 }}>
              <Space>
                <Button type="primary" htmlType="submit">搜索</Button>
                <Button type="primary" ghost onClick={reset}>重置</Button>
              </Space>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default SearchForm