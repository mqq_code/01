import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { getUserInfoApi } from '../../api'

// 创建异步action
export const getUserInfo = createAsyncThunk('user/getUserInfo', async () => {
  const res = await getUserInfoApi()
  return res.data
})


export const userSlice = createSlice({
  name: 'user',
  initialState: {
    avatar: '',
    id: '',
    username: ''
  },
  reducers: {

  },
  extraReducers: builder => {
    builder
      .addCase(getUserInfo.fulfilled, (state, action) => {
        // console.log(action.payload.values)
        const { avatar, username, id } = action.payload.values
        state.avatar = avatar
        state.username = username
        state.id = id
      })
      .addCase(getUserInfo.rejected, (state, action) => {
        console.log('失败', action)
      })
  }
})


export default userSlice.reducer