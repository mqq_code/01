const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')

const app = http.createServer((req, res) => {
  console.log(req.url)
  // 解析用户请求的 url
  // pathname：url 的 path 部分
  // query：url ？后的参数
  const { pathname, query } = url.parse(req.url, true)
  // 根据path去dist文件夹中查找文件
  const filePath = path.join(__dirname, './dist', pathname)
  // 如果是 / 直接返回首页
  if (pathname === '/') {
    res.end(fs.readFileSync(path.join(__dirname, './dist/index.html')))
    return
  }
  // 判断path对应的文件在dist中是否存在，存在就返回静态资源
  if (fs.existsSync(filePath)) {
    res.end(fs.readFileSync(filePath))
    return
  }


  
  // 处理接口
  if (pathname === '/api/list' && req.method ===  'GET') {
    res.setHeader('Content-Type', 'application/json;charset=utf-8;')
    console.log('前端传过来的参数', query)
    // 解析前端传过来的参数
    const { page, pagesize, keyword = '' } = query
    if (!page || !pagesize) {
      res.end(JSON.stringify({
        code: -1,
        msg: '参数错误'
      }))
      return
    }

    // 读取json文件
    let data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
    // 搜索关键字
    if (keyword.trim()) {
      data = data.filter(item => item.name.includes(keyword))
    }
    // page: 1  pagesize: 10  data.slice(0, 10)
    // page: 2  pagesize: 10  data.slice(10, 20)
    // page: 3  pagesize: 10  data.slice(20, 30)
    
    // 返回数据给前端
    res.end(JSON.stringify({
      code: 0,
      msg: 'success',
      // 总条数
      total: data.length,
      // 根据参数截取数据给前端
      values: data.slice((page - 1) * pagesize, page * pagesize)
    }))
    return
  } else if (pathname === '/api/del' && req.method ===  'POST') {
    res.setHeader('Content-Type', 'application/json;charset=utf-8;')
    let params = ''
    // post 接收前端传过来的参数
    req.on('data', chunk => {
      params += chunk
    })
    req.on('end', () => {
      // 接收完成
      params = JSON.parse(params)
      console.log('/api/del 参数:', params)
      // 读取data.json
      const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
      // 根据id查找对应的数据，删除数据
      const index = data.findIndex(item => item.id === params.id)
      if (index === -1) {
        res.end(JSON.stringify({
          code: -1,
          msg: '参数错误'
        }))
      } else {
        // 删除数据，更新数据
        data.splice(index, 1)
        fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))
        // 返回成功
        res.end(JSON.stringify({
          code: 0,
          msg: 'success'
        }))
      }
    })
    return
  }



  // 找不到就返回404
  // 设置状态码
  res.statusCode = 404
  // 设置相应头，通知浏览器返回的内容类型
  res.setHeader('Content-Type', 'text/html;charset=utf-8')
  res.end(`
    <div>
      <h1>404</h1>
      <h2>访问的内容不存在</h2>
      <a href="./index.html">去首页</a>
    </div>
  `)
})


const PORT = 10086
app.listen(PORT, () => {
  console.log(`服务启动成功 http://localhost:${PORT}`)
  console.log(`服务启动成功 http://127.0.0.1:${PORT}`)
  console.log(`服务启动成功 http://10.37.19.14:${PORT}`)
})