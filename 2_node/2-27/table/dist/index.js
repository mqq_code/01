
let page = 1
let pagesize = 10
let totalPage = 0
let keyword = ''

function getData() {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    // 请求接口时如果没有写 协议、域名、端口 会自动把当前页面的协议、域名、端口拼接到请求路径前
    xhr.open('get', `/api/list?page=${page}&pagesize=${pagesize}&keyword=${keyword}`)
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        const data = JSON.parse(xhr.responseText)
        resolve(data)
      }
    }
    xhr.send()
  })
}

function renderPagination(num) {
  let innerHTML = '<button data-type="prev">上一页</button>'
  innerHTML += new Array(num).fill(0).map((v, i) => {
    if (i + 1 === page) {
      return `<button class="active" data-page="${i + 1}">${i + 1}</button>`
    }
    return `<button data-page="${i + 1}">${i + 1}</button>`
  }).join('')
  innerHTML += '<button data-type="next">下一页</button>'
  $('.pagination').innerHTML = innerHTML
}

function renderTable(array) {
  $('tbody').innerHTML = array.map(item => {
    return `
      <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.age}</td>
        <td>${item.sex}</td>
        <td>${item.grade}</td>
        <td>
          <button data-id="${item.id}" class="del-btn">删除</button>
        </td>
      </tr>
    `
  }).join('')
}

async function init(){
  const res = await getData()
  // 渲染table数据
  renderTable(res.values)
  // 计算共多少页，渲染分页按钮
  totalPage = Math.ceil(res.total / pagesize)
  renderPagination(totalPage)
}

init()

// 删除
$('table').addEventListener('click', e => {
  // 删除按钮
  if (e.target.classList.contains('del-btn')) {
    // 获取点击按钮的id
    const id = e.target.getAttribute('data-id')
    // 调用删除接口把id传给后端
    const xhr = new XMLHttpRequest()
    xhr.open('post', '/api/del')
    xhr.onreadystatechange = async () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        const data = JSON.parse(xhr.responseText)
        if (data.code === 0) {
          alert('删除成功')
          // 删除成功，更新table数据
          init()
        } else {
          alert(data.msg)
        }
      }
    }
    // 设置请求头，通知接口传过去的参数类型
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8')
    xhr.send(JSON.stringify({
      id: Number(id),
      a: 100,
      b: 200
    }))
  }
})

// 分页
$('.pagination').addEventListener('click', async e => {
  if (e.target.nodeName === 'BUTTON') {
    // 获取按钮类型，区分上一页和下一页
    const type = e.target.getAttribute('data-type')
    $('button.active')?.classList.remove('active')
    if (type) {
      if (type === 'next') {
        console.log(page, totalPage)
        page = Number(page) === totalPage ? totalPage : Number(page) + 1
        
      } else {
        page = Number(page) === 1 ? 1 : page - 1
      }
      $(`button[data-page="${page}"]`).classList.add('active')
    } else {
      // 高亮
      e.target.classList.add('active')
      // 获取点击的按钮的page
      page = e.target.getAttribute('data-page')
    }

    // 重新调用接口
    const res = await getData()
    // 渲染table数据
    renderTable(res.values)
  }
})

// 切换每页几条
$('select').value = pagesize
$('select').addEventListener('change', e => {
  pagesize = e.target.value
  init()
})

// 搜索
$('.search').addEventListener('click', () => {
  keyword = $('.search-inp').value
  init()
})


function $(el) {
  return document.querySelector(el)
}

// new Table({
//   el: ,
//   column: [
//     {
//       title: 'id'
//     },
//     {
//       title: '姓名'
//     },
//     {
//       title: '年龄'
//     },
//     {
//       title: '性别'
//     }
//   ],
//   data: [
//     { id: 0, name: '小明', age: 10, sex: '男'}
//   ]
// })

