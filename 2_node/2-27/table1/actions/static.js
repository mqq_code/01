const fs = require('fs')
const path = require('path')
const url = require('url')

const static = (req, res, staticDir) => {
  const { pathname, query } = url.parse(req.url, true)
  // 根据path去dist文件夹中查找文件
  const filePath = path.join(__dirname, '../', staticDir, pathname)
  // 如果是 / 直接返回首页
  if (pathname === '/') {
    res.end(fs.readFileSync(path.join(__dirname, '../', staticDir, 'index.html')))
    return false
  }
  // 判断path对应的文件在dist中是否存在，存在就返回静态资源
  if (fs.existsSync(filePath)) {
    res.end(fs.readFileSync(filePath))
    return false
  }
  return true
}

module.exports = static