const fs = require('fs')
const path = require('path')
const url = require('url')


const getListApi = function(req, res) {
  const { pathname, query } = url.parse(req.url, true)
  res.setHeader('Content-Type', 'application/json;charset=utf-8;')
  // 解析前端传过来的参数
  const { page, pagesize, keyword = '' } = query
  if (!page || !pagesize) {
    res.end(JSON.stringify({
      code: -1,
      msg: '参数错误'
    }))
    return
  }

  // 读取json文件
  let data = JSON.parse(fs.readFileSync(path.join(__dirname, '../', './data.json'), 'utf-8'))
  // 搜索关键字
  if (keyword.trim()) {
    data = data.filter(item => item.name.includes(keyword))
  }
  // page: 1  pagesize: 10  data.slice(0, 10)
  // page: 2  pagesize: 10  data.slice(10, 20)
  // page: 3  pagesize: 10  data.slice(20, 30)
  
  // 返回数据给前端
  res.end(JSON.stringify({
    code: 0,
    msg: 'success',
    // 总条数
    total: data.length,
    // 根据参数截取数据给前端
    values: data.slice((page - 1) * pagesize, page * pagesize)
  }))
}

module.exports = getListApi