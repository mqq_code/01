const fs = require('fs')
const path = require('path')
const url = require('url')

const delApi = function(req, res) {
  const { pathname, query } = url.parse(req.url, true)
  res.setHeader('Content-Type', 'application/json;charset=utf-8;')
  let params = ''
  // post 接收前端传过来的参数
  req.on('data', chunk => {
    params += chunk
  })
  req.on('end', () => {
    // 接收完成
    params = JSON.parse(params)
    console.log('/api/del 参数:', params)
    // 读取data.json
    const data = JSON.parse(fs.readFileSync(path.join(__dirname, '../', './data.json'), 'utf-8'))
    // 根据id查找对应的数据，删除数据
    const index = data.findIndex(item => item.id === params.id)
    if (index === -1) {
      res.end(JSON.stringify({
        code: -1,
        msg: '参数错误'
      }))
    } else {
      // 删除数据，更新数据
      data.splice(index, 1)
      fs.writeFileSync(path.join(__dirname, '../', './data.json'), JSON.stringify(data))
      // 返回成功
      res.end(JSON.stringify({
        code: 0,
        msg: 'success'
      }))
    }
  })
}

module.exports = delApi