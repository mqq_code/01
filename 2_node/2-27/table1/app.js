const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')
const static = require('./actions/static')
const getListApi = require('./actions/getListApi')
const delApi = require('./actions/delApi')
const createApi = require('./actions/createApi')


const obj = {
  '/api/list GET': getListApi,
  '/api/del POST': delApi,
  '/api/create GET': createApi
}


const app = http.createServer((req, res) => {
  const { pathname, query } = url.parse(req.url, true)
  // 静态资源
  if (!static(req, res, './dist')) return

  // 处理接口
  if (obj[`${pathname} ${req.method}`]) {
    obj[`${pathname} ${req.method}`](req, res)
    return
  }

  // 找不到就返回404
  // 设置状态码
  res.statusCode = 404
  // 设置相应头，通知浏览器返回的内容类型
  res.setHeader('Content-Type', 'text/html;charset=utf-8')
  res.end(`
    <div>
      <h1>404</h1>
      <h2>访问的内容不存在</h2>
      <a href="./index.html">去首页</a>
    </div>
  `)
})


const PORT = 10086
app.listen(PORT, () => {
  console.log(`服务启动成功 http://localhost:${PORT}`)
  console.log(`服务启动成功 http://127.0.0.1:${PORT}`)
  console.log(`服务启动成功 http://10.37.19.14:${PORT}`)
})
