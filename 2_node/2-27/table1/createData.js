const fs = require('fs')
const path = require('path')

const firstName = ['张','王', '李', '赵', '诸葛', '欧阳', '西门', '慕容', '上官', '司马', '令狐', '东方', '南宫', '爱新觉罗', '叶赫那拉', '钮钴禄', '老']
const lastName = ['铁柱', '狗蛋', '翠花', '铁蛋', '钢弹', '二狗', '铁锤', '德刚', '草', '三', '四', '六']


function createData(num) {
  const data = []
  for (let i = 0; i < num; i++) {
    const fullName = firstName[Math.floor(Math.random() * firstName.length)] + lastName[Math.floor(Math.random() * lastName.length)]
    const arr = data.filter(v => v.name.includes(fullName))
    data.push({
      id: i,
      name: fullName + (arr.length + 1),
      age: Math.floor(Math.random() * 30) + 18,
      sex: Math.floor(Math.random() * 3),
      grade: Math.floor(Math.random() * 101)
    })
  }
  return data
}


const data = createData(400)

fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))
