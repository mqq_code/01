const $ = el => document.querySelector(el)

const token = localStorage.getItem('token')
if (!token && location.pathname !== '/login.html') {
  alert('登录信息失效，请重新登录')
  location.href = './login.html'
}