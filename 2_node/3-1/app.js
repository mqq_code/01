const express = require('express')
const fs = require('fs')
const path = require('path')
// token 插件
const jwt = require('jsonwebtoken')
// 接收文件
const multer = require('multer')
// 设置文件的保存位置
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // 设置文件保存的文件夹
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    // 设置文件保存的名字
    cb(null, `${file.fieldname}_${file.originalname}`)
  }
})
const upload = multer({ storage: storage })

const secretKey = 'bw_zyx'
// 生成token: jwt.sign(token保存的数据, 密钥, { expiresIn: token有效期 })
// const token = jwt.sign({ username: '小明', id: 123 }, secretKey, { expiresIn: '10s' })
// 解析token: jwt.verify(token, secretKey)
// 解析成功返回token保存的数据，解析失败报错


const app = express()

// 处理静态资源
app.use(express.static('./dist'))
app.use(express.static('./uploads'))
// post接口接收json类型的参数
app.use(express.json())

// 读取用户列表
const readUser = () => JSON.parse(fs.readFileSync(path.join(__dirname, './data/userlist.json'), 'utf-8'))
// 修改用户列表
const writeUser = userlist => fs.writeFileSync(path.join(__dirname, './data/userlist.json'), JSON.stringify(userlist))


// 注册
app.post('/api/register', (req, res) => {
  const { username, password } = req.body // 获取前端参数
  const userlist = readUser() // 读取用户列表
  const index = userlist.findIndex(v => v.username === username) // 查找用户名是否存在
  if (index > -1) {
    res.send({
      code: -1,
      msg: '用户名已存在'
    })
  } else {
    // 用户名不存在就添加到数据库中
    userlist.push({
      id: Date.now(),
      username,
      password
    })
    // 更新数据库
    writeUser(userlist)
    res.send({
      code: 0,
      msg: '注册成功'
    })
  }
})

// 登录
app.post('/api/login', (req, res) => {
  const { username, password } = req.body // 获取前端参数
  const userlist = readUser() // 读取用户列表
  const user = userlist.find(v => v.username === username && v.password === password) // 查找用户名是否存在
  if (user) {
    // 生成token返回给前端
    const token = jwt.sign({ id: user.id, username: user.username }, secretKey, { expiresIn: '2h' })
    res.send({
      code: 0,
      msg: '登录成功',
      token
    })
  } else {
    res.send({
      code: -1,
      msg: '用户名或密码错误'
    })
  }
})

app.use((req, res, next) => {
   // 从请求头获取token
   const token = req.headers.authorization
   // 解析token是否正确
   try {
     const data = jwt.verify(token, secretKey)
     // 根据token中的信息去用户列表中查找数据
     const userlist = readUser()
     const user = userlist.find(v => v.id === data.id)
     if (user) {
      // token验证通过，把解析的用户信息存到req对象中，继续往下执行
        req.user = user
        next()
     } else {
      res.status(401).send({ msg: '登录信息失效' })
     }
   } catch(e) {
     res.status(401).send({ msg: '登录信息失效' })
   }

  // // 继续往下执行
  // next()
})


// 获取个人信息
app.get('/api/user/info', (req, res) => {
  // console.log(req.user)
  res.send({
    code: 0,
    msg: '成功',
    values: req.user
  })
})

// upload.single('前端传过来的参数名')
app.post('/api/user/avatar', upload.single('avatar'), (req, res) => {
  console.log('前端传过来的图片', req.file)
  // 获取用户列表
  const userlist = readUser()
  // 根据token中的id查找用户下表
  const index = userlist.findIndex(v => v.id === req.user.id)
  // 给当前用户修改头像
  userlist[index].avatar = req.file.filename
  // 更新数据库
  writeUser(userlist)
  res.send({
    code: 0,
    msg: '成功',
  })
})


// 所有用户列表
app.get('/api/userlist', (req, res) => {
  res.send({
    code: 0,
    msg: '成功',
    values: readUser()
  })
})



app.listen(9000, () => {
  console.log('启动成功 http://10.37.19.15:9000')
})



// 200
// 301: 永久重定向
// 302: 临时重定向
// 304: 协商缓存
// 401: 登录失效
// 403: 没有权限
// 404: 访问的地址不存在
// 500: 服务器内部错误
