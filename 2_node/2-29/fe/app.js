const express = require('express')
const ip = require('ip')
const { createProxyMiddleware } = require('http-proxy-middleware');


const app = express()

app.use(express.static('./dist'))

// 添加请求代理
app.use('/api', createProxyMiddleware({ target: 'http://localhost:8000', changeOrigin: true }));




app.listen(9000, () => {
  console.log('启动成功 http://localhost:9000')
  console.log('启动成功 http://127.0.0.1:9000')
  console.log(`启动成功 http://${ip.address()}:9000`)
})
