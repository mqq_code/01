const express = require('express')
const ip = require('ip')

const app = express()


app.get('/api/jsonp/list', (req, res) => {
  const { callback } = req.query
  // 设置相应头
  res.setHeader('Content-Type', 'application/javascript;charset=utf-8')
  const obj = {
    code: 0,
    msg: 'jsonp/list 成功',
    values: [1,2,3,4,5,6,7,8]
  }
  res.send(`window.${callback}(${JSON.stringify(obj)});`)
})

app.get('/api/jsonp/list2', (req, res) => {
  const { callback } = req.query
  // 设置相应头
  res.setHeader('Content-Type', 'application/javascript;charset=utf-8')
  const obj = {
    code: 0,
    msg: 'jsonp/list2 成功',
    values: [1,2,3,4,5,6,7,8]
  }
  res.send(`window.${callback}(${JSON.stringify(obj)});`)
})






// 添加允许跨域访问的响应头
app.get('/api/list', (req, res) => {
  // res.setHeader('Access-Control-Allow-Origin', '*')

  res.send({
    code: 0,
    msg: '访问 8000 成功',
    values: [1,2,3,4,5,6,7,8]
  })
})



app.listen(8000, () => {
  console.log('启动成功 http://localhost:8000')
  console.log('启动成功 http://127.0.0.1:8000')
  console.log(`启动成功 http://${ip.address()}:8000`)
})
