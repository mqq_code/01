const fs = require('fs')
const path = require('path')

// 递归删除
function rm(filepath) {
  // 判断文件是否存在
  if (fs.existsSync(filepath)) {
    // 读取文件信息
    const info = fs.statSync(filepath)
    // 判断是文件还是文件夹
    if (info.isFile()) {
      fs.unlinkSync(filepath)
      console.log('文件删除成功', filepath)
    } else {
      // 删除文件夹，读取目录
      const menu = fs.readdirSync(filepath)
      // 遍历目录，依次删除
      menu.forEach(child => {
        const childPath = path.join(filepath, child)
        // 递归调用删除子目录
        rm(childPath)
      })
      fs.rmdirSync(filepath)
      console.log('文件夹删除成功', filepath)
    }
  } else {
    console.log('要删除的文件不存在')
  }
}

// rm(path.join(__dirname, './text'))
// rm(path.join(__dirname, './aaa.js'))


function getFileToJson(filepath) {
  if (!fs.existsSync(filepath)) {
    return new Error(`${filepath} 不存在`)
  }
  const info = fs.statSync(filepath)
  const obj = {
    path: filepath,
    isFile: info.isFile()
  }
  if (info.isDirectory()) {
    const childMenu = fs.readdirSync(filepath)
    obj.children = childMenu.map(child => {
      const childPath = path.join(filepath, child)
      return getFileToJson(childPath)
    })
  }

  return obj
}

const data = getFileToJson(path.join(__dirname, './text'))


fs.writeFileSync('./data.json', JSON.stringify(data))

