
require('./b.js')


const arr = '1111'

console.log('a.js', arr)

const objA = {
  name: '我是a'
}
const sum = (a, b) => {
  return a + b
}

// 抛出变量
module.exports = {
  objA,
  sum
}

// exports.objA = objA
// exports.sum = sum