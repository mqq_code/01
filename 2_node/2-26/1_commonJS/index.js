// CommonJS 模块化规范: 每个 js 就是一个模块，在 js 文件中可以引入其他的 js 文件，每个 js 文件都是单独的作用域

// 引入: require(文件路径)
// 接收 a.js 抛出的变量
const moduleA = require('./a.js')
console.log('moduleA', moduleA)

// const { objA, sum } = require('./a.js')
// console.log('moduleA', objA)


const arr = [1,2,3]

console.log('index.js', 123, arr)


for (let i = 0; i < 10; i++) {
  console.log(i)
}



