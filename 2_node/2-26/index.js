const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')

// 创建服务器应用
const app = http.createServer((request, response) => {
  // request: 请求的信息
  // response: 响应的信息
  console.log('请求的 path', request.url)
  console.log('请求方式', request.method)

  // 解析请求的url
  const { pathname, query } = url.parse(request.url, true)

  // 去dist中查找是否有此文件
  const fullPath = path.join(__dirname, `../../js/2-20/music${pathname}`)
  if (fs.existsSync(fullPath)) {
    // 把文件返回给用户
    response.end(fs.readFileSync(fullPath))
    return 
  }
  
  // 定义接口
  if (pathname === '/api/list' && request.method === 'POST') {
    response.setHeader('Content-Type', 'application/json;charset=utf-8;')
    response.end(JSON.stringify({
      name: '小明',
      hobby: [1,2,3,4,5,6]
    }))
    return
  }


  // 设置状态码
  response.statusCode = 404
  // 设置相应头
  response.setHeader('Content-Type', 'text/html;charset=utf-8;')
  response.end('<h1 style="color: red">查找的文件不存在</h1>')
})

// 监听端口: 0 - 65535
app.listen(9000, () => {
  console.log('服务启动成功 http://10.37.19.13:9000')
})
