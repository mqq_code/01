// fs: node 内置模块文件操作
const fs = require('fs')
// path: 处理路径
const path = require('path')

// fs 使用相对路径时，相对的是执行命令时终端所在的路径，使用fs时最好使用绝对路径

// 读取文件
// try {
//   const data = fs.readFileSync('./text.txt', 'utf-8')
//   console.log(data)
// } catch(e) {
//   console.log('兜底逻辑', e)
// }


// fs.readFile('./text.txt', 'utf-8', (err, data) => {
//   if (!err) {
//     console.log('成功', data)
//   } else {
//     console.log('失败', err)
//   }
// })

// console.log('end')

// 写文件
// fs.writeFile('./test.txt', `${new Date().toLocaleString()}: 我是新内容`, err => {
//   if (!err) {
//     console.log('成功')
//   }
// })

// 追加内容
// fs.appendFile('./test.txt', `${new Date().toLocaleString()}: 我是新内容 ${Math.random()} \n`, err => {
//   if (!err) {
//     console.log('成功')
//   }
// })


// 删除文件: unlink
// 文件是否存在: existsSync
// if (fs.existsSync('./test.txt')) {
//   fs.unlinkSync('./test.txt')
// }


// 拷贝文件
// fs.copyFileSync('./笔记.md', './1_commonJS/新笔记.md')


// 修改文件名称
// fs.renameSync('./1_commonJS/旧笔记.md', './笔记.md', )

// 获取文件信息
// fs.stat('./笔记.md', (err, data) => {
//   if (!err) {
//     console.log('成功')
//     console.log(data)
//     console.log('文件大小', data.size)
//     console.log('是不是文件：', data.isFile())
//     console.log('是不是文件夹：', data.isDirectory())
//   }
// })

// 创建文件夹
// fs.mkdir('./aa', err => {
//   console.log(err)
// })

// 删除文件夹(只能删除空文件夹)
// fs.rmdir('./bb', err => {
//   console.log(err)
// })


// 读取目录
// fs.readdir('./bb', (err, data) => {
//   if (!err) {
//     console.log(data)
//   }
// })


// console.log('当前文件夹的绝对路径', __dirname)
// console.log('当前文件的绝对路径', __filename)

// console.log(path.join('/a/b/c/d', '../../', 'g.js'))
console.log(path.resolve('/a/b/c/d', '/e/', './index.js'))



console.log('end')