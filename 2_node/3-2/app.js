const express = require('express')
const ip = require('ip')
const Mock = require('mockjs')

// 引入插件
const multer  = require('multer')

// 配置文件储存的位置
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // 上传的文件储存到哪个文件夹
    cb(null, './files')
  },
  filename: function (req, file, cb) {
    const hash = Math.random().toString().slice(2)
    // 上传的文件储存的文件名称
    cb(null, hash + '_' +  Buffer.from(file.originalname, "latin1").toString("utf8"))
  }
})
// 给插件添加配置
const upload = multer({ storage: storage })


const app = express()


app.use(express.static('./dist'))
app.use(express.static('./files'))
app.use(express.json())
app.use(express.urlencoded())

app.post('/api/upload', upload.single('pic'), (req, res) => {
  // console.log(111, req.file)
  res.send({
    code: 0,
    msg: '成功',
    url: `http://${ip.address()}:9000/${req.file.filename}`
  })
})





const data = Mock.mock({
  "list|100": [{
    'index|+1': 0,
    name: '@cname',
    "age|20-40": 20,
    email: '@email',
    text: '@string(100)',
    date: '@date',
    image: '@image(100x100, @color, @name)',
    address: '@county(true)'
  }]
})

const arr = [1,2,3,4,5,6,7]
app.get('/api/list', (req, res) => {
  res.send({
    code: 0,
    msg: '成功',
    values: data.list
  })
})

app.post('/api/add', (req, res) => {
  console.log('req.body', req.body)
  arr.push(req.body)

  res.send({
    code: 0,
    msg: '成功',
    values: arr
  })
})


// 0-65535
app.listen(9000, () => {
  console.log(`运行成功 http://localhost:9000`)
  console.log(`运行成功 http://127.0.0.1:9000`)
  console.log(`运行成功 http://${ip.address()}:9000`)
})


