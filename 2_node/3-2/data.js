const Mock = require('mockjs')

const data = Mock.mock({
  "list|100": [{
    name: '@cname',
    "age|20-40": 20,
    email: '@email',
    text: '@string(100)',
    date: '@date',
    image: '@image(100x100, @color, @name)',
    address: '@county(true)'
  }]
})
console.log(data)