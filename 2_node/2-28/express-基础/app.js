const express = require('express')
const fs = require('fs')
const path = require('path')


// 创建服务器应用
const app = express()

// 处理静态资源
app.use(express.static('./dist'))
// 让post接口可以接受json类型参数
app.use(express.json())
// 让post接口可以接受 form-urlencoded 类型参数
app.use(express.urlencoded())


// 定义 get 接口
app.get('/api/list', (req, res) => {
  console.log('调用了 /api/list')
  console.log('前端传的参数', req.query)
  const { page, pagesize } = req.query
  const data = JSON.parse(fs.readFileSync(path.join(__dirname, 'data.json')))

  res.send({
    code: 0,
    msg: '成功',
    value: data.slice(page * pagesize - pagesize, page * pagesize)
  })
})


app.post('/api/create', (req, res) => {
  console.log('post 参数', req.body)

  res.send({
    code: 0,
    msg: '成功',
    value: [1,2,3,4,5,6,7]
  })
})



const PORT = 3000
app.listen(PORT, () => {
  console.log(`running http://localhost:${PORT}`)
  console.log(`running http://127.0.0.1:${PORT}`)
  console.log(`running http://10.37.19.14:${PORT}`)
})
