const express = require('express')
const fs = require('fs')
const path = require('path')
const ip = require('ip')
const md5 = require('md5')

// 创建服务器应用
const app = express()
// 处理静态资源
app.use(express.static('./dist'))
// 让post接口可以接受json格式的参数
app.use(express.json())

// 登录
app.post('/api/login', (req, res) => {
  console.log('前端参数', req.body)
  // 读取用户列表
  const userlist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/userlist.json'), 'utf-8'))
  // 根据前端参数查询用户是否存在
  const { username, password } = req.body
  const user = userlist.find(item => item.username === username && item.password === password)
  // 判断是否有此用户
  if (user) {
    // 给浏览器设置cookie 记录登录标识
    // res.setHeader('Set-Cookie', `blogToken=${user.token}; httpOnly=true`);
    // 直接把token通过接口返回给前端
    res.send({
      code: 0,
      msg: '成功',
      token: user.token
    })
  } else {
    res.send({
      code: -1,
      msg: '用户名或者密码错误'
    })
  }
})
// 注册
app.post('/api/register', (req, res) => {
  console.log('register', req.body)
  // 读取用户列表
  const userlist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/userlist.json'), 'utf-8'))
  // 根据前端参数查询用户是否存在
  const { username, password } = req.body
  const user = userlist.find(item => item.username === username)
  // 判断是否有此用户
  if (user) {
    res.send({
      code: -1,
      msg: '该用户名已被占用'
    })
  } else {
    // 写入数据库
    userlist.push({
      token: md5(Date.now()),
      username,
      password
    })
    fs.writeFileSync(path.join(__dirname, './data/userlist.json'), JSON.stringify(userlist))
    res.send({
      code: 0,
      msg: '成功'
    })
  }
})

// 统一校验登录
app.use((req, res, next) => {
  const userlist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/userlist.json'), 'utf-8'))
  // 判断用户的token是否失效
  const user = userlist.find(v => v.token === req.headers.authorization)
  if (!user) {
    res.send({
      code: -2,
      msg: '用户信息失效，请重新登录'
    })
    // res.statusCode = 401
    // res.send('用户信息失效，请重新登录')
    return
  } else {
    next()
  }
})

// 博客列表
app.get('/api/list', (req, res) => {
  const bloglist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/bloglist.json'), 'utf-8'))
  res.send({
    code: 0,
    msg: '成功',
    values: bloglist
  })
})

// 博客详情
app.get('/api/blog/detail', (req, res) => {
  const { id } = req.query
  // 根据id去博客列表中查找详情
  const bloglist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/bloglist.json'), 'utf-8'))
  const blog = bloglist.find(item => item.id === id)
  if (blog) {
    res.send({
      code: 0,
      msg: '成功',
      values: blog
    })
  } else {
    res.send({
      code: -1,
      msg: '参数错误'
    })
  }
})

// 新增博客
app.post('/api/blog/create', (req, res) => {
  // 获取前端传过来的数据，存博客
  const { title, content } = req.body
  const bloglist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/bloglist.json'), 'utf-8'))
  bloglist.unshift({
    id: md5(Date.now() + user.username),
    title,
    content,
    createTime: Date.now(),
    user: {
      username: user.username
    }
  })
  fs.writeFileSync(path.join(__dirname, './data/bloglist.json'), JSON.stringify(bloglist))

  res.send({
    code: 0,
    msg: '成功'
  })
})

// 删除博客
app.delete('/api/blog/del', (req, res) => {
  const { id } = req.query
  // 根据id去博客列表中查找下标
  const bloglist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/bloglist.json'), 'utf-8'))
  const index = bloglist.findIndex(item => item.id === id)
  if (index > -1) {
    // 根据下标删除数据, 更新数据
    bloglist.splice(index, 1)
    fs.writeFileSync(path.join(__dirname, './data/bloglist.json'), JSON.stringify(bloglist))
    res.send({
      code: 0,
      msg: '删除成功'
    })
  } else {
    res.send({
      code: -1,
      msg: '参数错误，文章不存在'
    })
  }
})
// 编辑博客
app.post('/api/blog/update', (req, res) => {
  const { id, title, content } = req.body
  // 根据id去博客列表中查找下标
  const bloglist = JSON.parse(fs.readFileSync(path.join(__dirname, './data/bloglist.json'), 'utf-8'))
  const index = bloglist.findIndex(item => item.id === id)
  const blog = bloglist[index]

  if (index > -1) {
    // 根据下标 更新数据
    bloglist.splice(index, 1, { ...blog, title, content })


    fs.writeFileSync(path.join(__dirname, './data/bloglist.json'), JSON.stringify(bloglist))
    res.send({
      code: 0,
      msg: '修改成功'
    })
  } else {
    res.send({
      code: -1,
      msg: '参数错误，文章不存在'
    })
  }
})

const PORT = 3000
app.listen(PORT, () => {
  console.log(`running http://localhost:${PORT}`)
  console.log(`running http://127.0.0.1:${PORT}`)
  console.log(`running http://${ip.address()}:${PORT}`)
})