

$('.tab').addEventListener('click', e => {
  const target = e.target
  if (target.nodeName === 'P') {
    // 添加高亮
    $('.tab .active').classList.remove('active')
    target.classList.add('active')
    // 切换登录和注册
    const type = target.getAttribute('data-type')
    if (type === 'login') {
      $('.login-form').classList.add('show')
      $('.register-form').classList.remove('show')
    } else {
      $('.login-form').classList.remove('show')
      $('.register-form').classList.add('show')
    }
  }
})

// 登录
$('.login-form button').addEventListener('click', async () => {
  const username = $('.login-form .username').value.trim()
  const password = $('.login-form .password').value.trim()
  if (!username) {
    return alert('请输入用户名')
  }
  if (!password) {
    return alert('请输入密码')
  }
  const res = await axios.post('/api/login', { username, password })
  console.log(res.data)
  if (res.data.code === 0) {
    // 存token
    localStorage.setItem('token', res.data.token)
    alert('登录成功')
    location.href = './index.html'
  } else {
    alert(res.data.msg)
  }
})


// 注册
$('.register-form button').addEventListener('click', async () => {
  const username = $('.register-form .username').value.trim()
  const password = $('.register-form .password').value.trim()
  const okPassword = $('.register-form .ok-password').value.trim()
  if (!username) {
    return alert('请输入用户名')
  }
  if (!password) {
    return alert('请输入密码')
  }
  if (password !== okPassword) {
    return alert('两次输入的密码不一致')
  }
  const res = await axios.post('/api/register', { username, password })
  console.log(res.data)
  if (res.data.code === 0) {
    alert('注册成功')
  } else {
    alert(res.data.msg)
  }
})













