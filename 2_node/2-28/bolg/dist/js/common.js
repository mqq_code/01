const $ = el => document.querySelector(el)
const $All = el => [...document.querySelectorAll(el)]

const query = () => {
  if (location.search === '') return {}
  const arr = location.search.slice(1).split('&')
  const queryObj = {}
  arr.forEach(item => {
    const [key, val] = item.split('=')
    queryObj[key] = decodeURIComponent(val)
  })
  return queryObj
}
window.queryObj = query()



function isAuth() {
  const token = localStorage.getItem('token')
  if (!token && location.pathname !== '/login.html') {
    alert('用户登录信息失效，请重新登录！')
    location.href = './login.html'
  } 
}
isAuth()