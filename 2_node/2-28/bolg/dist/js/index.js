async function init() {
  const res = await axios.get('/api/list', {
    headers: {
      // 把 token 通过请求头传给后端
      Authorization: localStorage.getItem('token')
    }
  })
  
  if (res.data.code === 0) {
    if (res.data.values.length === 0) {
      $('.empty').style.display = 'block'
      $('.list').innerHTML = ''
    } else {
      // 渲染博客列表
      $('.list').innerHTML = res.data.values.map(item => {
        return `
          <div class="list-item">
            <h2>${item.title}</h2>
            <p>${item.user.username} ${new Date(item.createTime).toLocaleString()}</p>
            <div class="list-action">
              <button>编辑</button>
              <button class="del">删除</button>
            </div>
          </div>
        `
      }).join('')
    }
  } else if (res.data.code === -2) {
    alert('登录失效，请重新登录')
    localStorage.removeItem('token')
    location.href = './login.html'
  } else {
    alert(res.data.msg)
  }

  $All('.list-item').forEach((item, index) => {
    item.addEventListener('click', (e) => {
      // console.log(res.data.values[index])
      if (e.target.nodeName === 'BUTTON') {
        if (e.target.classList.contains('del')) {
          // 点击删除按钮，调用删除接口
          del(res.data.values[index].id)
        } else {
          console.log('编辑')
          // 跳转到编辑页面
          location.href = `./edit.html?id=${res.data.values[index].id}`
        }
      } else {
        const url = encodeURIComponent('https://www.baidu.com?keyword=js&a=100')
        const text = encodeURIComponent('我是中文')
        // 跳转详情页，把id传到详情页
        location.href = `./detail.html?id=${res.data.values[index].id}&text=${text}&url=${url}`
      }
    })
  })
}

init()

async function del(id) {
  const res = await axios.delete('/api/blog/del', { params: { id } })
  console.log(res.data)
  if (res.data.code === 0) {
    // 删除成功更新数据
    alert('删除成功')
    init()
  } else {
    alert(res.data.msg)
  }
}



$('footer button').addEventListener('click', () => {
  location.href = './edit.html'
})