const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

// 抛出配置项
module.exports = {
  // 打包模式
  // 'mode': 'development' or 'production' 
  mode: 'production',
  // 入口文件
  entry: './src/index.js',
  // 输出目录和文件
  output: {
    // 输出目录，必须是绝对路径
    path: path.join(__dirname, 'build'),
    // 文件名
    filename: 'js/index_[hash:8].js',
    // 打包时清空build文件夹
    clean: true,
    // 通过 asset 处理的文件输出目录
    assetModuleFilename: "assets/[name]_[hash:8].[ext]"
  },
  // 插件
  plugins: [
    // 打包时自动创建 html 文件到输出的目录，自动引入构建完成的 js
    new HtmlWebpackPlugin({
      template: './index.html'
    }),
    // 把 js 中的 css 抽离到单独的 css 文件
    new MiniCssExtractPlugin({
      filename: 'css/style.css'
    })
  ],
  // loader作用: 加载器，让 js 可以引入其他类型的文件，例如: ts、tsx、less、scss、css、img...
  // 配置 loader
  module: {
    rules: [
      {
        // 匹配文件类型
        test: /\.(css|scss)$/i,
        // 使用指定的解析器
        // npm i -D style-loader css-loader sass-loader sass
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(je?pg|png|svg|gif|webp)$/i,
        // webpack5之前使用 file-loader、url-loader
        // use: ['file-loader']
        type: 'asset',
        parser: {
          // 配置 13kb 以内的图片转换成 base64格式
          dataUrlCondition: {
            maxSize: 13 * 1024 // 13kb
          }
        }
      },
      {
        // 解析html文件中的图片
        test: /\.html$/i,
        use: 'html-loader'
      },
      {
        test: /\.js$/i,
        // 使用 babel 转译 js 的时候跳过 node_modules 文件夹
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  // 配置开发服务器, 需要下载 webpack-dev-server，在 package.json 中添加 命令 "dev": "webpack serve"
  devServer: {
    port: 3000
  }
}
