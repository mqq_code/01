

console.log('这是format.js')

const format = (time) => {
  const h = Math.floor(time / 1000 / 60 / 60)
  const m = Math.floor(time / 1000 / 60 % 60)
  return `${h}:${m}`
}

// 单独抛出，可以抛出多个变量，export 后必须是定义变量的写法
export const a = 100
export let b = [1,2,3,4,5]
export function fn() {
  console.log('我是函数')
}


// 默认抛出，每个文件只能有一个默认抛出，export default 后可以是任意类型的值
export default format