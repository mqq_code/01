
// 引入文件: import '文件路径'
// import './utils/format'

// 使用默认抛出的变量，变量名称可以是任意名字
// import 变量名 from './utils/format'

// 使用单独抛出的变量，变量名必须和抛出的名字一致
// import { a, b, fn } from './utils/format'

// 引入变量时可以使用 as 修改变量名
// import format, { a as a1, b as bb, fn as cc } from './utils/format'

// 把文件中的所有变量都放到 obj 这个对象内
import * as obj from './utils/format'

console.log(obj)

console.log('这是index.js')
// console.log(format)
// console.log(a1, bb, cc)

// const a = 'AAAAA'

// const sum = (a, b) => {
//   return a + b
// }

// console.log(sum(1, 2))




// function Person(name, age) {
//   this.name = name
//   this.age = age
// }
// Person.prototype.say = function() {
//   console.log(this.name)
// }
// const xm = new Person('小明', 22)

// setTimeout(() => {
//   xm.say()
//   const xh = new Person('小明', 22)
//   console.log(xh)
// }, Math.random() * 10000)