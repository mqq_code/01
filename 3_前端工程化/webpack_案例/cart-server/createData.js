const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

const data = Mock.mock({
  "list|20": [
    {
      "id": "@id",
      "name": "@cname",
      "price|5-50": 5,
      "count": 0,
      "desc": "@cword(200)",
      "image": "@image(100x100, @color, @title)"
    }
  ]
})

fs.writeFileSync(path.join(__dirname, 'data/goods.json'), JSON.stringify(data.list))