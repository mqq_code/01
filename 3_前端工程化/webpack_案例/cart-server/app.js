const express = require('express')
const ip = require('ip')
const fs = require('fs')
const path = require('path')

const readData = (file) => {
  return JSON.parse(fs.readFileSync(path.join(__dirname, './data', file), 'utf-8'))
}
const writeData = (file, data) => {
  return fs.writeFileSync(path.join(__dirname, './data', file), JSON.stringify(data))
}

const app = express()
app.use(express.json())

app.get('/api/list', (req, res) => {
  // res.setHeader('Access-Control-Allow-Origin', '*')
  const goods = readData('goods.json')
  console.log('请求来源的地址', req.headers.host)
  res.send({
    code: 0,
    msg: '成功',
    values: goods
  })
})

// 添加购物车接口
app.post('/api/cart/add', (req, res) => {
  const goodslist = readData('goods.json')
  const cartlist = readData('cart.json')
  // 接收前端数据
  const { id } = req.body
  // 根据 id 去购物车查找数据
  const index = cartlist.findIndex(v => v.id === id)
  if (index > -1) {
    // 购物车存在此商品，直接加1
    cartlist[index].count ++
    writeData('cart.json', cartlist)
    res.send({
      code: 0,
      msg: '成功'
    })
  } else {
    // 购物车不存在，去所有商品列表中查找
    const goods = goodslist.find(v => v.id === id)
    if (goods) {
      goods.count++
      cartlist.push(goods)
      writeData('cart.json', cartlist)
      res.send({
        code: 0,
        msg: '成功'
      })
    } else {
      res.send({
        code: -1,
        msg: '商品不存在'
      })
    }
  }
})

// 购物车列表
app.get('/api/cart/list', (req, res) => {
  res.send({
    code: 0,
    msg: '成功',
    values: readData('cart.json')
  })
})

// 详情接口
app.get('/api/goods/detail', (req, res) => {
  const { id } = req.query
  const goodslist = readData('goods.json')
  const goods = goodslist.find(v => v.id === id)
  if (goods) {
    res.send({
      code: 0,
      msg: '成功',
      values: goods
    })
  } else {
    res.send({
      code: -1,
      msg: '商品不存在'
    })
  }
})

// 商品加减
app.post('/api/cart/count', (req, res) => {
  const cartlist = readData('cart.json')
  const { id, num } = req.body
  const index = cartlist.findIndex(v => v.id === id)
  if (index > -1) {
    cartlist[index].count += num
    if (cartlist[index].count === 0) {
      cartlist.splice(index, 1)
    }
    writeData('cart.json', cartlist)
    res.send({
      code: 0,
      msg: '成功'
    })
  } else {
    res.send({
      code: -1,
      msg: '商品不存在'
    })
  }
})


const PORT = 9000
app.listen(PORT, () => {
  console.log(`running http://localhost:${PORT}`)
  console.log(`running http://127.0.0.1:${PORT}`)
  console.log(`running http://${ip.address()}:${PORT}`)
})