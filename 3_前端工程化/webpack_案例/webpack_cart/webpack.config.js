const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: 'development',
  // 生成打包前和打包后代码的对应关系
  devtool: 'source-map',
  // 配置多入口文件，多出口文件
  entry: {
    home: './src/home/index.js',
    detail: './src/detail/detail.js',
    cart: './src/cart/cart.js'
  },
  output: {
    // 打包时根据 entry 配置的属性名打包对应的js文件
    filename: 'js/[name].js',
    path: path.join(__dirname, 'build'),
    clean: true,
    assetModuleFilename: 'images/[name]_[hash:8][ext]'
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|je?pg|gif|webp|svg)$/i,
        type: 'asset',
        parser: {
          // 配置小于15kb的图片转换成base64格式
          dataUrlCondition: {
            maxSize: 15 * 1024
          }
        }
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      },
      {
        test: /\.js$/i,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      // 引入打包后的 js 模块
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      template: './src/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new HtmlWebpackPlugin({
      template: './src/cart.html',
      filename: 'cart.html',
      chunks: ['cart']
    }),
    new MiniCssExtractPlugin({
      // [name]: 使用对应的模块名生成 css 文件名
      filename: 'css/[name].css'
    })
  ],
  devServer: {
    // 配置开发服务器
    proxy: [
      {
        // 请求 `/bw/api/list` 转发到 `http://10.37.19.16:9000/bw/api/list`
        // context: ['/bw'],
        // target: 'http://10.37.19.16:9000',

        // 请求 `/bw/api/list` 转发到 `http://10.37.19.16:9000/api/list`
        context: ['/bw'],
        // 代理地址
        target: 'http://10.37.19.16:9000',
        pathRewrite: { '^/bw': '' },
        // 修改请求来源
        changeOrigin: true
      }
    ]
  }
}