import '../common/common.scss'
import './cart.scss'
import axios from 'axios'
import { $, $All } from '../common/utils'

function getCartList() {
  axios.get('/bw/api/cart/list').then(res => {
    $('.list').innerHTML = res.data.values.map(item => {
      return `
        <div class="goods">
          <h3>${item.name}</h3>
          <h3>¥${item.price}</h3>
          <div class="count">
            <button data-id="${item.id}" data-num="-1">-</button>
            ${item.count}
            <button data-id="${item.id}" data-num="1">+</button>
          </div>
        </div>
      `
    }).join('')
    $('.total span').innerHTML = res.data.values.reduce((prev, val) => {
      return prev + val.count * val.price
    }, 0)
  })
}
getCartList()

$('.list').addEventListener('click', async e => {
  if (e.target.nodeName === 'BUTTON') {
    const id = e.target.getAttribute('data-id')
    const num = Number(e.target.getAttribute('data-num'))
    const res = await axios.post('/bw/api/cart/count', { id, num })
    if (res.data.code === 0) {
      getCartList()
    } else {
      alert(res.data.msg)
    }
  }
})