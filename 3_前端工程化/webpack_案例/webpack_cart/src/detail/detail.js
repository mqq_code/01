import '../common/common.scss'
import { $, $All, query } from '../common/utils'
import axios from 'axios'

const queryObj = query()

// 调用接口，把id传给详情接口
axios.get('/bw/api/goods/detail', {
  params: {
    id: queryObj.id
  }
}).then(res => {
  if (res.data.code === 0) {
    $('main').innerHTML = `
      <h2>${res.data.values.name}</h2>
      <h2>价格：¥${res.data.values.price}</h2>
      <p>商品介绍：${res.data.values.desc}</p>
      <img src="${res.data.values.image}" />
    `
  } else {
    alert(res.data.msg)
  }
})


// 添加到购物车
async function addCart(id) {
  const res = await axios.post('/bw/api/cart/add', { id })
  if (res.data.code === 0) {
    alert('添加成功')
  } else {
    alert(res.data.msg)
  }
}

$('.add').addEventListener('click', () => {
  addCart(queryObj.id)
})