import '../common/common.scss'
import './home.scss'
import axios from 'axios'
import { $, $All } from '../common/utils'

// 请求商品数据
axios.get('/bw/api/list').then(res => {
  // 渲染列表
  $('.app main').innerHTML = res.data.values.map(item => {
    return `
      <div class="goods" data-id="${item.id}">
        <img src="${item.image}" />
        <h3>${item.name}</h3>
        <h3>¥${item.price}</h3>
        <button>+</button>
      </div>
    `
  }).join('')
  // 点击商品
  $All('.goods').forEach(goods => {
    goods.addEventListener('click', (e) => {
      const id = goods.getAttribute('data-id')
      if (e.target.nodeName === 'BUTTON') {
        // 调用添加到购物车的接口
        addCart(id)
      } else {
        // 跳转到详情页面
        location.href = `./detail.html?id=${id}`
      }
    })
  })
})
// 添加到购物车
async function addCart(id) {
  const res = await axios.post('/bw/api/cart/add', { id })
  if (res.data.code === 0) {
    alert('添加成功')
  } else {
    alert(res.data.msg)
  }
}