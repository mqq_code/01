import { defineConfig } from 'vite'
import { join } from 'path'

// 定义配置规则
export default defineConfig({
  resolve: {
    // 配置路径别名
    alias: {
      '@': join(__dirname, './src'),
      'Utils': join(__dirname, './src/utils'),
    },
    // 配置允许忽略的文件后缀名
    extensions: ['.js', '.css', '.jpg']
  },
  // 构建配置
  build: {
    // 输出目录
    outDir: join(__dirname, 'build'),
    // 资源输出目录
    assetsDir: 'assets',
    // 小于此数值的资源转成 base64 形式
    assetsInlineLimit: 12 * 1024,
    // 生成源代码和打包后代码的映射文件
    sourcemap: true,
    // rollup 配置项
    rollupOptions: {
      // 配置入口文件
      input: {
        index: 'index.html',
        player: 'player.html'
      },
    }
  }
})