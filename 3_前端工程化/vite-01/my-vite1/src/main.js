import './scss/style.scss'


// js 计算 rem
function rem() {
  // 标准屏幕下 750px: 1rem === 10px
  // 放大比例 = 当前屏幕宽度 / 750 
  // 1rem === 10px * 放大比例
  document.documentElement.style.fontSize = document.documentElement.clientWidth / 750 * 10 + 'px'
}
// rem()


const btn = document.querySelector('.btn')
const box1 = document.querySelector('.box1')

btn.addEventListener('click', () => {
  box1.style.transform = `translateX(200px)`
})

box1.addEventListener('transitionend', () => {
  console.log('动画结束')
  box1.style.transform = `translateX(200px) translateY(200px)`
})
box1.addEventListener('transitionstart', () => {
  console.log('动画开始')
  box1.style.transform = `translateX(200px) translateY(200px)`
})
box1.addEventListener('animationstart', () => {
  console.log('动画开始')
})
box1.addEventListener('animationend', () => {
  console.log('动画结束')
})