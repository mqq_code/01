export const createImg = url => {
  const img = new Image()
  img.src = url
  img.onload = () => {
    document.body.appendChild(img)
  }
}
export const $ = el => document.querySelector(el)