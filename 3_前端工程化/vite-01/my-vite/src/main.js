import './font/iconfont.css'
import './scss/style.scss'
import javascriptLogo from './img/javascript.svg'
import img0 from './img/img0'
import img3 from './img/img3.jpg'
// import viteLogo from '/vite.svg'
import { createImg } from './utils/utils'
import './test/components/aaa/test'
import axios from 'axios'

console.log('main.js')

createImg(javascriptLogo)
// createImg(viteLogo)
createImg(img0)
createImg(img3)


axios.get('/bw/music_api/banner').then(res => {
  console.log(res.data)
})

console.log(`%c 环境变量: ${process.env.NODE_ENV}`, 'font-size: 20px;color:red;')
console.log(`%c 环境变量:`, 'font-size: 20px;color:red;', import.meta.env)
