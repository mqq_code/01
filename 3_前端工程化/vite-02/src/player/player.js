import './player.scss'
import { query, $, $All } from 'Utils'
import { songDetail, songUrl, lyric } from '@/api'

const qs = query(location.href)

console.log('player 页面', qs.id)

// 获取详情
songDetail(qs.id).then(res => {
  console.log(res.data.songs[0])
  $('h2').innerHTML = res.data.songs[0].name
  $('h3').innerHTML = '歌手：' + res.data.songs[0].ar.map(v => v.name).join('/')
  $('img').src = res.data.songs[0].al.picUrl
})

// 获取播放地址
songUrl(qs.id).then(res => {
  console.log(res.data.data[0].url)
  $('audio').src = res.data.data[0].url
})

$('button').addEventListener('click', () => {
  if ($('audio').paused) {
    $('audio').play()
    $('button').innerHTML = '暂停'
  } else {
    $('audio').pause()
    $('button').innerHTML = '播放'
  }
})

let lyricArr = []
// 获取歌词
lyric(qs.id).then(res => {
  lyricArr = res.data.lrc.lyric.split('\n').map(item => {
    const [a, b] = item.split(']')
    const [m, s] = a.slice(1).split(':')
    return [m * 60 + Number(s), b]
  }).filter(v => v[1])
  console.log(lyricArr)
  $('ul').innerHTML = lyricArr.map(item => {
    return `<li>${item[1]}</li>`
  }).join('')
})

$('audio').addEventListener('timeupdate', () => {
  const currentTime = $('audio').currentTime
  const index = lyricArr.findIndex((item, i) => {
    if (i === lyricArr.length - 1) return i
    return currentTime >= item[0] && currentTime < lyricArr[i + 1][0]
  })
  $('.active')?.classList.remove('active')
  $All('.lyric li')[index]?.classList.add('active')
  $('ul').scrollTop = index * 40 - 120
})


