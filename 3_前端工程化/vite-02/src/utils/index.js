export const $ = el => document.querySelector(el)
export const $All = el => [...document.querySelectorAll(el)]

export const query = (url) => {
  const obj = {}
  const arr = url.split('?')[1].split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })
  return obj
}
