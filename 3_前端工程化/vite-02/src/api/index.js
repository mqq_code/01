import axios from 'axios'

axios.defaults.baseURL = 'https://zyxcl.xyz/music_api'

// 热搜
export const searchHot = () => {
  return axios.get('/search/hot')
}
// 搜索建议
export const suggest = (keywords) => {
  return axios.get('/search/suggest', {
    params: {
      keywords,
      type: 'mobile'
    }
  })
}
// 搜索
export const search = (keywords, offset = 0) => {
  return axios.get('/search', {
    params: {
      keywords,
      offset
    }
  })
}

// 歌曲详情
export const songDetail = ids => {
  return axios.get('/song/detail', {
    params: {
      ids
    }
  })
}
// 播放地址
export const songUrl = id => {
  return axios.get('/song/url', {
    params: {
      id
    }
  })
}
// 歌词
export const lyric = id => {
  return axios.get('/lyric', {
    params: {
      id
    }
  })
}