import '../scss/common.scss'
import './index.scss'
import { $, $All } from 'Utils'
import { searchHot, suggest, search } from '@/api'

// 搜索的关键字
let keyword = ''
// 第几页
let offset = 0
// 是否可以上拉加载下一页
let hasMore = true

// 搜索歌曲
async function startSearch() {
  // 展示搜索结果页面，存搜索历史
  $('.search-input').value = keyword
  $('.search-content.show').classList.remove('show')
  $('.result').classList.add('show')
  // 搜索历史
  setHistory(keyword)
  // 调用搜索接口，渲染搜索结果
  const res = await search(keyword, offset)
  hasMore = res.data.result.hasMore
  $('.result ul').innerHTML += res.data.result.songs.map((item, index) => {
    return `
      <li data-id="${item.id}">
        <h3>${index + offset}. ${item.name}</h3>
        <p>
          <span>
            ${item.artists.map(v => v.name).join('/')}
          </span>
          -
          <span>${item.album.name}</span>
        </p>
        <svg t="1709863280591" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4347" width="22" height="22">
          <path d="M512 0C230.4 0 0 230.4 0 512s230.4 512 512 512 512-230.4 512-512S793.6 0 512 0z m0 981.333333C253.866667 981.333333 42.666667 770.133333 42.666667 512S253.866667 42.666667 512 42.666667s469.333333 211.2 469.333333 469.333333-211.2 469.333333-469.333333 469.333333z" fill="#666666" p-id="4348"></path>
          <path d="M672 441.6l-170.666667-113.066667c-57.6-38.4-106.666667-12.8-106.666666 57.6v256c0 70.4 46.933333 96 106.666666 57.6l170.666667-113.066666c57.6-42.666667 57.6-106.666667 0-145.066667z" fill="#666666" p-id="4349"></path>
        </svg>
      </li>
    `
  }).join('')
}

// 渲染搜索建议
async function renderSuggest(keyword) {
  const res = await suggest(keyword)
  if (res.data.result.allMatch) {
    $('.suggest ul').innerHTML = res.data.result.allMatch.map(item => {
      return `<li data-keyword="${item.keyword}">${item.keyword}</li>`
    }).join('')
  } else {
    $('.suggest ul').innerHTML = ''
  }
}

// 存搜索历史
function setHistory(keyword) {
  const historylist = JSON.parse(localStorage.getItem('historylist')) || []
  const index = historylist.indexOf(keyword)
  if (index > -1) {
    historylist.splice(index, 1)
  }
  historylist.unshift(keyword)
  localStorage.setItem('historylist', JSON.stringify(historylist))
  renderHistory()
}
// 删除历史
function removeHistory(keyword) {
  const historylist = JSON.parse(localStorage.getItem('historylist')) || []
  const index = historylist.indexOf(keyword)
  if (index > -1) {
    historylist.splice(index, 1)
    localStorage.setItem('historylist', JSON.stringify(historylist))
    renderHistory()
  }
}

// 渲染搜索历史
function renderHistory() {
  const historylist = JSON.parse(localStorage.getItem('historylist')) || []
  $('.history').innerHTML = historylist.map(item => {
    return `<p data-keyword="${item}">${item}<i></i></p>`
  }).join('')
}

renderHistory()

// 选项卡
$All('nav p').forEach((item, index) => {
  item.addEventListener('click', () => {
    // 点击标题添加高亮
    $('nav .active').classList.remove('active')
    item.classList.add('active')
    // 根据点击的下标展示对应的内容
    $('.content.show').classList.remove('show')
    $All('.content')[index].classList.add('show')
  })
})

// 热门搜索
searchHot().then(res => {
  $('.search-hot ul').innerHTML = res.data.result.hots.map(item => {
    return `<li data-keywords="${item.first}">${item.first}</li>`
  }).join('')
})
// 点击热门搜索按钮
$('.search-hot ul').addEventListener('click', e => {
  if (e.target.nodeName === 'LI') {
    // 获取关键字开始搜索
    keyword = e.target.getAttribute('data-keywords')
    startSearch()
  }
})
// 点击清空input, 展示默认搜索页面
$('.clear').addEventListener('click', () => {
  $('.search-input').value = ''
  $('.search-content.show').classList.remove('show')
  $('.default').classList.add('show')
  $('.result ul').innerHTML = ''
  $('.suggest ul').innerHTML = ''
  hasMore = true
  offset = 0
})

let timer = null
// 搜索建议
$('.search-input').addEventListener('input', e => {
  // 展示搜索建议页面
  $('.search-content.show').classList.remove('show')
  $('.suggest').classList.add('show')
  $('.suggest h3 span').innerHTML = e.target.value
  // 搜索内容为空时回到默认页面
  if (e.target.value.length === 0) {
    $('.search-content.show').classList.remove('show')
    $('.default').classList.add('show')
    clearInterval(timer)
    $('.suggest ul').innerHTML = ''
    return 
  }
  // 调用搜索建议接口，渲染列表，添加防抖，连续多次调用同一个函数只执行最后一次
  if (timer) clearInterval(timer)
  timer = setTimeout(() => {
    renderSuggest(e.target.value)
    timer = null
  }, 1000)
})

// 搜索建议绑定事件
$('.suggest ul').addEventListener('click', async e => {
  // 展示搜索结果页面
  keyword = e.target.getAttribute('data-keyword')
  startSearch()
})

// 点击搜索历史
$('.history').addEventListener('click', e => {
  if (e.target.nodeName === 'P') {
    // 获取关键字开始搜索
    keyword = e.target.getAttribute('data-keyword')
    startSearch()
  } else if (e.target.nodeName === 'I') {
    // 删除历史
    const keyword = e.target.parentNode.getAttribute('data-keyword')
    removeHistory(keyword)
  }
})

// 绑定滚动事件
$('.search-page').addEventListener('scroll', e => {
  if ($('.result.show') && hasMore) {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if (scrollTop + clientHeight >= scrollHeight) {
      offset += 30
      startSearch()
    }
  }
})

// 点击搜索结果
$('.result ul').addEventListener('click', e => {
  if (e.target.nodeName === 'LI') {
    const id = e.target.getAttribute('data-id')
    location.href = `./player.html?id=${id}`
  }
})