import { defineConfig } from 'vite'
import { join } from 'path'

// 定义配置规则
export default defineConfig({
  resolve: {
    // 配置路径别名
    alias: {
      '@': join(__dirname, './src'),
      'Utils': join(__dirname, './src/utils'),
    },
    // 配置允许忽略的文件后缀名
    extensions: ['.js', '.css', '.jpg']
  },
  // 构建配置
  build: {
    // 输出目录
    outDir: join(__dirname, 'build'),
    // 资源输出目录
    assetsDir: 'assets',
    // 小于此数值的资源转成 base64 形式
    assetsInlineLimit: 12 * 1024,
    // 生成源代码和打包后代码的映射文件
    sourcemap: true,
    // rollup 配置项
    rollupOptions: {
      // 配置入口文件
      input: {
        index: join(__dirname, 'index.html'),
        player: join(__dirname, 'player.html')
      },
    }
  },
  // 本地服务配置
  server: {
    proxy: {
      // /bba/api/list  转发到 http://10.37.19.16:9000/bba/api/list
      // '/bba': {
      //   target: 'http://10.37.19.16:9000'
      // }
      // /bba/api/list  转发到 http://10.37.19.16:9000/api/list
      // '/bba': {
      //   target: 'http://10.37.19.16:9000',
      //   rewrite: path => path.replace(/\/bba/, '')
      // }
      // /api/list 转发到 http://10.37.19.16:9000/api/list
      // '/api': {
      //   target: 'http://10.37.19.16:9000'
      // }
    }
  }
})