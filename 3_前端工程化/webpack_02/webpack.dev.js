const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: 'development',
  // 生成打包前和打包后代码的对应关系
  devtool: 'source-map',
  // 配置多入口文件，单出口文件
  // entry: ['./src/index.js', './src/test.js'],
  // output: {
  //   filename: 'js/index.js',
  //   path: path.join(__dirname, 'build'),
  //   clean: true,
  //   assetModuleFilename: 'images/[name]_[hash:8][ext]'
  // },
  // 配置多入口文件，多出口文件
  entry: {
    index: './src/index.js',
    detail: './src/detail.js'
  },
  output: {
    // 打包时根据 entry 配置的属性名打包对应的js文件
    filename: 'js/[name].js',
    path: path.join(__dirname, 'build'),
    clean: true,
    assetModuleFilename: 'images/[name]_[hash:8][ext]'
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|je?pg|gif|webp|svg)$/i,
        type: 'asset',
        parser: {
          // 配置小于15kb的图片转换成base64格式
          dataUrlCondition: {
            maxSize: 15 * 1024
          }
        }
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      },
      {
        test: /\.js$/i,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      // 引入打包后的 js 模块
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      template: './src/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new MiniCssExtractPlugin({
      // [name]: 使用对应的模块名生成 css 文件名
      filename: 'css/[name].css'
    })
  ],
  devServer: {
    // 配置开发服务器
    proxy: [
      {
        context: ['/bw'],
        target: 'http://10.37.19.16:9000',
        pathRewrite: { '^/bw': '' },
      }
    ]
  },
  resolve: {
    // 配置路径别名
    alias: {
      '@': path.join(__dirname, 'src'),
      utils: path.join(__dirname, 'src/utils')
    },
    // 引入文件时可以省略的后缀名
    extensions: ['.js', '.scss', '.jpg']
  }
}