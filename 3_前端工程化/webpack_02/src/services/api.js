import axios from 'axios'
// 统一管理项目中的所有请求



// 给所有请求的url添加公共前缀
// axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? '/bw' : 'https://zyxcl.xyz'

console.log('环境变量', process.env.NODE_ENV)

// 根据环境变量判断要请求哪个地址
let host = '/bw'
if (process.env.NODE_ENV === 'production') {
  host = 'http://zyxcl.xyz'
}

axios.defaults.baseURL = host


export const getlist = () => {
  return axios.get('/api/list')
}

export const getCartList = () => {
  return axios.get('/api/cart/list')
}

export const addCart = (id) => {
  return axios.post('/api/cart/add', { id })
}



