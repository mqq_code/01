export default {
  // 开启命名空间
  namespaced: true,
  state: () => {
    return {
      title1: '我是storeA',
      count: 0
    }
  },
  mutations: {
    setCount(state, c) {
      console.log('storeA 的 setCount')
      state.count += c
    }
  },
  actions: {},
  getters: {},
}