import Vue from 'vue'
import Vuex from 'vuex'
import storeA from './modules/storeA'
import storeB from './modules/storeB'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    username: '小明',
    age: 20,
    sex: '男',
    email: '123@qq.com',
    list: [],
    cartList: []
  },
  mutations: {
    setUsername(state, name) {
      state.username = name
    },
    setAge(state, age) {
      state.age = age
    },
    setSex(state, sex) {
      state.sex = sex
    },
    setList(state, list) {
      state.list = list
    },
    addCart(state, payload) {
      state.cartList.push(payload)
    },
    setCount() {
      console.log('index 的 setCount')
    }
  },
  actions: {
  },
  // 拆分子模块
  modules: {
    storeA,
    storeB
  }
})
export default store