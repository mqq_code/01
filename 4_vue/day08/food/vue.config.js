const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/exam_api': {
        target: 'https://zyxcl.xyz'
      }
    }
  }
})
