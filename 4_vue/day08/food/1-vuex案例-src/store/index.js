import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// 数据持久化插件
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

const store = new Vuex.Store({
  // 安装插件
  plugins: [createPersistedState()],
  state: {
    list: [],
    selectedList: [], // 选中的食物
  },
  mutations: {
    setList(state, payload) {
      state.list = payload
    },
    // 选中食物
    changeSelected(state, name) {
      // 根据点击的名称查找到食物对象
      state.list.forEach(item => {
        item.list.forEach(food => {
          if (food.name === name) {
            // 修改选中状态
            Vue.set(food, 'checked', !food.checked)
            if (food.checked) {
              state.selectedList.push(food)
            } else {
              const index = state.selectedList.findIndex(v => v.name === food.name)
              state.selectedList.splice(index, 1)
            }
          }
        })
      })
    }
  },
  actions: {
    getDataApi(context) {
      // 调用接口
      axios.get('/exam_api/food').then(res => {
        // 把接口返回值传给 mutations 的函数，修改 state 数据
        context.commit('setList', res.data.value)
      })
    }
  }
})
export default store