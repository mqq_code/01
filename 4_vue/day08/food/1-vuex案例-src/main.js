import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
new Vue({
  router,
  // 把 vuex 实例对象添加 vue 中
  store,
  render: h => h(App)
}).$mount('#app')
