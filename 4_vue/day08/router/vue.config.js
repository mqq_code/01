const { defineConfig } = require('@vue/cli-service')
const zh = require('./data/zh.json')
const xl = require('./data/xl.json')
const sx = require('./data/sx.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {

      // 模拟接口
      devServer.app.get('/api/list/zh', (req, res) => {
        res.send(zh)
      });
      devServer.app.get('/api/list/xl', (req, res) => {
        res.send(xl)
      });
      devServer.app.get('/api/list/sx', (req, res) => {
        res.send(sx)
      });

      return middlewares;
    },
  }
})
