import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    isGrid: false,
    sortType: 0 // 0: 默认排序， 1: 升序，2:降序
  },
  mutations: {
    setGrid(state, payload) {
      state.isGrid = payload
    },
    setSortType(state) {
      if (state.sortType === 2) {
        state.sortType = 0
      } else {
        state.sortType++
      }
    }
  }
})
export default store