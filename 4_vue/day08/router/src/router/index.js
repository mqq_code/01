import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Goods from '../views/home/goods/Goods.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home,
    redirect: '/home/zh',
    children: [
      {
        path: '/home/:id',
        name: 'goods',
        component: Goods
      }
    ]
  },
  {
    path: '/detail',
    name: 'detail',
    component: () => import('../views/detail/Detail.vue')
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('../views/search/Search.vue')
  },
  {
    path: '*',
    redirect: '/home'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
