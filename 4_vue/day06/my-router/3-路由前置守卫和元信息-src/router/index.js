import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import Movie from '../pages/home/movie/Movie.vue'
import Cinema from '../pages/home/cinema/Cinema.vue'
import Mine from '../pages/home/mine/Mine.vue'

import Detail from '../pages/detail/Detail.vue'
import Login from '../pages/login/Login.vue'
import City from '../pages/city/City.vue'
import Notfound from '../pages/Notfound.vue'

// 给 vue 安装 router 插件
Vue.use(VueRouter)

// 创建 router 实例对象
const router = new VueRouter({
  // 路由模式: 
  // hash => url 中显示 #，利用 hashchange 事件监听#后的内容改变，展示对应的组件
  // history => url 没有#，h5 history API 的 pushState replaceState  修改历史记录，上线后需要后端配置
  mode: 'hash',
  // 配置路由页面
  routes: [
    {
      path: '/',
      // 重定向
      redirect: '/home'
    },
    {
      // 地址为 /home 时，页面展示 Home 组件
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      meta: {
        title: '首页'
      },
      // 配置子路由，此处路由只在home页面中展示
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          // 路由元信息
          meta: {
            a: 100,
            b: 200,
            title: '电影'
          }
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema,
          meta: {
            title: '影院'
          }
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine,
          meta: {
            title: '个人信息',
            isAuth: true
          }
        }
      ]
    },
    {
      // 配置动态路由
      path: '/detail',
      name: 'detail',
      component: Detail,
      meta: {
        isAuth: true,
        title: '详情页面'
      }
    },
    {
      path: '/city',
      name: 'city',
      component: City,
      meta: {
        isAuth: true,
        title: '选择城市'
      }
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: '用户登录'
      },
      component: Login
    },
    {
      path: '/404',
      name: '404',
      meta: {
        title: '404'
      },
      component: Notfound
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})


// 全局前置守卫(拦截)
router.beforeEach((to, from, next) => {
  // to: 跳转的目标路由，到哪去
  // from: 来源路由，从哪来
  // console.log('to', to)
  // 根据路由元信息修改 title 
  document.title = to.meta.title || 'app'
  // 根据路由元信息判断此路由是否需要登录
  // to.matched: 当前路由的所有层级
  // 判断父级路由和当前路由是否需要登录
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      console.log('没有登录,直接跳转到登录页面')
      next({
        path: '/login',
        query: {
          // 跳转到登录时，把目标路由的地址传给登录页面，url传参数时需要转译特殊字符
          fullpath: encodeURIComponent(to.fullPath)
        }
      })
      return
    }
  }
  // 继续执行
  next()
})

// 抛出实例
export default router
