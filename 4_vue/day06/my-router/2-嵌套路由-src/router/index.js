import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import Movie from '../pages/home/movie/Movie.vue'
import Cinema from '../pages/home/cinema/Cinema.vue'
import Mine from '../pages/home/mine/Mine.vue'

import Detail from '../pages/detail/Detail.vue'
import Login from '../pages/login/Login.vue'
import City from '../pages/city/City.vue'
import Notfound from '../pages/Notfound.vue'

// 给 vue 安装 router 插件
Vue.use(VueRouter)

// 创建 router 实例对象
const router = new VueRouter({
  // 路由模式: 
  // hash => url 中显示 #，利用 hashchange 事件监听#后的内容改变，展示对应的组件
  // history => url 没有#，h5 history API 的 pushState replaceState  修改历史记录，上线后需要后端配置
  mode: 'hash',
  // 配置路由页面
  routes: [
    {
      path: '/',
      // 重定向
      redirect: '/home'
    },
    {
      // 地址为 /home 时，页面展示 Home 组件
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      // 配置子路由，此处路由只在home页面中展示
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      // 配置动态路由
      path: '/detail/:id/:a/:b',
      name: 'detail',
      component: Detail
    },
    {
      path: '/city',
      name: 'city',
      component: City
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/404',
      name: '404',
      component: Notfound
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})

// 抛出实例
export default router
