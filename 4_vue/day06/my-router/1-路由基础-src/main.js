import Vue from 'vue'
import App from './App.vue'
// 引入 router 实例对象
import router from './router'

Vue.config.productionTip = false

new Vue({
  // 把 router 实例对象添加到 vue 实例对象中
  router,
  render: h => h(App)
}).$mount('#app')
