import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import Detail from '../pages/detail/Detail.vue'
import Login from '../pages/login/Login.vue'
import City from '../pages/city/City.vue'

// 给 vue 安装 router 插件
Vue.use(VueRouter)

// 创建 router 实例对象
const router = new VueRouter({
  // 路由模式: 
  // hash => url 中显示 #，利用 hashchange 事件监听#后的内容改变，展示对应的组件
  // history => url 没有#，h5 history API 的 pushState replaceState  修改历史记录，上线后需要后端配置
  mode: 'hash',
  // 配置路由页面
  routes: [
    {
      // 地址为 /home 时，页面展示 Home 组件
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      // 配置动态路由
      path: '/detail/:id/:a/:b',
      name: 'detail',
      component: Detail
    },
    {
      path: '/city',
      name: 'city',
      component: City
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

// 抛出实例
export default router
