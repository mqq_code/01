import Vue from 'vue'
import Vuex from 'vuex'
// 给vue安装插件
Vue.use(Vuex)

// 创建 vuex 实例对象
const store = new Vuex.Store({
  // 定义数据
  state: {
    list: [
      { title: '苹果', price: 10, count: 1 },
      { title: '香蕉', price: 11, count: 2 },
      { title: '梨', price: 5, count: 3 },
      { title: '橘子', price: 6, count: 4 },
    ]
  },
  // 修改vuex state数据的唯一方法，必须是一个同步函数
  // 在 mutations 中使用异步修改 state 数据 devtool 调试工具就无法跟踪 state 修改，不方便调试
  mutations: {
    changeCount(state, { index, num }) {
      console.log('changeCount', index, num)
      state.list[index].count += num
    }
  },
  // 类似组件中的计算属性，根据一个或者多个state数据生成一个新变量
  getters: {
    total(state) {
      // 自动监听函数中使用的变量，改变时自动重新计算
      return state.list.reduce((prev, val) => {
        return prev + val.count * val.price
      }, 0)
    }
  }
})
export default store