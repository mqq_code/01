import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false
Vue.config.devtools = true

Vue.use(ElementUI)

new Vue({
  router,
  // 把 vuex 实例对象添加 vue 中
  store,
  render: h => h(App)
}).$mount('#app')
