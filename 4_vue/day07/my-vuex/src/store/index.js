import Vue from 'vue'
import Vuex from 'vuex'

// 给vue安装插件
Vue.use(Vuex)

// 创建 vuex 实例对象
const store = new Vuex.Store({
})
export default store