import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// 给vue安装插件
Vue.use(Vuex)

// 创建 vuex 实例对象
const store = new Vuex.Store({
  state: {
    banners: []
  },
  mutations: {
    setBanners(state, payload) {
      state.banners = payload
    }
  },
  // 处理异步
  actions: {
    async getData(context) {
      // context: 类似组件中的 this.$store
      const res = await axios.get('https://zyxcl.xyz/music_api/banner')
      // 调用 mutations 的方法修改 state 数据
      context.commit('setBanners', res.data.banners)
    }
  }
})
export default store