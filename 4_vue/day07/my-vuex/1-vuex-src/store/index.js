import Vue from 'vue'
import Vuex from 'vuex'
// 给vue安装插件
Vue.use(Vuex)

// 创建 vuex 实例对象
const store = new Vuex.Store({
  // 定义数据
  state: {
    token: '123',
    rootTitle: '全局标题',
    num: 10,
    arr: [1,2,3,4,5],
    obj: { name: '小明' }
  },
  // 修改vuex state数据的唯一方法，必须是一个同步函数
  // 在 mutations 中使用异步修改 state 数据 devtool 调试工具就无法跟踪 state 修改，不方便调试
  mutations: {
    setNum(state, payload) {
      console.log('调用 setNum', payload)
      state.num += payload
    },
    setObjAge(state) {
      const age = state.obj.age || 0
      // 给响应式对象添加新属性
      Vue.set(state.obj, 'age', age + 1)
      // state.obj.age = age + 1
      console.log(state.obj)
    }
  }
})
export default store