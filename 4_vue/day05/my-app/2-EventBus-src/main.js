import Vue from 'vue'
import App from './App.vue'
// Event Bus: 同级或者跨级组件通讯，发布订阅模式
Vue.prototype.$bus = new Vue()

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
