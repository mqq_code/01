import Vue from 'vue'
import App from './App.vue'
import errorImg from './img/error.jpeg'
import loadingImg from './img/loading.gif'

Vue.config.productionTip = false

function renderImg (el, binding) {
  // 展示loading
  el.src = loadingImg
  setTimeout(() => {
    // 开始加载图片
    const img = new Image()
    img.src = binding.value
    img.onload = () => {
      // 加载成功
      el.src = binding.value
      console.log('%c 加载成功', 'font-size: 20px; color: green')
    }
    img.onerror = () => {
      // 加载失败
      console.log('加载失败')
      el.src = errorImg
    }
  }, 2000)
}

// 注册自定义指令
Vue.directive('load', {
  // 只执行一次，初次绑定到元素上时执行
  bind (el, binding) {
    console.log('bind', el, binding.value)
    renderImg(el, binding)
  },
  // 插入到父节点时执行
  inserted (el, binding) {
    // console.log('inserted', el.parentNode)
  },
  // 组件更新时执行, dom可能没有渲染完，无法获取到最新的dom
  update (el, binding) {
    if (binding.value !== binding.oldValue) {
      console.log('组件更新', binding)
      // 更新时重新渲染
      renderImg(el, binding)
    }
  },
  // 组件更新完成，可以获取到最新的dom
  componentUpdated () {
  },
  // 解绑时调用，销毁异步任务
  unbind () {
    console.log('指令解绑')
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')
