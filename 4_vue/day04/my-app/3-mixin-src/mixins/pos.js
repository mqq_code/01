export default {
  data () {
    return {
      a: 100,
      mixinTitle: '我是混入的变量',
      pos: {
        x: 0,
        y: 0
      }
    }
  },
  methods: {
    mixinFn () {
      console.log('我是混入的函数')
    },
    mousemove (e) {
      this.pos = {
        x: e.clientX,
        y: e.clientY
      }
    }
  },
  created () {
    console.log('混入的created')
  },
  mounted () {
    document.addEventListener('mousemove', this.mousemove)
  },
  beforeDestroy () {
    console.log('混入的销毁之前')
    document.removeEventListener('mousemove', this.mousemove)
  }
}
