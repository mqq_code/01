const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'https://zyxcl.xyz/exam_api',
        pathRewrite: { '^/api': '' }
      }
    }
  }
})
