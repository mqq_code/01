import Vue from 'vue'
// .vue 文件就是一个组件
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
