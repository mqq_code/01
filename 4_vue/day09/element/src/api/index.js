import axios from 'axios'
import { Message } from 'element-ui'
import router from '../router'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 发送请求之前给后端传公共参数，例如把 token 传给 header
  config.headers.authorization = localStorage.getItem('token')
  config.headers.aa = 'AAAAAAAAAAAA'
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
})

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  if (error.response.status === 401) {
    // 登录失效
    Message.error('登录信息失效，请重新登录')
    localStorage.removeItem('token')
    router.push('/login')
  } else {
    Message.error(error.message)
  }
  return Promise.reject(error);
});

export const loginApi = (params) => {
  return axios.post('/api/login', params)
}

export const registerApi = (params) => {
  return axios.post('/api/register', params)
}
// 个人信息
export const userInfoApi = () => {
  return axios.get('/api/user/info')
}

// 所有列表
export const userListApi = (params) => {
  return axios.get('/api/userlist', {
    params
  })
}
// 删除接口
export const delUserApi = (id) => {
  return axios.post('/api/user/delete', {
    id
  })
}
// 新增接口
export const createUserApi = (params) => {
  return axios.post('/api/user/create', params)
}
// 编辑接口
export const updateUserApi = (params) => {
  return axios.post('/api/user/update', params)
}

// 上传头像
export const updateAvatarApi = (formdata) => {
  return axios.post('/api/user/avatar', formdata)
}

// 导出excel
export const exportExcelApi = (ids) => {
  return axios.post('/api/export', { ids }, { responseType: 'blob' })
}
