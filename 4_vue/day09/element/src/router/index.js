import Vue from 'vue'
import VueRouter from 'vue-router'
import { Message } from 'element-ui'
import Home from '../views/home/Home.vue'
import Dashboard from '../views/home/dashboard/Dashboard.vue'
import List from '../views/home/list/List.vue'
import Edit from '../views/home/edit/Edit.vue'
import Detail from '../views/home/detail/Detail.vue'
import Notfound from '../views/Notfound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/dashboard',
    meta: {
      isAuth: true
    },
    children: [
      { path: '/dashboard', name: 'dashboard', component: Dashboard },
      { path: '/list', name: 'list', component: List },
      { path: '/edit', name: 'edit', component: Edit },
      { path: '/detail', name: 'detail', component: Detail }
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: 'login' */'../views/login/Login.vue')
  },
  {
    path: '/404',
    name: 'notfound',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      Message.error('登录信息失效，请重新登录')
      next('/login')
      returng
    }
  }
  next()
})

export default router
