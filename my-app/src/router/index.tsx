import type { RouteObject } from 'react-router-dom'
import { Navigate } from 'react-router-dom'
import { lazy } from 'react'
import Home from '../pages/home/Home'
import Login from '../pages/login/Login'
import Detail from '../pages/detail/Detail'
// import Child1 from '../pages/home/child1/Child1'
// import Child2 from '../pages/home/child2/Child2'
// import Child3 from '../pages/home/child3/Child3'

// 异步加载组件
const Child1 = lazy(() => import('../pages/home/child1/Child1'))
const Child2 = lazy(() => import('../pages/home/child2/Child2'))
const Child3 = lazy(() => import('../pages/home/child3/Child3'))
const Child4 = lazy(() => import('../pages/home/child4/Child4'))
const Child5 = lazy(() => import('../pages/home/child5/Child5'))

export const routes: RouteObject[] = [
  {
    path: '/',
    element: <Home></Home>,
    children: [
      {
        path: '/',
        element: <Navigate to="/child1" />
      },
      {
        path: '/child1',
        element: <Child1></Child1>,
      },
      {
        path: '/child2',
        element: <Child2></Child2>,
      },
      {
        path: '/child3',
        element: <Child3></Child3>,
      },
      {
        path: '/child4',
        element: <Child4></Child4>,
      },
      {
        path: '/child5',
        element: <Child5></Child5>,
      }
    ]
  },
  {
    path: '/login',
    element: <Login></Login>
  },
  {
    path: '/detail/:id/:name',
    element: <Detail></Detail>
  },
  {
    path: '*',
    element: <div style={{ color: 'red', fontSize: '40px' }}>404</div>
  }
]