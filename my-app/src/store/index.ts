import { configureStore } from '@reduxjs/toolkit'
import userReducer from './models/user'
import addressReducer from './models/address'


export const store = configureStore({
  reducer: {
    user: userReducer,
    address: addressReducer
  }
})

// 推断出整个store的数据类型
export type RootState = ReturnType<typeof store.getState>
// 推断出整个dispatch的数据类型
export type AppDispatch = typeof store.dispatch