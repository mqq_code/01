import { createSlice } from '@reduxjs/toolkit'


const userSlice = createSlice({
  name: 'user',
  initialState: {
    name: '小明',
    age: 20,
    sex: '男'
  },
  reducers: {
    setName(state, { payload }) {
      state.name = payload
    },
    setAge(state, { payload }) {
      state.age = state.age + payload
    },
    setInfo(state, { payload }) {
      state.age = payload.age
      state.name = payload.name
    }
  }
})

// 抛出action
export const { setName, setAge, setInfo } = userSlice.actions

export default userSlice.reducer