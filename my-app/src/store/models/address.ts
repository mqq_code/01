import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

// 定义state类型
export interface IState {
  currentCity: string;
  cityList: any[];
}

// 创建异步 action
export const addressCityList = createAsyncThunk<{ banners: any[] }>('address/addressCityList', async () => {
  const res = await axios.get('https://zyxcl.xyz/music_api/banner')
  return res.data
})

// 定义state初始值
export const initialState: IState = {
  currentCity: '北京',
  cityList: [] 
}

const addressSlice = createSlice({
  name: 'address',
  initialState,
  reducers: {
    // 定义修改state的方法
  },
  extraReducers: builder => {
    // 监听异步 action 执行
    builder
      .addCase(addressCityList.fulfilled, (state, { payload }) => {
        // 等待 addressCityList 异步执行完毕
        console.log('addressCityList中异步的返回值', payload.banners)
        state.cityList = payload.banners
      })
      // .addCase(xxxx.fulfilled, (state, { payload }) => {
      // })
      // .addCase(abc.pending, (state, { payload }) => {
      // })
      // .addCase(abc.rejected, (state, { payload }) => {
      // })
  }
})

export default addressSlice.reducer