import React from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'

const Detail: React.FC = () => {
  const location = useLocation()
  const [searchParams] = useSearchParams()
  const params = useParams()

  console.log(location.state)
  console.log(searchParams.get('id'))
  console.log(searchParams.get('img'))
  console.log(params)

  return (
    <div>Detail</div>
  )
}

export default Detail