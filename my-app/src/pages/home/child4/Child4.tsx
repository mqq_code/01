import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setName, setAge, setInfo } from '../../../store/models/user'

const Child4: React.FC = () => {
  const dispatch = useDispatch()
  const username = useSelector((rootState: any) => rootState.user.name)
  const age = useSelector((rootState: any) => rootState.user.age)


  return (
    <div>
      <h3>{username}</h3>
      <input type="text" value={username} onChange={e => {
        dispatch(setName(e.target.value))
      }} />
      <p>年龄：{age} <button onClick={() => {
        dispatch(setAge(1))
      }}>+</button></p>
      <hr />
      <button onClick={() => {
        dispatch(setInfo({ name: '小明', age: 20 }))
      }}>重置</button>
    </div>
  )
}

export default Child4