import React, { useEffect, useState, useRef } from 'react'
import Count from '../../../components/Count'


const Child3: React.FC = () => {
  const [count, setCount] = useState(0)
  const timer = useRef<any>(null)

  useEffect(() => {
    timer.current = setInterval(() => {
      setCount(c => {
        if (c + 1 === 10) {
          clearInterval(timer.current)
        }
        return c + 1
      })
    }, 1000)

    return () => {
      clearInterval(timer.current)
    }
  }, [])

  console.log(count)

  return (
    <div>
      <h2>Child3</h2>
      <p>计时器：{count}</p>
      <button onClick={() => {
        clearInterval(timer.current)
      }}>停止</button>
      <hr />
      <Count num={count} />
    </div>
  )
}

export default Child3



// const useCount = () => {

// }

// const { start, stop, reset } = useCount(10)

// <button onClick={start}>开始</button>