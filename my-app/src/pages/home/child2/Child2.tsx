import React, { useState, useCallback } from 'react'
import List from '../../../components/List'

const Child2: React.FC = () => {
  const [count, setCount] = useState(0)
  const [title, setTitle] = useState('Child2')

  const changeCount = useCallback((n: number) => {
    setCount(c => c + n)
  }, [])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <List
        count={count}
        onChange={changeCount}
      />
    </div>
  )
}

export default Child2