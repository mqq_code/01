import { Suspense } from 'react'
import style from './home.module.scss'
import { NavLink, Outlet } from 'react-router-dom'

const Home = () => {
  return (
    <div className={style.home}>
      <header>
        <NavLink to="/child1">路由1</NavLink>
        <NavLink to="/child2">路由2</NavLink>
        <NavLink to="/child3">路由3</NavLink>
        <NavLink to="/child4">路由4</NavLink>
        <NavLink to="/child5">路由5</NavLink>
        {/* <span className="box">aaaa</span> */}
      </header>
      <section>
        <Suspense fallback={<div style={{ color: 'red', fontSize: '30px' }}>加载中</div>}>
          <Outlet />
        </Suspense>
      </section>
    </div>
  )
}

export default Home