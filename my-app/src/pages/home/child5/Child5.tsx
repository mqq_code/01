import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addressCityList } from '../../../store/models/address'
import type { RootState } from '../../../store'

const Child5 = () => {
  const cityList = useSelector((state: RootState) => state.address.cityList)
  const dispatch = useDispatch()

  console.log(cityList)

  useEffect(() => {
    dispatch(addressCityList())
  }, [])

  return (
    <div>Child5</div>
  )
}

export default Child5