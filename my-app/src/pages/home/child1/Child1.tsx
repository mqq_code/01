import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

interface TopListItem {
  id: number;
  coverImgUrl: string;
  name: string;
}

interface TopListRes {
  code: number;
  list: TopListItem[]
}

const Child1: React.FC = () => {
  const [list, setList] = useState<TopListItem[]>([])
  const navigate = useNavigate()

  const getTopList = async () => {
    const res = await axios.get<TopListRes>('https://zyxcl.xyz/music_api/toplist')
    setList(res.data.list)
  }

  useEffect(() => {
    getTopList()
  }, [])

  console.log(list)

  return (
    <div>
      <ul>
        {list.map(item =>
          <li key={item.id} onClick={() => {
            // navigate(`/detail?id=${item.id}&img=${encodeURIComponent(item.coverImgUrl)}`)
            // navigate({
            //   pathname: '/detail',
            //   search: `id=${item.id}&img=${encodeURIComponent(item.coverImgUrl)}`
            // })
            // navigate(`/detail/${item.id}/${item.name}?id=${item.id}&img=${encodeURIComponent(item.coverImgUrl)}`)
            navigate({
              pathname: `/detail/${item.id}/${item.name}`,
              search: `id=${item.id}&img=${encodeURIComponent(item.coverImgUrl)}`,
            }, {
              state: {
                img: item.coverImgUrl,
                id: item.id
              }
            })
          }}>
            <img src={item.coverImgUrl} width={100} alt="" />
            <h3>{item.name}</h3>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Child1