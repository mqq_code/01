import React, { memo } from 'react'

interface Props {
  title?: string;
  count: number;
  onChange: (n: number) => void;
}

const List: React.FC<Props> = ({
  title = '默认标题',
  count,
  onChange
}) => {
  console.log('子组件渲染了')
  return (
    <div className='list-warp'>
      <h2>{title}</h2>
      <p>
        <button onClick={() => onChange(-1)}>-</button>
        {count}
        <button onClick={() => onChange(1)}>+</button>
      </p>
    </div>
  )
}

// memo: 浅比较组件最新的props和当前的props是否一致
export default memo(List)