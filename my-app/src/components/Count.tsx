import React, { Component } from 'react'

interface Props {
  num: number;
}
interface State {
  title: string;
}

class Count extends Component<Props, State> {

  state = {
    title: '默认标题'
  }

  shouldComponentUpdate(nextProps: Readonly<Props>, nextState: Readonly<State>, nextContext: any): boolean {
    return false
  }

  render() {
    return (
      <div>
        <h2>{this.state.title}</h2>
        <input type="text" value={this.state.title} onChange={e => {
          this.setState({
            title: e.target.value
          })
        }} />
        <p>num: {this.props.num}</p>
      </div>
    )
  }
}

export default Count