Page({
  data: {
    text: '',
    show: false,
  },

  onDisplay() {
    this.setData({ show: true });
  },
  onClose() {
    this.setData({ show: false });
  },
  onConfirm(event) {
    this.setData({
      show: false,
      text: `选择了 ${event.detail.length} 个日期`,
    });
  },
});