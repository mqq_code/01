import { defineStore } from 'pinia'
import { ref } from 'vue'


export const useCartStore = defineStore('cartStore', () => {
  const list = ref([])
  const cart = ref([])
  
  // 调用接口
  const getGoodsList = () => {
    console.log('调用接口');
    uni.request({
      // #ifdef H5
      url: '/api/sell/api/goods',
      // #endif
      // #ifndef H5
      url: 'http://ustbhuangyi.com/sell/api/goods',
      // #endif
      success: res => {
        list.value = res.data.data
        console.log(1111);
      },
      fail: err => {
        console.log(err);
      }
    })
  }
  // 修改数量
  const changeCount = (food, num) => {
    if (!food.count) food.count = 0
    food.count += num
    const index = cart.value.findIndex(v => v.name === food.name)

    if (index === -1) {
      cart.value.push(food)
    } else {
      if (food.count <= 0) {
        cart.value.splice(index, 1)
      }
    }
    
  }
  
  return {
    list,
    cart,
    getGoodsList,
    changeCount
  }
})