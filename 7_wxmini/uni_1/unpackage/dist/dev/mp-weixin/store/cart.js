"use strict";
const common_vendor = require("../common/vendor.js");
const useCartStore = common_vendor.defineStore("cartStore", () => {
  const list = common_vendor.ref([]);
  const cart = common_vendor.ref([]);
  const getGoodsList = () => {
    console.log("调用接口");
    common_vendor.index.request({
      url: "http://ustbhuangyi.com/sell/api/goods",
      success: (res) => {
        list.value = res.data.data;
        console.log(1111);
      },
      fail: (err) => {
        console.log(err);
      }
    });
  };
  const changeCount = (food, num) => {
    if (!food.count)
      food.count = 0;
    food.count += num;
    const index = cart.value.findIndex((v) => v.name === food.name);
    if (index === -1) {
      cart.value.push(food);
    } else {
      if (food.count <= 0) {
        cart.value.splice(index, 1);
      }
    }
  };
  return {
    list,
    cart,
    getGoodsList,
    changeCount
  };
});
exports.useCartStore = useCartStore;
