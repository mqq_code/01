"use strict";
const common_vendor = require("../../common/vendor.js");
const store_cart = require("../../store/cart.js");
const _sfc_main = {
  __name: "detail",
  setup(__props) {
    const cartStore = store_cart.useCartStore();
    const name = common_vendor.ref("");
    const curFood = common_vendor.computed(() => {
      let cur = null;
      cartStore.list.forEach((item) => {
        item.foods.forEach((food) => {
          if (food.name === name.value) {
            cur = food;
          }
        });
      });
      return cur;
    });
    common_vendor.onLoad((options) => {
      console.log("获取地址栏参数", options);
      name.value = options.name;
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(common_vendor.unref(curFood))
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/01/7_wxmini/uni_1/pages/detail/detail.vue"]]);
wx.createPage(MiniProgramPage);
