"use strict";
const common_vendor = require("../../common/vendor.js");
const store_cart = require("../../store/cart.js");
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const cartStore = store_cart.useCartStore();
    cartStore.getGoodsList();
    const curIndex = common_vendor.ref(0);
    const goDetail = (name) => {
      common_vendor.index.navigateTo({
        url: `/pages/detail/detail?name=${name}`
      });
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.f(common_vendor.unref(cartStore).list, (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: item.name,
            c: common_vendor.n({
              active: curIndex.value === index
            }),
            d: common_vendor.o(($event) => curIndex.value = index, item.name)
          };
        }),
        b: common_vendor.unref(cartStore).list[curIndex.value]
      }, common_vendor.unref(cartStore).list[curIndex.value] ? {
        c: common_vendor.t(common_vendor.unref(cartStore).list[curIndex.value].name),
        d: common_vendor.f(common_vendor.unref(cartStore).list[curIndex.value].foods, (food, k0, i0) => {
          return {
            a: common_vendor.t(food.name),
            b: food.image,
            c: common_vendor.o(($event) => goDetail(food.name), food.name),
            d: common_vendor.o(($event) => common_vendor.unref(cartStore).changeCount(food, 1), food.name),
            e: common_vendor.t(food.count || 0),
            f: common_vendor.o(($event) => common_vendor.unref(cartStore).changeCount(food, -1), food.name),
            g: food.name
          };
        })
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1cf27b2a"], ["__file", "/Users/zhaoyaxiang/Desktop/01/7_wxmini/uni_1/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
