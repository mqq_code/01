"use strict";
const common_vendor = require("../../common/vendor.js");
const store_cart = require("../../store/cart.js");
const _sfc_main = {
  __name: "cart",
  setup(__props) {
    const cartStore = store_cart.useCartStore();
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(common_vendor.unref(cartStore).cart, (food, k0, i0) => {
          return {
            a: common_vendor.t(food.name),
            b: common_vendor.t(food.count),
            c: food.name
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/01/7_wxmini/uni_1/pages/cart/cart.vue"]]);
wx.createPage(MiniProgramPage);
