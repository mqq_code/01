// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollY: 0,
    inner: '<h1 style="color: red">大标题</h1>',
    person: {
      name: '小明',
      age: 22,
      info: {
        a: {
          b: {
            c: 100
          }
        }
      }
    }
  },
  input(e){
    console.log('获取input内容', e.detail.value);
    // this.setData({
    //   person: {...this.data.person, name: e.detail.value}
    // })
    this.setData({
      'person.info.a.b.c': e.detail.value
    })
  },

  changeScroll(){
    this.setData({
      scrollY: 100
    })
    console.log(this.data.scrollY);
  },

  change(e) {
    console.log(e.detail);
  },

  scroll(e) {
    console.log(e);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})