// pages/detail/detail.js
function request({ url, method = 'GET', data = {} }) {
  return new Promise((resolve, reject) => {
    wx.request({
      url,
      data,
      method,
      success: res => {
        resolve(res.data)
      },
      fail: err => {
        reject(err)
      }
    })
  })
}


Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    console.log('onload');
    const res = await request({
      url: 'https://zyxcl.xyz/music_api/banner'
    })
    this.setData({
      banners: res.banners
    })
    console.log(this.data.banners);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('onReady');
    // wx.showShareMenu({
    //   menus: ['shareAppMessage', 'shareTimeline']
    // })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log('onShow');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log('onHide');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log('onUnload');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage({ from }) {
    // from: 触发分享的来源，button 或者 menu
    return {
      title: '我是分享的标题',
      path: '/pages/mine/mine',
      imageUrl: 'http://p1.music.126.net/Dx2LIIBk_R9JRNG5Xc2K6w==/109951169500279261.jpg'
    }
  }
})