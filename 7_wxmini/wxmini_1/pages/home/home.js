// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '我是一个默认标题',
    className: 'box',
    arr: ['a', 'b', 'c', 'd'],
    arrObj: [
      {name: '小明1', age: 22},
      {name: '小明2', age: 33},
      {name: '小明3', age: 25},
      {name: '小明4', age: 28}
    ],
    num: 99,
    person: {
      name: '小明',
      age: 33
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})