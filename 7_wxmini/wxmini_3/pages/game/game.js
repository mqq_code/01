// pages/game/game.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {
      avatarUrl: wx.getStorageSync('avatarUrl') || '',
      money: 10000
    },
    // 骰子点数
    dices: [4, 5, 6],
    // 下注金额
    chipMoney: 0
  },

  addChipMoney(e) {
    console.log('增加下注金额', e.detail);
    let chipMoney = this.data.chipMoney + e.detail
    // 限制上限
    chipMoney = chipMoney > this.data.user.money ? this.data.user.money : chipMoney
    this.setData({ chipMoney })
  },

  clearChip() {
    this.setData({
      chipMoney: 0
    })
  },

  async start(e) {
    if (this.data.user.money <= 0) {
      wx.showToast({
        title: '没钱了',
      })
      return
    }
    if (this.data.chipMoney === 0) {
      wx.showToast({
        title: '请先下注',
        icon: 'error'
      })
      return
    }
    console.log('开始摇', e.detail);
    const result = await this.getDicesResult()
    const resultText = this.computedDice(result)
    console.log('摇完了', resultText);
    if (e.detail === resultText) {
      console.log('赢了');
      this.setData({
        'user.money': this.data.user.money + this.data.chipMoney
      })
      wx.showToast({
        title: '赢了',
      })
    } else {
      console.log('输了');
      this.setData({
        'user.money': this.data.user.money - this.data.chipMoney
      })
      wx.showToast({
        title: '输了',
        icon: 'error'
      })
    }
    this.clearChip()
  },

  computedDice(arr) {
    if (arr.every(v => arr[0] === v)) {
      return '豹子'
    }
    const total = arr.reduce((prev, val) => prev + val)
    if (total >= 10) {
      return '大'
    }
    return '小'
  },

  getDicesResult() {
    return new Promise((resolve, reject) => {
      const interval = setInterval(() => {
        this.setData({
          dices: this.data.dices.map(this.createRandom)
        })
      }, 60)
      setTimeout(() => {
        clearInterval(interval)
        resolve(this.data.dices)
      }, 2000)
    })
  },

  createRandom() {
    return Math.floor(Math.random() * 6) + 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 获取本地数据
    const arr = wx.getStorageSync('arr')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})