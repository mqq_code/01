// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: wx.getStorageSync('avatarUrl') || ''
  },

  getAvatar(e) {
    console.log(e.detail.avatarUrl);
    // 获取用户头像
    this.setData({
      avatarUrl: e.detail.avatarUrl
    })
    // 本地存储，可以存对象类型的数据，不需要转成字符串
    wx.setStorageSync('arr', [{name: 'aaa', age: 22}, {name: 'aaa', age: 22}])
    wx.setStorageSync('avatarUrl', e.detail.avatarUrl)
  },

  goGame() {
    wx.navigateTo({
      url: '/pages/game/game',
    })
  },



  // 通过按钮获取用户信息的回调
  getButtonUserinfo(e) {
    console.log(e.detail);
  },

  getUserInfo() {
    // 获取用户信息
    wx.getUserProfile({
      desc: '展示用户名和头像',
      success: res => {
        console.log(res);
      },
      fail: err => {
        console.log(err);
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})