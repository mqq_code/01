// components/footer/footer.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    btns: ['大', '豹子', '小']
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleClick(e) {
      // 通知父组件开始摇骰子
      const { text } = e.currentTarget.dataset
      this.triggerEvent('click', text)
    }
  }
})
