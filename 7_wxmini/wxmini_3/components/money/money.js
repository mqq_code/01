// components/money/money.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    chipMoney: Number
  },

  /**
   * 组件的初始数据
   */
  data: {
    moneyList: [50, 100, 200, 500]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickMoney(e) {
      const { num } = e.currentTarget.dataset
      console.log('点击下注金额', num)
      this.triggerEvent('addMoney', num)
    }
  }
})
