// components/counter/counter.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 接收父组件传入的参数，需要定义类型
    rootTitle: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    count: 0,
  },

  observers: {
    // 监听变量修改，类似vue中的watch
    count(newCount) {
      console.log('count改变了', newCount);
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeCount(e) {
      console.log(e.currentTarget.dataset);
      const { num } = e.currentTarget.dataset
      this.setData({
        count: this.data.count + num
      })
    },
    handleClick() {
      console.log('点击了修改标题的按钮');
      // 调用父组件传入的自定义事件，给父组件传参数
      this.triggerEvent('change', {
        title: Math.random()
      })
    }
  }
})
