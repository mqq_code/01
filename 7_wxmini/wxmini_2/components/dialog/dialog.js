// components/dialog/dialog.js
Component({
  options: {
    multipleSlots: true // 启用多slot支持
  },

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    title: '我是dialog组件'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    close(e) {
      // 调用父组件传入的事件
      this.triggerEvent('close')
    }
  },
  // 定义生命周期
  lifetimes: {
    created() {
      console.log('组件创建成功，不能调用 setData', this.data);
    },
    attached() {
      console.log('组件加载完成', this.data);
    },
    detached() {
      console.log('组件销毁');
    }
  }
})
