// pages/home/home.js
// 获取app实例
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'home的标题',
    show: false,
    // 使用全局数据
    userInfo: app.globalData.userInfo
  },
  setShow() {
    this.setData({
      show: true
    })
  },
  closeDialog() {
    this.setData({
      show: false
    })
  },
  changeTitle(e) {
    console.log('接收子组件传入的数据', e.detail);
    this.setData({
      title: e.detail.title
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('获取全局数据', app.globalData)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})